/**
** \file parser.h
** \brief All the rule needed to parse a command.
**
** All the functions, except parse_redirection and parse_element, taken a
** lexer and return the ast built. Parse_redirection and parse_element
** return 1 if the function has success, 0 if the function failed but the
** parsing can continue. If a fatal error happen the function return -1.
** The axiom is parse_input. See 42sh grammar to more details.
*/
#ifndef PARSER_COMMAND_H
#define PARSER_COMMAND_H

#include <string.h>

#include "utilities/utilities.h"
#include "lexer/lexer.h"
#include "parser/ast.h"


#define REDIRECT_WRG_FILE -10

/* parser_utils.c */
struct ast *handle_error(struct ast *ast);
struct ast *syntax_error(struct lexer *lexer, struct ast *ast);

/* parser_input.c */
struct ast *parse_input(struct lexer *lexer);
struct ast *parse_list(struct lexer *lexer);
struct ast *parse_compound_list_break(struct lexer *lexer);

/* parser_if.c */
struct ast *parse_rule_if(struct lexer *lexer);
struct ast *parse_rule_else_clause(struct lexer *lexer);
struct ast *parse_rule_elif(struct lexer *lexer);

/* parser_command.c */
struct ast *parse_shell_command(struct lexer *lexer);
struct ast *parse_command(struct lexer *lexer);
int parse_element(struct lexer *lexer, struct ast *cmd);
struct ast *parse_simple_command(struct lexer *lexer);
struct ast *parse_and_or(struct lexer *lexer);
struct ast *parse_fundec(struct lexer *lexer);
struct ast *parse_prefix(struct lexer *lexer, struct ast *cmd);

/* parser_redirection.c */
struct ast *parse_pipeline(struct lexer *lexer);
int parse_redirection(struct lexer *lexer, struct ast *ast);

/* parser_case.c */
struct ast *parse_rule_case(struct lexer *lexer);
struct case_item *parse_case_clause(struct lexer *lexer);
struct case_item *parse_case_item(struct lexer *lexer);

/* parser_loop.c */
struct ast *parse_rule_while(struct lexer *lexer);
struct ast *parse_rule_until(struct lexer *lexer);
struct ast *parse_do_group(struct lexer *lexer);
struct ast *parse_rule_for(struct lexer *lexer);


#endif /* PARSER_COMMAND_H */
