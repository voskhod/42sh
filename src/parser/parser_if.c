#include "parser/parser.h"


struct ast *parse_rule_if(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter if");
#endif /* DEBUG */

    struct ast *ast_if = ast_init(AST_NODE_IF);
    ast_if->statement.if_node = mcalloc(1, sizeof(struct if_node));

    if (lexer_peek(lexer) != TOKEN_IF)
        return handle_error(ast_if);

    token_destroy(lexer_pop(lexer));
    ast_if->statement.if_node->cond = parse_compound_list_break(lexer);
    if (ast_if->statement.if_node->cond == NULL)
        return handle_error(ast_if);

    if (lexer_peek(lexer) != TOKEN_THEN)
        return handle_error(ast_if);

    token_destroy(lexer_pop(lexer));
    ast_if->statement.if_node->if_body = parse_compound_list_break(lexer);
    if (ast_if->statement.if_node->if_body == NULL)
        return handle_error(ast_if);

    ast_if->statement.if_node->else_body = parse_rule_else_clause(lexer);

    if (lexer_peek(lexer) != TOKEN_FI)
        return handle_error(ast_if);

    token_destroy(lexer_pop(lexer));

    return ast_if;
}


struct ast *parse_rule_else_clause(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter else_clause");
#endif /* DEBUG */

    if (lexer_peek(lexer) == TOKEN_ELSE)
    {
        token_destroy(lexer_pop(lexer));
        return parse_compound_list_break(lexer);
    }
    else if (lexer_peek(lexer) == TOKEN_ELIF)
        return parse_rule_elif(lexer);
    else
        return NULL;
}


struct ast *parse_rule_elif(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter if");
#endif /* DEBUG */

    struct ast *ast_if = ast_init(AST_NODE_IF);
    ast_if->statement.if_node = mcalloc(1, sizeof(struct if_node));

    if (lexer_peek(lexer) != TOKEN_ELIF)
        return handle_error(ast_if);

    token_destroy(lexer_pop(lexer));
    ast_if->statement.if_node->cond = parse_compound_list_break(lexer);
    if (ast_if->statement.if_node->cond == NULL)
        return handle_error(ast_if);

    if (lexer_peek(lexer) != TOKEN_THEN)
        return handle_error(ast_if);

    token_destroy(lexer_pop(lexer));
    ast_if->statement.if_node->if_body = parse_compound_list_break(lexer);
    if (ast_if->statement.if_node->if_body == NULL)
        return handle_error(ast_if);

    ast_if->statement.if_node->else_body = parse_rule_else_clause(lexer);


    return ast_if;
}
