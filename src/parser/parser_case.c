#include "parser/parser.h"


struct ast *parse_rule_case(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter rule case");
#endif
    if (lexer_peek(lexer) != TOKEN_CASE)
        return NULL;
    token_destroy(lexer_pop(lexer));

    if (lexer_peek(lexer) != TOKEN_WORD)
        return NULL;
    struct ast *ast_case = ast_init(AST_NODE_CASE);
    ast_case->statement.case_node = mcalloc(1, sizeof(struct case_node));
    struct token *token = lexer_pop(lexer);
    ast_case->statement.case_node->value = strdup(token->value);
    token_destroy(token);

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    if (lexer_peek(lexer) != TOKEN_IN)
        return handle_error(ast_case);

    token_destroy(lexer_pop(lexer));

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    ast_case->statement.case_node->item = parse_case_clause(lexer);

    if (lexer_peek(lexer) != TOKEN_ESAC)
        return handle_error(ast_case);
    token_destroy(lexer_pop(lexer));
    return ast_case;
}


struct case_item *parse_case_clause(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter parser_case_clause");
#endif
    struct case_item *item = parse_case_item(lexer);
    struct case_item *current = item;

    while (lexer_peek(lexer) == TOKEN_DBL_SEMI_COLON)
    {
        token_destroy(lexer_pop(lexer));
        while (lexer_peek(lexer) == TOKEN_ENDLINE)
            token_destroy(lexer_pop(lexer));
        current->next = parse_case_item(lexer);
        current = current->next;
    }

    if (lexer_peek(lexer) == TOKEN_DBL_SEMI_COLON)
        token_destroy(lexer_pop(lexer));

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));
    return item;

}


struct case_item *parse_case_item(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter Case_item");
#endif
    if (lexer_peek(lexer) == TOKEN_LEFT_PARENTHESIS)
        token_destroy(lexer_pop(lexer));

    if (lexer_peek(lexer) != TOKEN_WORD)
        return NULL;
    struct case_item *item = mcalloc(1, sizeof(struct case_item));
    item->vector = vector_init();

    struct token *token = NULL;

    token = lexer_pop(lexer);
    vector_append(item->vector, token->value);
    token_destroy(token);

    while (lexer_peek(lexer) == TOKEN_PIPE)
    {
        token_destroy(lexer_pop(lexer));
        token = lexer_pop(lexer);
        vector_append(item->vector, token->value);
        token_destroy(token);
    }

    if (lexer_peek(lexer) != TOKEN_RIGHT_PARENTHESIS)
        return NULL;

    token_destroy(lexer_pop(lexer));

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    item->body = parse_compound_list_break(lexer);

    return item;
}
