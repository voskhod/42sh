/**
** \file ast.h
** \brief AST initilisation and destruction.
*/
#ifndef AST_H
#define AST_H

#include "parser/command.h"
#include "utilities/vector.h"

/**
** \enum
** \brief The type of a AST node.
*/
enum ast_node_type
{
    AST_NODE_COMMAND = 0,
    AST_NODE_LIST_BREAK,
    AST_NODE_IF,
    AST_NODE_AND_OR,
    AST_NODE_WHILE,
    AST_NODE_UNTIL,
    AST_NODE_PIPE,
    AST_NODE_FOR,
    AST_NODE_CASE,
    AST_NODE_FUN,
    AST_NODE_VAR_DEF
};


struct var_def
{
    char *var_def;
};


struct case_item
{
    struct vector *vector;
    struct ast *body;
    struct case_item *next;
};


struct case_node
{
    char *value;
    struct case_item *item;
};


struct for_node
{
    struct vector *vector;

    char *var;
    struct ast *body;
};


struct pipeline
{
    struct ast *left;
    struct ast *right;
};


struct compound_list_break
{
    struct ast *current;

    struct compound_list_break *next;
};


struct until_node
{
    struct ast *cond;

    struct ast *body;
};


struct while_node
{
    struct ast *cond;

    struct ast *body;
};


struct function
{
    char *name;
    struct ast *fun_body;
};

/**
** \struct and_node
** \brief A node for And and Or statement
*/
struct and_or_node
{
    enum logical_type
    {
        AND_TYPE,
        OR_TYPE
    } type;
    struct ast *left;
    struct ast *right;
};


/**
** \struct if_node
** \brief The statment if.
*/
struct if_node
{
    struct ast *cond;

    struct ast *if_body;
    struct ast *else_body;
};


/**
** \struct ast
** \brief A node of an AST.
**
** This struct represent a statements. The statement field is the action needed
** to be executed. All the AST are linked to the next statement.
*/
struct ast
{
    struct ast *next;

    int is_async;

    int fd_in;
    int fd_out;
    int fd_err;

    enum ast_node_type type;
    union
    {
        struct command *cmd;
        struct compound_list_break *cpd_list_break;
        struct if_node *if_node;
        struct and_or_node *and_or_node;
        struct while_node *while_node;
        struct until_node *until_node;
        struct for_node *for_node;
        struct pipeline *pipeline;
        struct case_node *case_node;
        struct function *function;
        struct var_def *var_def;
    } statement;
};


/**
** \fn struct ast *ast_init(enum ast_node_type type)
** \brief Initialize an AST node.
**
** \param type The type of the AST node desire.
** \return The AST node initialized.
*/
struct ast *ast_init(enum ast_node_type type);

/**
** \fn void ast_destroy(struct ast *ast)
** \brief Destroy an AST.
**
** \param ast The AST to destoy.
**
** This function destroy all the AST node recursively.
*/
void ast_destroy(struct ast *ast);

/**
** \fn void ast_node_destroy(struct ast *ast_node)
** \brief Destroy only ONE AST node.
**
** \param ast_node The AST node to destroy.
**
** Don't use this function, use ast_destroy. This function is only for 
** backward compatibily.
*/
void ast_node_destroy(struct ast *ast_node);

#endif /* AST_H */
