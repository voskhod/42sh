#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <err.h>

#include "parser/parser.h"
#include "interface/readline.h"
#include "utilities/utilities.h"


static void set_fd(struct ast *ast, int io_number, int fd)
{
    if (fd == REDIRECT_WRG_FILE)
    {
        ast->fd_in = REDIRECT_WRG_FILE;
        return;
    }
    if (fd < -1)
    {
        fd = fd * -1 - 2;
        int dst;

         if (fd == STDIN_FILENO)
            dst = ast->fd_in;
        else if (fd == STDOUT_FILENO)
            dst = ast->fd_out;
        else
            dst = ast->fd_err;

        fd = dup(dst);
    }

    if (io_number == STDIN_FILENO)
    {
        if (ast->fd_in != -1)
            close(ast->fd_in);
        ast->fd_in = fd;
    }
    else if (io_number == STDOUT_FILENO)
    {
        if (ast->fd_out != -1)
            close(ast->fd_out);
        ast->fd_out = fd;
    }
    else if (io_number == STDERR_FILENO)
    {
        if (ast->fd_err != -1)
            close(ast->fd_err);
        ast->fd_err = fd;
    }
}


static int redirect_out(char *file)
{
    return creat(file, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}


static int redirect_in(char *file)
{
    return open(file, O_RDONLY);
}


static int redirect_out_app(char *file)
{
    return open(file, O_CREAT | O_APPEND | O_WRONLY,
                    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}


static int get_heredoc(char *end, int no_tab)
{
    int heredoc = open("/tmp/", O_TMPFILE | O_RDWR,
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    FILE *file_heredoc = fdopen(dup(heredoc), "w");
    char *line = NULL;
    int begin = 0;

    while ((line = get_next_line("> ")) && strcmp(line, end))
    {
        if (no_tab)
        {
            for (begin = 0; line[begin] == '\t'; begin++)
                continue;
        }

        fprintf(file_heredoc, "%s\n", line + begin);
        mfree(line);
    }

    mfree(line);
    rewind(file_heredoc);
    fclose(file_heredoc);

    return heredoc;
}


static int redirect_in_app(char *file)
{
    return get_heredoc(file, 0);
}


static int redirect_in_app_nt(char *file)
{
    return get_heredoc(file, 1);
}


static int redirect_dup(char *word)
{
    if (word[0] == '-')
        return open("/dev/null", O_WRONLY);

    return -(word[0] - '0') - 2;
}


static int redirect_out_force(char *file)
{
    return creat(file, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}


static int redirect_both(char *file)
{
    return open(file, O_CREAT | O_RDWR,
                    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
}


struct redirect_config
{
    int default_io_number;
    int (*get_fd)(char *word);
};


struct redirect_config all_redirect[] =
{
    { STDOUT_FILENO, redirect_out },
    { STDIN_FILENO, redirect_in },
    { STDIN_FILENO, redirect_in_app },
    { STDOUT_FILENO, redirect_out_app },
    { STDIN_FILENO, redirect_in_app_nt },
    { STDOUT_FILENO, redirect_dup },
    { STDIN_FILENO, redirect_dup },
    { STDOUT_FILENO, redirect_out_force },
    { STDIN_FILENO, redirect_both }
};


static inline struct redirect_config *get_config(enum token_type redirect_type)
{
    return &all_redirect[redirect_type - TOKEN_REDIRECT_OUT];
}


static int set_redirect(struct lexer *lexer, struct ast *ast, int io_number,
                        enum token_type redirect_type)
{
    struct redirect_config *redirect = get_config(redirect_type);

    if (io_number < 0)
        io_number = redirect->default_io_number;

    struct token *end = lexer_pop(lexer);
    if (end->type != TOKEN_WORD)
    {
        token_destroy(end);
        return -1;
    }
    int fd = redirect->get_fd(end->value);
    if (fd == -1)
    {
        warn("Redirect errr: %s", end->value);
        fd = REDIRECT_WRG_FILE;
    }

    token_destroy(end);

    set_fd(ast, io_number, fd);

    return (fd != -1) ? 1 : -1;
}


static int get_io_number(char *str)
{
    if (str[0] < '0' || str[0] > '2' || str[1] != '\0')
        return -1;
    return str[0] - '0';
}


static int is_redirection(enum token_type type)
{
    return (type >= TOKEN_REDIRECT_OUT && type <= TOKEN_REDIRECT_BOTH);
}

int parse_redirection(struct lexer *lexer, struct ast *ast)
{
#ifdef DEBUG
    warnx("Parser: Enter redirection");
#endif /* DEBUG */

    int io_number = -1;

    if (lexer_peek(lexer) == TOKEN_IO_NUMBER)
    {
        struct token *token = lexer_pop(lexer);
        io_number = get_io_number(token->value);
        token_destroy(token);
    }

    if (!is_redirection(lexer_peek(lexer)))
        return 0;

    enum token_type redirect_type = lexer_peek(lexer);
    token_destroy(lexer_pop(lexer));

    return set_redirect(lexer, ast, io_number, redirect_type);
}


struct ast *parse_pipeline(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter pipeline");
#endif /* DEBUG */
    if (lexer_peek(lexer) == TOKEN_NOT)
        token_destroy(lexer_pop(lexer));

    struct ast *ast = parse_command(lexer);
    if (ast == NULL)
        return handle_error(ast);

    if (lexer_peek(lexer) == TOKEN_PIPE)
    {
        struct ast *pipeline = ast_init(AST_NODE_PIPE);
        pipeline->statement.pipeline = mcalloc(1, sizeof(struct pipeline));
        pipeline->statement.pipeline->left = ast;
        ast = pipeline;

        token_destroy(lexer_pop(lexer));

        while (lexer_peek(lexer) == TOKEN_ENDLINE)
            token_destroy(lexer_pop(lexer));

        ast->statement.pipeline->right = parse_pipeline(lexer);
    }

    return ast;
}


