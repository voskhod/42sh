#include "parser/parser.h"


struct ast *parse_and_or(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter and_or");
#endif /* DEBUG */
    struct ast *ast = parse_pipeline(lexer);
    struct ast *tmp = ast;
    if (ast == NULL)
        return NULL;

    while (lexer_peek(lexer) == TOKEN_AND || lexer_peek(lexer) == TOKEN_OR)
    {
        ast = ast_init(AST_NODE_AND_OR);
        ast->statement.and_or_node = mcalloc(1, sizeof(struct and_or_node));
        ast->statement.and_or_node->left = tmp;

        if (lexer_peek(lexer) == TOKEN_AND)
            ast->statement.and_or_node->type = AND_TYPE;
        else
            ast->statement.and_or_node->type = OR_TYPE;
        token_destroy(lexer_pop(lexer));

        while (lexer_peek(lexer) == TOKEN_ENDLINE)
            token_destroy(lexer_pop(lexer));

        ast->statement.and_or_node->right = parse_pipeline(lexer);
        if (ast->statement.and_or_node->right == NULL)
            return handle_error(ast);
        tmp = ast;
    }
    return ast;
}


struct ast *parse_command(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter command");
#endif /* DEBUG */

    struct ast *ast = NULL;

    ast = parse_simple_command(lexer);

    if (ast != NULL)
        return ast;

    ast = parse_shell_command(lexer);
    if (ast != NULL)
    {
        int success = 1;
        while (success == 1)
            success = parse_redirection(lexer, ast);

        if (success == -1)
            return handle_error(ast);

        return ast;
    }


    if (ast == NULL)
        ast = parse_fundec(lexer);

    if (ast == NULL)
        return handle_error(ast);

    return ast;
}


struct ast *parse_simple_command(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter simple_command");
#endif /* DEBUG */

    struct ast *cmd = ast_init(AST_NODE_COMMAND);
    cmd->statement.cmd = command_init();

    struct ast *prefix;
    if ((prefix = parse_prefix(lexer, cmd)))
    {
        cmd = prefix;
        while ((prefix = parse_prefix(lexer, cmd)))
            cmd = prefix;

        return cmd;
    }

    while ((prefix = parse_prefix(lexer, cmd)))
        cmd = prefix;


    if (parse_element(lexer, cmd) != 1)
    {
        ast_destroy(cmd);
        return NULL;
    }

    int success;
    while ((success = parse_element(lexer, cmd)) == 1)
        continue;

    if (success == -1)
        return handle_error(cmd);

    return cmd;
}


struct ast *parse_prefix(struct lexer *lexer, struct ast *cmd)
{
#ifdef DEBUG
    warnx("Parser: Enter prefix");
#endif /* DEBUG */

    if (lexer_peek(lexer) == TOKEN_ASSIGN_WORD)
    {
        struct ast *assign = ast_init(AST_NODE_VAR_DEF);
        assign->statement.var_def= mcalloc(1, sizeof(struct var_def));

        struct token *token = lexer_pop(lexer);
        assign->statement.var_def->var_def = strdup(token->value);
        token_destroy(token);

        assign->next = cmd;

        return assign;
    }

    if (parse_redirection(lexer, cmd) != 1)
        return NULL;
    return cmd;
}


int parse_element(struct lexer *lexer, struct ast *cmd)
{
#ifdef DEBUG
    warnx("Parser: Enter element");
#endif /* DEBUG */

    struct token *token = NULL;
    if (lexer_peek(lexer) == TOKEN_WORD
            ||  lexer_peek(lexer) == TOKEN_ASSIGN_WORD)
    {
        token = lexer_pop(lexer);
        command_append(cmd->statement.cmd, token->value);
        token_destroy(token);

        return 1;
    }
    return parse_redirection(lexer, cmd);
}


struct ast *parse_shell_command(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter shell_command");
#endif /* DEBUG */

    struct ast *ast = NULL;

    if (lexer_peek(lexer) == TOKEN_LEFT_PARENTHESIS
            || lexer_peek(lexer) == TOKEN_LEFT_BRACKET)
    {
        token_destroy(lexer_pop(lexer));

        ast = parse_compound_list_break(lexer);
        if (ast == NULL)
            return NULL;

        if (lexer_peek(lexer) != TOKEN_RIGHT_PARENTHESIS
                && lexer_peek(lexer) != TOKEN_RIGHT_BRACKET)
            return handle_error(ast);

        token_destroy(lexer_pop(lexer));

        return ast;
    }


    ast = parse_rule_if(lexer);
    if (ast != NULL)
        return ast;

    ast = parse_rule_while(lexer);
    if (ast != NULL)
        return ast;

    ast = parse_rule_until(lexer);
    if (ast != NULL)
        return ast;

    ast = parse_rule_for(lexer);
    if (ast != NULL)
        return ast;

    ast = parse_rule_case(lexer);
    if (ast != NULL)
        return ast;

    return ast;
}


struct ast *parse_fundec(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter fundec");
#endif /* DEBUG */

    if (lexer_peek(lexer) == TOKEN_FUNCTION)
        token_destroy(lexer_pop(lexer));

    if (lexer_peek(lexer) != TOKEN_FUN_NAME)
        return NULL;

    struct token *fun_dec = lexer_pop(lexer);
    char *fun_name = strdup(fun_dec->value);
    token_destroy(fun_dec);

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    struct ast *fun_body = parse_shell_command(lexer);
    if (fun_body == NULL)
    {
        mfree(fun_name);
        return NULL;
    }

    int success = 1;
    while (success == 1)
        success = parse_redirection(lexer, fun_body);

    if (success == -1)
    {
        mfree(fun_name);
        ast_destroy(fun_body);
        return NULL;
    }

    struct ast *ast = ast_init(AST_NODE_FUN);
    ast->statement.function = mcalloc(1, sizeof(struct function));
    ast->statement.function->name = fun_name;
    ast->statement.function->fun_body = fun_body;

    return ast;
}
