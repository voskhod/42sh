#include "parser/parser.h"
#include "interface/input.h"
#include "interface/readline.h"

struct ast *handle_error(struct ast *ast)
{
    ast_destroy(ast);
    return NULL;
}


struct ast *syntax_error(struct lexer *lexer, struct ast *ast)
{
    input_reset(lexer->input);
    ast_destroy(ast);
    if (!is_interactive())
        errx(EXIT_SYNTAX_ERR, "Syntax Error");
    warnx("Syntax Error");
    return NULL;
}

