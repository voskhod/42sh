/**
** \file statement.h
**
** This unit contains all the functions needed to handle the different type
** of statement use in the AST.
*/
#ifndef STATEMENT_H
#define STATEMENT_H

#include "parser/ast.h"

/**
** \fn void destroy_statement(struct ast *ast_node)
** \brief Destroy a statement.
**
** \param ast_node The AST node containing the statement to destroy.
*/
void statement_destroy(struct ast *ast_node);

/**
** \fn void case_item_destroy(struct case_item *item)
** \brief Destroy a case_item.
**
** \param item The case_item to destroy
*/
void case_item_destroy(struct case_item *item);

#endif /* STATEMENT_H */
