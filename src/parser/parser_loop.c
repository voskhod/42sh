#include "parser/parser.h"


struct ast *parse_rule_while(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter while");
#endif
    if (lexer_peek(lexer) != TOKEN_WHILE)
        return NULL;
    token_destroy(lexer_pop(lexer));
    struct ast *ast_while = ast_init(AST_NODE_WHILE);
    ast_while->statement.while_node = mcalloc(1, sizeof(struct while_node));
    ast_while->statement.while_node->cond = parse_compound_list_break(lexer);
    if (ast_while->statement.while_node->cond == NULL)
        return handle_error(ast_while);
    ast_while->statement.while_node->body = parse_do_group(lexer);
    if (ast_while->statement.while_node->body == NULL)
        return handle_error(ast_while);
    return ast_while;
}


struct ast *parse_rule_until(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter until");
#endif
    if (lexer_peek(lexer) != TOKEN_UNTIL)
        return NULL;
    token_destroy(lexer_pop(lexer));

    struct ast *ast_until = ast_init(AST_NODE_UNTIL);
    ast_until->statement.until_node = mcalloc(1, sizeof(struct until_node));
    ast_until->statement.until_node->cond = parse_compound_list_break(lexer);
    if (ast_until->statement.until_node->cond == NULL)
        return NULL;
    ast_until->statement.until_node->body = parse_do_group(lexer);
    if (ast_until->statement.until_node->body == NULL)
        return NULL;
    return ast_until;
}


struct ast *parse_rule_for(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter for");
#endif
    if (lexer_peek(lexer) != TOKEN_FOR)
        return NULL;
    token_destroy(lexer_pop(lexer));
    struct ast *ast_for = ast_init(AST_NODE_FOR);
    ast_for->statement.for_node = mcalloc(1, sizeof(struct for_node));
    ast_for->statement.for_node->vector = vector_init();
    struct token *token = NULL;

    if (lexer_peek(lexer) != TOKEN_WORD)
        return handle_error(ast_for);

    token = lexer_pop(lexer);
    ast_for->statement.for_node->var = strdup(token->value);
    token_destroy(token);

    if (lexer_peek(lexer) == TOKEN_SEMI_COLON)
    {
        token_destroy(lexer_pop(lexer));
        while (lexer_peek(lexer) == TOKEN_ENDLINE)
            token_destroy(lexer_pop(lexer));
        ast_for->statement.for_node->body = parse_do_group(lexer);
        if (ast_for->statement.for_node->body == NULL)
            return handle_error(ast_for);
        return ast_for;
    }

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    if (lexer_peek(lexer) != TOKEN_IN)
        return handle_error(ast_for);

    token_destroy(lexer_pop(lexer));

    while (lexer_peek(lexer) == TOKEN_WORD)
    {
        token = lexer_pop(lexer);
        vector_append(ast_for->statement.for_node->vector, token->value);
        token_destroy(token);
    }
    if (lexer_peek(lexer) != TOKEN_ENDLINE
                && lexer_peek(lexer) != TOKEN_SEMI_COLON)
        return handle_error(ast_for);

    token_destroy(lexer_pop(lexer));

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    ast_for->statement.for_node->body = parse_do_group(lexer);
    if (ast_for->statement.for_node->body == NULL)
        return handle_error(ast_for);

    return ast_for;
}


struct ast *parse_do_group(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter do_group");
#endif
    if (lexer_peek(lexer) != TOKEN_DO)
        return NULL;
    token_destroy(lexer_pop(lexer));

    struct ast *ast = parse_compound_list_break(lexer);
    if (ast == NULL)
        return handle_error(ast);

    if (lexer_peek(lexer) != TOKEN_DONE)
        return handle_error(ast);

    token_destroy(lexer_pop(lexer));
    return ast;
}

