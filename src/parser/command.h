/**
** \file command.h
** \brief A program to execute with args.
*/
#ifndef COMMAND_H
#define COMMAND_H

#include <stddef.h>

/**
** \struct command
** \brief The program to execute with args.
*/
struct command
{
    char **argv;
    size_t size;
    size_t capacity;
};


/**
** \fn struct command *command_init(void)
** \brief Initialize a command.
**
** \return The initialized command
*/
struct command *command_init(void);

/**
** \fn void command_destroy(struct command *cmd)
** \brief Destroy a command_destroy.
**
** \pararm cmd The command to destroy
*/
void command_destroy(struct command *cmd);

/**
** \fn void command_append(struct command *cmd, char *str)
** \brief Append a word to a command.
**
** \param cmd A command initialized by command_init
** \param str The word to add to command
**
** This function add str at the end of the string at argv[0].
*/
void command_append(struct command *cmd, char *str);

/**
** \fn void command_append(struct command *cmd, char *str)
** \brief Append a word to a command.
**
** \param cmd A command initialized by command_init
** \param str The word to add to command
**
** This function add str at the next argv[n] free.
*/
void command_append_field(struct command *cmd, char *str);

#endif /* COMMAND_H */
