#include "utilities/utilities.h"
#include "parser/statement.h"
#include "parser/ast.h"

static void destroy_statement_command(struct ast *ast_node)
{
    command_destroy(ast_node->statement.cmd);
}


static void destroy_statement_if(struct ast *ast_node)
{
    ast_destroy(ast_node->statement.if_node->cond);
    ast_destroy(ast_node->statement.if_node->if_body);
    ast_destroy(ast_node->statement.if_node->else_body);

    mfree(ast_node->statement.if_node);
}


static void destroy_statement_and_or(struct ast *ast_node)
{
    ast_destroy(ast_node->statement.and_or_node->left);
    ast_destroy(ast_node->statement.and_or_node->right);

    mfree(ast_node->statement.and_or_node);
}


static void destroy_statement_while(struct ast *ast_node)
{
    ast_destroy(ast_node->statement.while_node->cond);
    ast_destroy(ast_node->statement.while_node->body);

    mfree(ast_node->statement.while_node);
}


static void destroy_statement_list_break(struct ast *ast_node)
{
    if (ast_node == NULL)
        return;

    struct compound_list_break *to_delete;
    struct compound_list_break *cur = ast_node->statement.cpd_list_break;

    while (cur != NULL)
    {
        to_delete = cur;
        cur = cur->next;

        ast_destroy(to_delete->current);
        mfree(to_delete);
    }
}


static void destroy_statement_until(struct ast *ast_node)
{
    ast_destroy(ast_node->statement.until_node->cond);
    ast_destroy(ast_node->statement.until_node->body);

    mfree(ast_node->statement.until_node);
}


static void destroy_statement_for(struct ast *ast_node)
{
    vector_destroy(ast_node->statement.for_node->vector);
    mfree(ast_node->statement.for_node->var);

    ast_destroy(ast_node->statement.for_node->body);
    mfree(ast_node->statement.for_node);
}


static void destroy_statement_pipe(struct ast *ast_node)
{
    ast_destroy(ast_node->statement.pipeline->left);
    ast_destroy(ast_node->statement.pipeline->right);

    mfree(ast_node->statement.pipeline);
}


static void destroy_statement_case(struct ast *ast_node)
{
    mfree(ast_node->statement.case_node->value);
    case_item_destroy(ast_node->statement.case_node->item);

    mfree(ast_node->statement.case_node);
}


static void destroy_statement_fun(struct ast *ast_node)
{
    mfree(ast_node->statement.function);
}


static void destroy_var_def(struct ast *ast_node)
{
    mfree(ast_node->statement.var_def->var_def);
    mfree(ast_node->statement.var_def);
}


void statement_destroy(struct ast *ast_node)
{
    void( *destroy_statement_list[])(struct ast *) =
    {
        destroy_statement_command,
        destroy_statement_list_break,
        destroy_statement_if,
        destroy_statement_and_or,
        destroy_statement_while,
        destroy_statement_until,
        destroy_statement_pipe,
        destroy_statement_for,
        destroy_statement_case,
        destroy_statement_fun,
        destroy_var_def,
    };

    destroy_statement_list[ast_node->type](ast_node);
}


void case_item_destroy(struct case_item *item)
{
    if (item == NULL)
        return;
    case_item_destroy(item->next);
    vector_destroy(item->vector);
    ast_destroy(item->body);
    mfree(item);
}

