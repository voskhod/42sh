#include "parser/parser.h"
#include "interface/input.h"

struct ast *parse_input(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter input");
#endif /* DEBUG */

    switch_shell_prompt();

    if (lexer_peek(lexer) == TOKEN_EOF || lexer_peek(lexer) == TOKEN_ENDLINE)
    {
        switch_shell_prompt();
        token_destroy(lexer_pop(lexer));
        return NULL;
    }

    struct ast *ast = parse_list(lexer);
    if (ast == NULL)
    {
        switch_shell_prompt();
        return syntax_error(lexer, ast);
    }

    if (lexer_peek(lexer) != TOKEN_ENDLINE && lexer_peek(lexer) != TOKEN_EOF)
    {
        switch_shell_prompt();
        return syntax_error(lexer, ast);
    }

    token_destroy(lexer_pop(lexer));

    switch_shell_prompt();

    return ast;
}


struct ast *parse_list(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter list");
#endif /* DEBUG */

    struct ast *ast = parse_and_or(lexer);
    struct ast *current = ast;
    if (ast == NULL)
        return NULL;

    while (lexer_peek(lexer) == TOKEN_SEMI_COLON
            || lexer_peek(lexer) == TOKEN_BG)
    {
        while (current->next != NULL)
            current = current->next;

        if (lexer_peek(lexer) == TOKEN_BG)
            current->is_async = 1;

        token_destroy(lexer_pop(lexer));
        current->next = parse_and_or(lexer);

        if (current->next == NULL)
            return ast;
        current = current->next;
    }

    if (lexer_peek(lexer) == TOKEN_BG)
        current->is_async = 1;

    if (lexer_peek(lexer) == TOKEN_SEMI_COLON
            || lexer_peek(lexer) == TOKEN_BG)
        token_destroy(lexer_pop(lexer));

    return ast;
}


struct ast *parse_compound_list_break(struct lexer *lexer)
{
#ifdef DEBUG
    warnx("Parser: Enter list_break");
#endif /* DEBUG */

    while (lexer_peek(lexer) == TOKEN_ENDLINE)
        token_destroy(lexer_pop(lexer));

    struct ast *ast = ast_init(AST_NODE_LIST_BREAK);
    ast->statement.cpd_list_break = mcalloc(1,
            sizeof(struct compound_list_break));
    ast->statement.cpd_list_break->current = parse_and_or(lexer);


    struct compound_list_break *current = ast->statement.cpd_list_break;

    if (ast->statement.cpd_list_break->current == NULL)
        return handle_error(ast);

    while (lexer_peek(lexer) == TOKEN_ENDLINE || lexer_peek(lexer) == TOKEN_BG
            || lexer_peek(lexer) == TOKEN_SEMI_COLON)
    {
        token_destroy(lexer_pop(lexer));
        while (lexer_peek(lexer) == TOKEN_ENDLINE)
            token_destroy(lexer_pop(lexer));

        current->next = mcalloc(1, sizeof(struct compound_list_break));
        current->next->current = parse_and_or(lexer);
        if (current->next->current == NULL)
        {
            while (lexer_peek(lexer) == TOKEN_ENDLINE)
                token_destroy(lexer_pop(lexer));
            mfree(current->next);
            current->next = NULL;
            return ast;
        }

        current = current->next;
    }
    if (lexer_peek(lexer) == TOKEN_ENDLINE || lexer_peek(lexer) == TOKEN_BG
            || lexer_peek(lexer) == TOKEN_SEMI_COLON)
    {
        token_destroy(lexer_pop(lexer));
        while (lexer_peek(lexer) == TOKEN_ENDLINE)
            token_destroy(lexer_pop(lexer));
    }
    return ast;
}

