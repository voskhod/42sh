#include <string.h>
#include <stdio.h>

#include "parser/command.h"
#include "utilities/utilities.h"

#define CMD_CAPACITY 16


struct command *command_init(void)
{
    struct command *cmd = mcalloc(1, sizeof(struct command));
    cmd->capacity = CMD_CAPACITY;
    cmd->argv = mcalloc(CMD_CAPACITY, sizeof(char *));

    return cmd;
}


void command_destroy(struct command *cmd)
{
    if (!cmd)
        return;

    for (size_t i = 0; i < cmd->size; i++)
        mfree(cmd->argv[i]);
    mfree(cmd->argv);
    mfree(cmd);
}


void command_append(struct command *cmd, char *str)
{
    if (cmd->argv[0] != NULL)
    {
        char *new_line = NULL;
        asprintf(&new_line, "%s %s", cmd->argv[0], str);

        mfree(cmd->argv[0]);
        cmd->argv[0] = new_line;
    }
    else
        cmd->argv[0] = strdup(str);

    cmd->size = 1;
}

void command_append_field(struct command *cmd, char *str)
{
    if (cmd->size + 1 >= cmd->capacity)
    {
        cmd->capacity *= 2;
        cmd->argv = mrealloc(cmd->argv, cmd->capacity * sizeof(char *));
    }

    cmd->argv[cmd->size] = str;
    cmd->argv[cmd->size + 1] = NULL;
    cmd->size++;
}
