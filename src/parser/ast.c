#include <stddef.h>
#include <unistd.h>

#include "parser/ast.h"
#include "parser/statement.h"
#include "utilities/utilities.h"


struct ast *ast_init(enum ast_node_type type)
{
    struct ast *node = mcalloc(1, sizeof(struct ast));
    node->type = type;
    node->fd_in = -1;
    node->fd_out = -1;
    node->fd_err = -1;

    return node;
}


static void close_open_fd(struct ast *ast_node)
{
    if (ast_node->fd_in > -1)
        close(ast_node->fd_in);

    if (ast_node->fd_out > -1)
        close(ast_node->fd_out);

    if (ast_node->fd_out > -1)
        close(ast_node->fd_out);
}

void ast_node_destroy(struct ast *ast_node)
{
    if (ast_node == NULL)
        return;

    close_open_fd(ast_node);
    statement_destroy(ast_node);

    mfree(ast_node);
}


void ast_destroy(struct ast *ast)
{
    if (ast == NULL)
        return;

    ast_destroy(ast->next);
    ast_node_destroy(ast);
}
