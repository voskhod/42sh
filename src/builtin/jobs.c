#include <stdlib.h>

#include "builtin/jobs.h"
#include "execute/jobs_ctrl.h"


int exe_jobs(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    jobs_ctrl_print_all();
    return 0;
}


int exe_fg(int argc, char *argv[])
{
    int job_id = -1;
    if (argc >= 2)
        job_id = atoi(argv[1]);

    if (jobs_ctrl_fg(job_id))
        return 1;

    return 0;
}


int exe_bg(int argc, char *argv[])
{
    int job_id = -1;
    if (argc >= 2)
        job_id = atoi(argv[1]);

    if (jobs_ctrl_bg(job_id))
        return 1;

    return 0;
}
