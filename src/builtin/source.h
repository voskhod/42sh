/**
** \file source.h
** \brief Execute the source builtin.
*/
#ifndef SOURCE_H
#define SOURCE_H

/**
** \fn int exe_source(int argc, char *argv[])
** \brief Execute the source builtin.
**
** \param argc The number of strings in argv
** \parma argv The arguments array
** \return The return value of source builtin
*/
int exe_source(int argc, char *argv[]);

#endif /* SOURCE_H */
