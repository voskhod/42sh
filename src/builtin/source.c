#include "builtin/source.h"
#include "execute/exec_from_input.h"
#include "builtin/shopt.h"
#include "utilities/utilities.h"
#include <err.h>
#include <stdio.h>

#include <readline/readline.h>

int exe_source(int argc, char *argv[])
{
    if (argc < 2)
    {
        warnx("source: filename argument required");
        return 2;
    }
    else
    {
        int back = 0;
        int i = 0;
        while (argv[1][i] != '\0' && back == 0)
        {
            if (argv[1][i] == '/')
            {
                back++;
            }
            i++;
        }
        if (back == 0)
        {
            warnx("%s: No such file or directory", argv[1]);
            return 1;
        }
        else
        {
            int return_value = execute_from_file(argv[1], g_shopt->ast_print);
            if (return_value == 127)
            {
                warnx("%s: No such file or directory", argv[1]);
                return 1;
            }
            else
                return 0;
        }
    }
}
