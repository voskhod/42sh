#include "builtin/unalias.h"
#include "builtin/alias.h"
#include "utilities/utilities.h"
#include <string.h>
#include <err.h>
#include <stdio.h>

static int opt_unalias;

static int position;

static int arg_unalias(char *argv)
{
    opt_unalias = 0;
    if (argv[1] == '\0')
        return 3;
    int i = 1;
    for (; argv[i] != '\0'; i++)
    {
        if (argv[i] == 'a')
            opt_unalias = 1;
        else
            break;
    }
    if (argv[i] == '\0')
        return 0;
    else
    {
        warnx("%c: invalid argument", argv[i]);
        return 2;
    }
}

static int unalias_init(char *argv)
{
    struct alias *alias_next = alias_list;
    struct alias *alias_prev = NULL;
    while (alias_next != NULL)
    {
        if (!strcmp(alias_next->alias, argv))
        {
            if (alias_prev == NULL)
            {
                alias_list = alias_list->next;
                mfree(alias_next->alias);
                mfree(alias_next->value);
                mfree(alias_next);
                return 0;
            }
            else
            {
                alias_prev->next = alias_next->next;
                mfree(alias_next->alias);
                mfree(alias_next->value);
                mfree(alias_next);
                return 0;
            }
            return 0;
        }
        else
        {
            alias_prev = alias_next;
            alias_next = alias_next->next;
        }
    }
    warnx("unalias: %s: not found", argv);
    return 1;
}

int exe_unalias(int argc, char *argv[])
{
    position = 1;
    if (argc == 1)
    {
        warnx("unalias: usage: unalias [-a] name [name ...]");
        return 2;
    }
    else
    {
        int arg = 0;
        while (position < argc && argv[position][0] == '-')
        {
            arg = arg_unalias(argv[position]);
            if (arg != 3)
                position++;
            else
                break;
        }
        if (arg == 1)
            return 1;
        if (opt_unalias == 1)
        {
            delete_alias_list();
            return 0;
        }
        int return_value = 0;
        while (position < argc)
        {
            return_value += unalias_init(argv[position]);
            position++;
        }
        if (return_value != 0)
            return 1;
        else
            return 0;
    }
    return 0;
}
