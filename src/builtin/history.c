#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utilities/utilities.h"
#include "builtin/history.h"

static int arg_parse(char *argv, struct history_opt *hopt)
{
    if (!strcmp(argv, "-c"))
        hopt->hopt_c = 1;

    else if (!strcmp(argv, "-r"))
        hopt->hopt_r = 1;

    else
    {
        warnx("%s: invalide argument", argv);
        return EXIT_WRG_H_ARG;
    }
    return 0;
}

static int clean_history(void)
{
    FILE *fd;
    char *home = getenv( "HOME" );
    char *path;
    asprintf(&path, "%s/.42sh_history", home);
    fd = fopen(path, "w+");
    mfree(path);
    if (fd != NULL)
    {
        fclose(fd);
        return 0;
    }
    else
    {
        warnx("cannot open file .42sh_history");
        return 1;
    }
}

static int add_file_history(char *argv)
{
    FILE *fd;
    char *home = getenv( "HOME" );
    char *path;
    asprintf(&path, "%s/.42sh_history", home);
    fd = fopen(path, "a");
    mfree(path);
    if (fd != NULL)
    {
        FILE *fo;
        if ((fo = fopen(argv, "r")) == NULL)
        {
            fclose(fd);
            return 1;
        }
        else
        {
            char *line = NULL;
            size_t len = 0;
            while (getline(&line, &len, fo) != -1)
            {
                fprintf(fd, "%s", line);
            }
            mfree(line);
            fclose(fo);
            fclose(fd);
            return 0;
        }
    }
    else
    {
        warnx("cannot open file .42sh_history");
        return 1;
    }
}

static int print_history(void)
{
    FILE *fd;
    char *home = getenv( "HOME" );
    char *path;
    asprintf(&path, "%s/.42sh_history", home);
    fd = fopen(path, "r");
    mfree(path);
    if (fd != NULL)
    {
        char *str = NULL;
        size_t len = 0;
        int line = 1;
        while (getline(&str, &len, fd) != -1)
        {
            printf("   %d  %s", line, str);
            line++;
        }
        mfree(str);
        fclose(fd);
        return 0;
    }
    else
    {
        warnx("can't open file 42sh_history");
        return 1;
    }
}

static int history_chose(int argc, char *argv[], int *position, struct history_opt *hopt)
{
    if (!(hopt->hopt_c || hopt->hopt_r))
    {
        warnx("%s: numeric argument required", argv[*position]);
        return EXIT_WRG_H_ARG;
    }
    else
    {
        int return_val = 0;

        if (hopt->hopt_c)
            return_val = clean_history();

        if (hopt->hopt_r && *position < argc)
            return_val = add_file_history(argv[*position]);

        return return_val;
    }
}

int exe_history(int argc, char *argv[])
{
    int position = 1;
    struct history_opt *hopt;
    int return_val = 0;
    hopt = mcalloc(1, sizeof(struct history_opt));
    if (argc == 1)
        return_val = print_history();
    else
    {
        while (position < argc && return_val == 0)
        {
            if (argv[position][0] == '-')
            {
                return_val = arg_parse(argv[position], hopt);
                position++;
            }
            else
                break;
        }
        if (return_val == 0)
            return_val = history_chose(argc, argv, &position, hopt);
    }
    mfree(hopt);
    return return_val;
}
