/**
** \file echo.h
** \brief The echo builtin.
*/
#ifndef ECHO_H
#define ECHO_H

/**
** \struct opt_echo
** \brief The options of echo builtin
*/
struct opt_echo
{
    int opt_n;
    int opt_e;
    int opt_me;
};

/**
** \fn int exe_echo(int argc, char *argv[])
** \brief execute the echo builtin.
**
** \param argc The number of strings in argv
** \param argv The arguments array
** \return The expected value
*/
int exe_echo(int argc, char *argv[]);

#endif /* ECHO_H */
