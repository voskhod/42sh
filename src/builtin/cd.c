#include "execute/variable.h"
#include "builtin/cd.h"
#include "utilities/utilities.h"

#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <string.h>
#include <unistd.h>


static int exe_cd_path(char *argv)
{
    char *new_old;
    new_old = get_current_dir_name();
    if (new_old == NULL)
        return 1;
    int return_val = chdir(argv);
    if (return_val == -1)
    {
        mfree(new_old);
        warnx("cd: no such file or directory");
        return 1;
    }
    else
    {
        variable_set(strdup("OLDPWD"), new_old);

        return 0;
    }
}

int exe_cd(int argc, char *argv[])
{
    if (argc <= 2)
    {
        if (argc == 1)
        {
            char *path = getenv("HOME");
            return exe_cd_path(path);
        }
        else
        {
            if (!strcmp(argv[1], "-"))
            {
                char *old_pwd = variable_get("OLDPWD");
                if (old_pwd[0] == '\0')
                {
                    warnx("cd: \" OLDPWD \" undefined");
                    return 1;
                }
                else
                {
                    int val = exe_cd_path(old_pwd);
                    printf("%s\n", old_pwd);
                    return val;
                }
            }
            else
                return exe_cd_path(argv[1]);
        }
    }
    else
    {
        warnx("cd: too many arguments");
        return 1;
    }
}
