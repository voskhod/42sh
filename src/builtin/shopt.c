#include <err.h>
#include <string.h>
#include <stdio.h>
#include "interface/option.h"
#include "utilities/utilities.h"
#include "builtin/shopt.h"

struct shopt *g_shopt = NULL;

static int add_opt(char *argv, struct shopt_option *shopt_opt)
{
    if (!strcmp(argv, "-q"))
        shopt_opt->opt_q = 1;

    else if (!strcmp(argv, "-u"))
    {
        if (shopt_opt->opt_s == 1)
        {
            warnx("cannot set and unset 42sh options simultaneously");
            return EXIT_SET_UNSET_ERROR;
        }
        shopt_opt->opt_u = 1;
    }

    else if (!strcmp(argv, "-s"))
    {
        if (shopt_opt->opt_u == 1)
        {
            warnx("cannot set and unset 42sh options simultaneously");
            return EXIT_SET_UNSET_ERROR;
        }
        shopt_opt->opt_s = 1;
    }

    else
    {
        warnx("%s: invalid option", argv);
        return EXIT_WRG_OPT;
    }
    return 0;
}

static inline const char *bool_to_str(int b)
{
    return b ? "on" : "off";
}

static void print_shopt_all(int b)
{
    if (g_shopt->ast_print == b || b == -1)
        printf("ast_print       %s\n", bool_to_str(g_shopt->ast_print));

    if (g_shopt->dotglob == b || b == -1)
        printf("dotglob         %s\n", bool_to_str(g_shopt->dotglob));

    if (g_shopt->expand_aliases == b || b == -1)
        printf("expand_aliases  %s\n", bool_to_str(g_shopt->expand_aliases));

    if (g_shopt->extglob == b || b == -1)
        printf("extglob         %s\n", bool_to_str(g_shopt->extglob));

    if (g_shopt->nocaseglob == b || b == -1)
        printf("nocaseglob      %s\n", bool_to_str(g_shopt->nocaseglob));

    if (g_shopt->nullglob == b || b == -1)
        printf("nullglob        %s\n", bool_to_str(g_shopt->nullglob));

    if (g_shopt->sourcepath == b || b == -1)
        printf("sourcepath      %s\n", bool_to_str(g_shopt->sourcepath));

    if (g_shopt->xpg_echo == b || b == -1)
        printf("xpg_echo        %s\n", bool_to_str(g_shopt->xpg_echo));
}

static int add_option_print(struct shopt_option *shopt_opt)
{
    if (!shopt_opt->opt_q)
    {
        if (shopt_opt->opt_u)
            print_shopt_all(0);
        else
            print_shopt_all(1);
    }
    return 0;
}

static int o_and_a(int arg, int *g_val, struct value val, char *option)
{
    int return_value = 0;
    if (arg == 1)
    {
        if (val.i == -1)
        {
            if (val.arg)
                return_value += *g_val ? 0 : 1;
            else
                printf("%-15s\t%s\n", option, bool_to_str(*g_val));
        }
        else
            *g_val = val.i;
    }
    return return_value;
}

static int opt_and_arg_print(struct shopt *shopt, struct shopt_option *arg)
{
    struct value val;
    val.i = -1;
    if (arg->opt_u)
        val.i = 0;
    else if (arg->opt_s)
        val.i = 1;
    val.arg = arg->opt_q;
    int q_return = 0;
    q_return += o_and_a(shopt->ast_print, &g_shopt->ast_print, val,
                            "ast_print");
    q_return += o_and_a(shopt->dotglob, &g_shopt->dotglob, val, "dotglob");
    q_return += o_and_a(shopt->expand_aliases, &g_shopt->expand_aliases, val,
                            "expand_aliases");
    q_return += o_and_a(shopt->extglob, &g_shopt->extglob, val, "extglob");
    q_return += o_and_a(shopt->nocaseglob, &g_shopt->nocaseglob, val,
                            "nocaseglob");
    q_return += o_and_a(shopt->nullglob, &g_shopt->nullglob, val, "nullglob");
    q_return += o_and_a(shopt->sourcepath, &g_shopt->sourcepath, val,
                            "sourcepath");
    q_return += o_and_a(shopt->xpg_echo, &g_shopt->xpg_echo, val, "xpg_echo");
    mfree(shopt);
    return q_return ? 1 : 0;
}

static int opt_chose(int *position, int argc, char *argv[],
                        struct shopt_option *shopt_opt)
{
    struct shopt *opt_chose;
    opt_chose = mcalloc(1, sizeof(struct shopt));
    while (*position < argc)
    {
        if (!strcmp(argv[*position], "ast_print"))
            opt_chose->ast_print = 1;
        else if (!strcmp(argv[*position], "dotglob"))
            opt_chose->dotglob = 1;
        else if (!strcmp(argv[*position], "expand_aliases"))
            opt_chose->expand_aliases = 1;
        else if (!strcmp(argv[*position], "extglob"))
            opt_chose->extglob = 1;
        else if (!strcmp(argv[*position], "nocaseglob"))
            opt_chose->nocaseglob = 1;
        else if (!strcmp(argv[*position], "nullglob"))
            opt_chose->nullglob = 1;
        else if (!strcmp(argv[*position], "sourcepath"))
            opt_chose->sourcepath = 1;
        else if (!strcmp(argv[*position], "xpg_echo"))
            opt_chose->xpg_echo = 1;
        else
        {
            warnx("%s: invalid 42sh option name", argv[*position]);
            return EXIT_SHOPT_ERROR;
        }
        (*position)++;
    }
    return opt_and_arg_print(opt_chose, shopt_opt);
}

static int chose_print_shopt(int *position, int argc, char *argv[],
                        struct shopt_option *shopt_opt)
{
    if (argc == 1)
        print_shopt_all(-1);

    else if (*position >= argc)
        return add_option_print(shopt_opt);

    else
        return opt_chose(position, argc, argv, shopt_opt);

    return 0;
}

int exe_shopt(int argc, char *argv[])
{
    int position = 1;
    struct shopt_option *shopt_opt;
    shopt_opt = mcalloc(1, sizeof(struct shopt_option));
    int return_value = 0;
    if (argc != 1)
    {
        while (position < argc && argv[position][0] == '-' && return_value == 0)
        {
            return_value = add_opt(argv[position], shopt_opt);
            position++;
        }
    }
    if (return_value == 0)
        return_value = chose_print_shopt(&position, argc, argv, shopt_opt);
    mfree(shopt_opt);
    return return_value;
}
