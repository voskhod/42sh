/**
** \file exit.h
** \brief The exit builtin.
*/
#ifndef EXIT_H
#define EXIT_H

/**
** \fn int exe_exit(int argc, char *argv[])
** \brief The exit builtin.
**
** \param argc The number of string in argv
** \param argv The arguments, argv[0] contain "exit"
** \return This function never return
*/
int exe_exit(int argc, char *argv[]);

#endif /* EXIT_H */
