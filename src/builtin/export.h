#ifndef EXPORT_H
#define EXPORT_H

struct opt_export
{
    int opt_n;
    int opt_p;
};

int exe_export(int argc, char *argv[]);

#endif /* EXPORT_H */
