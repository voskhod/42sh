/**
** \file history.h
** \brief The history builtin.
*/
#ifndef HISTORY_H
#define HISTORY_H

/**
** \struct history_opt
** \brief The options of history builtin
*/
struct history_opt
{
    int hopt_c;
    int hopt_r;
};

/**
** \fn int exe_history(int argc, char *argv[])
** \brief Print, modify or clear the ~/.42sh_history.
**
** \param argc The number of strings in argv
** \param argv The arguments array
** \return The return value of history builtin
*/
int exe_history(int argc, char *argv[]);

#endif /* HISTORY_H */
