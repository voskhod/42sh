#include "builtin/echo.h"
#include "utilities/utilities.h"
#include <stdio.h>
#include <stdlib.h>

static struct opt_echo *opt_echo;

static int arg_echo(char *argv)
{
    if (argv[1] == '\0')
    {
        return 1;
    }
    int i = 1;
    for (; argv[i] != '\0'; i++)
    {
        if (argv[i] == 'e')
            opt_echo->opt_e = 1;
        else if (argv[i] == 'n')
            opt_echo->opt_n = 1;
        else if (argv[i] == 'E')
            opt_echo->opt_me = 1;
        else
            break;
    }
    if (argv[i] == '\0')
        return 0;
    else
    {
        opt_echo->opt_n = 0;
        opt_echo->opt_e = 0;
        opt_echo->opt_me = 0;
        return 1;
    }
}

static char char_maker(char p)
{
    if (p == 'n')
        return '\n';
    else if (p == 'a')
        return '\a';
    else if (p == 'b')
        return '\b';
    else if (p == 'c')
    {
        opt_echo->opt_n = 1;
        return 'c';
    }
    else if (p == 'f')
        return '\f';
    else if (p == 'r')
        return '\r';
    else if (p == 't')
        return '\t';
    else if (p == 'v')
        return '\v';
    else if (p == '\\')
        return '\\';
    else
        putchar('\\');
    return p;
}

static void print_x(char tab[2])
{
    int c = 0;
    int i_val = 0;
    for (int i = 0; i < 2; i++)
    {
        if ((tab[i] >= 'a' && tab[i] <= 'f') || (tab[i] >= 'A' && tab[i] <= 'F')
                || (tab[i] >= '0' && tab[i] <= '9'))
        {
            c++;
        }
        else
        {
            if (i == 0)
                i_val = 1;
        }
    }
    if (c == 2)
    {
        char *end;
        int hex = strtol(tab, &end, 16);
        printf("%c", hex);
    }
    else
    {
        if (i_val == 1)
            printf("\\x%s", tab);
        else
            printf("%c", tab[1]);
    }
}

static void print_echo_me(int argc, int position, char *argv[])
{
    while (position < argc)
    {
        int arg = 0;
        int count = 0;
        char tab[3] = { 0 };
        for (int i = 0; argv[position][i] != '\0'; i++)
        {
            char p = argv[position][i];
            if (count > 0)
            {
                if (count == 2)
                {
                    tab[0] = 0;
                    tab[1] = 0;
                    tab[2] = '\0';
                }
                tab[2 - count] = p;
                count--;
                if (count == 0)
                {
                    print_x(tab);
                }
            }
            else
            {
                if (p == '\\' && arg == 0)
                    arg++;
                else if (arg == 1)
                {
                    if (p != 'x')
                    {
                        p = char_maker(p);
                        if (p != 'c')
                            putchar(p);
                    }
                    else
                    {
                        if (argv[position][i + 1] != '\0')
                            count = 2;
                        else
                            printf("\\x");
                    }
                    arg = 0;
                }
                else
                    putchar(p);
            }
        }
        if (count > 0)
        {
            print_x(tab);
        }
        position++;
        if (position < argc)
            putchar(' ');
    }
}

static void print_echo(int argc, int position, char *argv[])
{
    if (opt_echo->opt_me || !opt_echo->opt_e)
    {
        while (position < argc)
        {
            int arg = 0;
            for (int i = 0; argv[position][i] != '\0'; i++)
            {
                char p = argv[position][i];
                if (p == '\\')
                {
                    arg++;
                    if (arg == 1)
                    {
                        arg = 0;
                        putchar('\\');
                    }
                }
                else
                {
                    arg = 0;
                    putchar(p);
                }
            }
            position++;
            if (position < argc)
                putchar(' ');
        }
    }
    else
        print_echo_me(argc, position, argv);
    if (!opt_echo->opt_n)
    {
        printf("\n");
    }
}

int exe_echo(int argc, char *argv[])
{
    if (argc > 1)
    {
        int position = 1;
        int arg = 0;
        opt_echo = mcalloc(1, sizeof(struct opt_echo));
        while (position < argc && argv[position][0] == '-' && arg == 0)
        {
            arg = arg_echo(argv[position]);
            if (arg == 0)
                position++;
        }
        print_echo(argc, position, argv);
        mfree(opt_echo);
        return 0;
    }
    else
    {
        printf("\n");
        return 0;
    }
}
