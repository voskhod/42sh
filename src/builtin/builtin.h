/**
** \file builtin.h
** \brief Contain all header for the builtin.
*/
#ifndef __BUILTIN_H__
#define __BUILTIN_H__

#include "builtin/shopt.h"
#include "builtin/history.h"
#include "builtin/echo.h"
#include "builtin/exit.h"
#include "builtin/cd.h"
#include "builtin/source.h"
#include "builtin/alias.h"
#include "builtin/unalias.h"
#include "builtin/jobs.h"
#include "builtin/export.h"

#endif /* __BUILTIN_H__ */
