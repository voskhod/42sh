#ifndef ALIAS_H
#define ALIAS_H

struct alias
{
    char *alias;
    char *value;
    struct alias *next;
};

extern struct alias *alias_list;

void delete_alias_list(void);

int exe_alias(int argc, char *argv[]);

#endif /* ALIAS_H */
