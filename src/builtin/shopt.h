/**
**  \file shopt.h
**  \brief The shopt builtin.
*/
#ifndef BUILTIN_SHOPT_H
#define BUILTIN_SHOPT_H

#include "interface/option.h"

/**
**  \struct shopt_option
**  \brief The options of shopt builtin
*/
struct shopt_option
{
    int opt_q;
    int opt_s;
    int opt_u;
};
/**
** \struct value
** \brief Value for print arg and option
*/
struct value
{
    int i;
    int arg;
};

/**
** \brief Global variable which contain shopt option.
*/
extern struct shopt *g_shopt;

/**
** \fn int exe_shopt(int argc, char *argv[])
** \brief modify or print the shopt.
**
** \param argc The number of strings in argv
** \param argv The arguments array
** \return The expected value if the arg are valid or invalid
*/
int exe_shopt(int argc, char *argv[]);

#endif /* BUILTIN_SHOPT_H */
