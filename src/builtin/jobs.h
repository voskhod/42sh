/**
** \file jobs.h
** \brief The jobs, fg, bg buitins
*/
#ifndef EXE_JOBS
#define EXE_JOBS

/**
** \fn int exe_jobs(int argc, char *argv[])
** \brief The jobs builtin
**
** \param The number of args
** \param The args
** \return The return value
*/
int exe_jobs(int argc, char *argv[]);

/**
** \fn int exe_fg(int argc, char *argv[])
** \brief The fg builtin
**
** \param The number of args
** \param The args
** \return The return value
*/
int exe_fg(int argc, char *argv[]);

/**
** \fn int exe_bg(int argc, char *argv[])
** \brief The bg builtin
**
** \param The number of args
** \param The args
** \return The return value
*/
int exe_bg(int argc, char *argv[]);

#endif /* EXE_JOBS */
