#include "builtin/alias.h"
#include "utilities/utilities.h"
#include <stdio.h>
#include <string.h>

struct alias *alias_list = NULL;
static int position;

static int opt_alias = 0;

static int arg_alias(char *argv)
{
    opt_alias = 0;
    if (argv[1] == '\0')
    {
        return 3;
    }
    int i = 1;
    for (; argv[i] != '\0'; i++)
    {
        if (argv[i] == 'p')
            opt_alias = 1;
        else
            break;
    }
    if (argv[i] == '\0')
        return 0;
    else
    {
        warnx("%c: invalid argument", argv[i]);
        return 1;
    }
}

static int find_alias_pos(char *alias, char *alias_new)
{
    if (!strcmp(alias, alias_new))
        return 0;
    int i = 0;
    while (alias[i] != '\0' && alias_new[i] != '\0' && alias[i] == alias_new[i])
    {
        i++;
    }
    if (alias[i] == '\0' || alias[i] < alias_new[i])
        return 1;
    else
        return -1;
}

static int add_alias(char *alias, char *value)
{
    if (alias_list == NULL)
    {
        if (opt_alias == 1)
        {
            mfree(alias);
            mfree(value);
            return 1;
        }
        alias_list = mcalloc(1, sizeof(struct alias));
        alias_list->value = value;
        alias_list->alias = alias;
        alias_list->next = NULL;
    }
    else
    {
        struct alias *alias_next = alias_list;
        struct alias *alias_prev = NULL;
        while (alias_next != NULL)
        {
            int val = find_alias_pos(alias_next->alias, alias);
            if (val == 0)
            {
                mfree(alias_next->value);
                alias_next->value = value;
                mfree(alias);
                break;
            }
            else if (val < 0)
            {
                if (alias_prev == NULL)
                {
                    alias_prev = mcalloc(1, sizeof(struct alias));
                    alias_prev->alias = alias;
                    alias_prev->value = value;
                    alias_prev->next = alias_next;
                    alias_list = alias_prev;
                }
                else
                {
                    struct alias *new_alias = mcalloc(1, sizeof(struct alias));
                    alias_prev->next = new_alias;
                    new_alias->alias = alias;
                    new_alias->value = value;
                    new_alias->next = alias_next;
                }
                break;
            }
            else
            {
                if (alias_next->next == NULL)
                {
                    alias_next->next = mcalloc(1, sizeof(struct alias));
                    alias_next->next->value = value;
                    alias_next->next->alias = alias;
                    alias_next->next->next = NULL;
                    break;
                }
                alias_prev = alias_next;
                alias_next = alias_next->next;
            }
        }
    }
    return 0;
}

static int alias_creation(char *argv, int pos_equal, int len)
{
    char *alias = mcalloc(pos_equal + 1, sizeof(char));
    char *value = mcalloc(len - pos_equal + 2, sizeof(char));
    int i = 0;
    while (i < pos_equal)
    {
        alias[i] = argv[i];
        i++;
    }
    alias[i] = '\0';
    i++;
    int pos = 0;
    while (i < len)
    {
        value[pos] = argv[i];
        i++;
        pos++;
    }
    value[pos] = '\0';
    return add_alias(alias, value);
}

static int search_alias(char *alias)
{
    struct alias *alias_next = alias_list;
    while (alias_next != NULL)
    {
        if (!strcmp(alias_next->alias, alias))
        {
            if (opt_alias == 1)
                printf("alias ");
            printf("%s=\'%s\'\n", alias_next->alias, alias_next->value);
            return 0;
        }
        alias_next = alias_next->next;
    }
    warnx("alias: %s : not found", alias);
    return 1;
}

static int alias_init(char *argv)
{
    int len = 0;
    int pos_equal = 0;
    int eq = 0;
    for (; argv[len] != '\0'; len++)
    {
        if (argv[len] == '=' && pos_equal == 0)
        {
            pos_equal = len;
            eq = 1;
        }
    }
    int return_value = 0;
    if (eq == 0 || pos_equal == 0)
    {
        return_value += search_alias(argv);
    }
    else
    {
        return_value += alias_creation(argv, pos_equal, len);
    }
    if (return_value > 0)
        return 1;
    else
        return 0;
}

static void print_alias_list(void)
{
    struct alias *alias_next = alias_list;
    while (alias_next != NULL)
    {
        if (opt_alias == 1)
        {
            printf("alias ");
        }
        printf("%s=\'%s\'\n", alias_next->alias, alias_next->value);
        alias_next = alias_next->next;
    }
}

static int alias_gestion(int argc, char *argv[])
{
    int arg = 0;
    while (position < argc && argv[position][0] == '-')
    {
        arg = arg_alias(argv[position]);
        if (arg != 3)
            position++;
        else
            break;
    }
    if (arg == 1)
        return 1;
    if (opt_alias == 1)
    {
        print_alias_list();
    }
    int return_value = 0;
    while (position < argc)
    {
        return_value += alias_init(argv[position]);
        position++;
    }
    opt_alias = 0;
    if (return_value != 0)
        return 1;
    else
        return 0;
}

void delete_alias_list(void)
{
    struct alias *alias_next = alias_list;
    while (alias_list != NULL)
    {
        mfree(alias_list->alias);
        mfree(alias_list->value);
        alias_list = alias_list->next;
        mfree(alias_next);
        alias_next = alias_list;
    }
}

int exe_alias(int argc, char *argv[])
{
    position = 1;
    if (argc == 1)
    {
        print_alias_list();
        return 0;
    }
    if (argc > 1)
    {
        return alias_gestion(argc, argv);
    }
    return 0;
}
