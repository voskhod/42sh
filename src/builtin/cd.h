/**
** \file cd.h
** \brief The cd builtin.
*/
#ifndef BUILTIN_CD_H
#define BUILTIN_CD_H

/**
** \fn int exe_cd(int argc, char *argv[])
** \brief Change the PWD
** \param argc The number of strings in argv
** \param argv The arguments array
** \return The expected return value of builtin cd
*/
int exe_cd(int argc, char *argv[]);

#endif
