#include "builtin/export.h"
#include "utilities/utilities.h"
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int position;

static struct opt_export *opt_export;

static int arg_export(char *argv)
{
    if (argv[1] == '\0')
    {
        warnx("export: \' - \' : not a valid identifier");
        return 1;
    }
    int i = 1;
    for (; argv[i] != '\0'; i++)
    {
        if (argv[i] == 'n')
            opt_export->opt_n = 1;
        else if (argv[i] == 'p')
            opt_export->opt_p = 1;
        else
            break;
    }
    if (argv[i] == '\0')
        return 0;
    else
    {
        warnx("export: -%c: invalid option", argv[i]);
        return 2;
    }
    return 0;
}
static int export_creation(char *argv, int pos_equal, int len)
{
    char *env = mcalloc(pos_equal + 1, sizeof(char));
    char *value = mcalloc(len - pos_equal + 2, sizeof(char));
    int i = 0;
    while (i < pos_equal)
    {
        env[i] = argv[i];
        i++;
    }
    env[i] = '\0';
    i++;
    int pos = 0;
    while (i < len)
    {
        value[pos] = argv[i];
        i++;
        pos++;
    }
    value[pos] = '\0';
    int return_value = setenv(env, value, 1);
    mfree(env);
    mfree(value);
    if (return_value == 0)
        return 0;
    else
        return 1;
}

static int export_init(char *argv)
{
    int len = 0;
    int pos_equal = 0;
    int eq = 0;
    for (; argv[len] != '\0'; len++)
    {
        if (argv[len] == '=' && pos_equal == 0)
        {
            pos_equal = len;
            eq = 1;
        }
    }
    int return_value = 0;
    if (eq == 0 || pos_equal == 0)
    {
        return_value += 0;
    }
    else
    {
        return_value += export_creation(argv, pos_equal, len);
    }
    if (return_value > 0)
        return 1;
    else
        return 0;
}

static int gestion_export(int argc, char *argv[])
{
    int return_value = 0;
    while (position < argc)
    {
        return_value = export_init(argv[position]);
        position++;
    }
    return return_value;
}

int exe_export(int argc, char *argv[])
{
    position = 1;
    if (argc == 1)
    {
        return 0;
    }
    opt_export = mcalloc(1, sizeof(struct opt_export));
    int arg = 0;
    while (position < argc && argv[position][0] == '-')
    {
        if ((arg = arg_export(argv[position])) != 0)
        {
            return arg;
        }
        position++;
    }
    arg = gestion_export(argc, argv);
    mfree(opt_export);
    return arg;
}
