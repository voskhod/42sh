#include <stdlib.h>
#include <stdio.h>

#include "builtin/exit.h"
#include "interface/configure_shell.h"

#include <err.h>

int exe_exit(int argc, char *argv[])
{
    shell_quit();
    if (argc >= 2)
    {
        int exit_value;
        if (sscanf(argv[1], "%d", &exit_value) != 1)
        {
            warnx("exit: %s: numeric argument required", argv[1]);
            exit(2);
        }
        else
        {
            if (argc > 2)
            {
                warnx("exit: to many arguments");
                return 1;
            }
            else
                exit(exit_value);
        }
    }
    exit(0);
}
