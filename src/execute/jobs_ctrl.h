/**
** \file jobs_ctrl.h
** \brief The job control unit
*/
#ifndef JOBS_CTRL_H
#define JOBS_CTRL_H

#include <unistd.h>
#include <termios.h>
#include <sys/types.h>

/**
** \enum jobs_status
** \brief The status of a job.
*/
enum jobs_status
{
    RUNNING = 0,
    DONE,
    STOPPED,
    STOPPED_SIGTSTP,
    STOPPED_SIGSTOP,
    STOPPED_SIGTTIN,
    STOPPED_SIGTTOU
};

/**
** \struct job
** \brief A job.
*/
struct job
{
    int job_id;

    char *cmd;

    char display_update;

    pid_t pid;
    pid_t pgid;
    struct termios tmode;
    enum jobs_status status;
    int exit_code;

    struct job *next;
};


/**
** \fn void jobs_ctrl_add(pid_t pid, char *str)
** \brief Add a job the job control.
**
** \param pid The pid of the process to add
** \param pid The id of the group process
** \param str The string representing the process (often just a command)
** \return The job id
*/
int jobs_ctrl_add(pid_t pid, pid_t pgid, char display_update, char *str);

/**
** \fn void jobs_ctrl_update_status(void)
** \brief Update the status of all the jobs tracked. If the statusjobs has
**      changed a message is display on stderr.
*/
void jobs_ctrl_update_status(void);

/**
** \fn void jobs_ctrl_print_all(void)
** \brief Print all the jobs with their status.
*/
void jobs_ctrl_print_all(void);

/**
** \fn void jobs_ctrl_clear(void)
** \brief Send a SIGTERM and wait for all the process in the job control.
*/
void jobs_ctrl_clear(void);

/**
** \fn int jobs_ctrl_fg(int job_id)
** \brief Send the job to the foreground.
**
** \param job_id The job
** \return 0 if the job was successfully sent to the foreground, else return 0
*/
int jobs_ctrl_fg(int job_id);

/**
** \fn int jobs_ctrl_bg(int job_id)
** \brief Send the job to the background.
**
** \param job_id The job
** \return 0 if the job was successfully sent to the background, else return 0
*/
int jobs_ctrl_bg(int job_id);

/**
** \fn void update_job_from_status(struct job *job, int status)
** \brief Update the status according to status.
**
** \param job The job to update
** \param status The status
*/
void update_job_from_status(struct job *job, int status);

/**
** \fn void jobs_ctrl_wait_job(struct job *job)
** \brief Wait for a job to be stopped or done.

** \param job The job to wait
** \return Return the exit code if the program has finished or 148
*/
int jobs_ctrl_wait_job(int job_id);

#endif /* JOBS_CTRL_H */
