/**
** \file exec_from_input.h
** \brief Execute a shell script, a string or in interactive mode
*/
#ifndef EXEC_FROM_INPUT_H
#define EXEC_FROM_INPUT_H

/**
** \fn int execute_from_file(char *str, int ast_print)
** \brief Execute a shell script.
**
** \param str The shell script
** \param ast_print Generate the dot file from the ast
** \return The exit code
*/
int execute_from_file(char *str, int ast_print);

/**
** \fn int execute_from_str(char *str, int ast_print)
** \brief Execute from a string.
**
** \param str The input string
** \param ast_print Generate the dot file from the ast
** \return The exit code
*/
int execute_from_str(char *str, int ast_print);

/**
** \fn int execute_from_stdin(int ast_print)
** \brief Execute the shell in interactive mode.
**
** \param ast_print Generate the dot file from the ast
** \return The exit code
*/
int execute_from_stdin(int ast_print);

#endif /* EXEC_FROM_INPUT_H */
