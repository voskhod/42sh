/**
** \file variable.h
** \brief The unit for the shell variable.
*/
#ifndef VARIABLE_H
#define VARIABLE_H

/**
** \fn void variable_set(char *name, char *value)
** \brief Set a variable.
**
** \param name The name of the variable
** \param value The value of the variable
*/
void variable_set(char *name, char *value);

/**
** \fn void variable_assign(char *str)
** \brief Set a variable with a string in the form "NAME=VALUE".
**
** \param str The string contaning the name and the value separate by an equal
*/
void variable_assign(char *str);

/**
** \fn char *variable_get(char *name)
** \brief Get the value of the variable, if the variable isn't already set the
**        function return "".
**
** \param name The name of the variable
** \return The value of the variable.
*/
char *variable_get(char *name);

/**
** \fn char *variable_get_longest_match(char *name)
** \brief Get the value of the variable with the longest match.
**
** \param name The name
** \param len The length of the longest match
** \return The value of the variable
*/
char *variable_get_longest_match(char *name, int *len);


void variable_set_from_int(char *name, int value);

/**
** \fn void variable_clear(void)
** \brief Delete all the variables.
*/
void variable_clear(void);

#endif /* VARIABLE_H */
