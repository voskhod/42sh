#include <stdlib.h>
#include <stddef.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#include "parser/command.h"
#include "execute/exe_bin.h"
#include "execute/term_man.h"
#include "execute/term_man.h"
#include "interface/readline.h"
#include "utilities/utilities.h"


static pid_t fork_loop(void)
{
    pid_t pid;

    errno = 0;
    do {
        pid = fork();
    } while (errno == EAGAIN);

    if (pid < 0)
        errx(EXIT_EXEC_ERROR, "Failed to fork");

    return pid;
}


static void exec_form_argv(char *argv[])
{
    errno = 0;

    do {
        execvp(argv[0], argv);
    } while (errno == EINTR || errno == EAGAIN);

    int err_code = errno;

    if (err_code == ENOEXEC)
        err(EXIT_NOT_EXEC, "Not an executable file");
    else if (err_code == EPERM || err_code == EACCES)
        err(EXIT_WRG_PERM, "Wrong perm");

    err(EXIT_UNK_CMD, "Error while executing: %s", argv[0]);
}


int execute_bin(struct command *cmd)
{
    pid_t pid = fork_loop();

    if (pid == 0) /* Child */
    {
        term_man_set(0);

        exec_form_argv(cmd->argv);

        /* Should never be call */
        exit(1);
    }

    int job_id = jobs_ctrl_add(pid, pid, 0, strdup(cmd->argv[0]));
    setpgid(pid, 0);

    jobs_ctrl_fg(job_id);

    return 0;//r;
}

