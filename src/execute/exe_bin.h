/**
** \file exe_bin.h
** \brief Execute a binnary.
**
** This module contain all the functions needed to execute a biannary.
**
*/
#ifndef EXE_BIN_H
#define EXE_BIN_H

#include "parser/command.h"

/**
** \fn int execute_bin(struct command *cmd)
** \brief Execute the binary in cmd.
**
** \param cmd The binary to  execute
** \return The exit code
*/
int execute_bin(struct command *cmd);

#endif /* EXE_BIN_H */
