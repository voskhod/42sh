/**
** \file term_man.h
** \brief Manage the terminal.
*/
#ifndef TERM_MAN_H
#define TERM_MAN_H

#include <unistd.h>
#include <termios.h>

#include "execute/jobs_ctrl.h"

/**
** \fn void term_man_init(void)
** \brief If in interactive mode, set up the terminal and job control.
*/
void term_man_init(void);


/**
** \fn void term_man_set(pid_t pgid, int foreground)
** \brief Set the new process signals, acess to termial and group.
**
** \param foreground The process is in foreground or not
*/
void term_man_set(int foreground);


/**
** \fn void term_man_new_command(void)
** \brief Reset the proccess group for executing new command.
*/
void term_man_new_command(void);

/**
** \fn void term_man_set_parent(pid_t pid)
** \brief Set the terminal for async jobs and update process group.
*/
void term_man_set_parent(pid_t pid);

/**
** \fn void term_man_put_process_fg(struct job *job, int send_cont)
** \brief Put the process in the foreground.
**
** \param job The job
** \param send_cont If true send a SIGCONT to the process
*/
void term_man_put_process_fg(struct job *job, int send_cont);

void term_man_restore(void);

void set_signal(void);

pid_t term_man_get_cur_pgid(void);
#endif /* TERM_MAN_H */
