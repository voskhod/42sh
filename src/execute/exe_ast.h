/**
** \file exe_ast.h
** \brief Execute an AST.
**
** This module contain all function needed to execute an AST.
*/
#ifndef EXE_AST_H
#define EXE_AST_H


#define SHELL_FALSE 1
#define SHELL_TRUE 0


#include "parser/ast.h"


/**
** \fn int execute_ast(struct ast *ast)
** \brief Execute an AST.
**
** \param ast The ast to execute
** \return Return EXIT_SUCCESS on success, if something goes wrong return an
**          error code defined in utilities.h
**
** Execute an AST and execute the command and expression inside.
*/
int execute_ast(struct ast *ast);


#endif /* EXE_AST_H */
