/**
** \file exe_cmd.h
** \brief Execute a builtin, an utilities or builtin.
*/
#ifndef EXE_CMD_H
#define EXE_CMD_H

#include "parser/command.h"

/**
** \fn int execute_command(struct command *cmd)
** \brief Execute a builtin, an utilities or builtin.
**
** \param cmd The command to execute
** \return The exit code
*/
int execute_command(struct command *cmd);

#endif /* EXE_CMD_H */
