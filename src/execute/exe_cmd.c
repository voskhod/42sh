#include <err.h>
#include <string.h>

#include "parser/ast.h"
#include "expand/expand.h"
#include "execute/exe_ast.h"
#include "execute/exe_cmd.h"
#include "execute/exe_bin.h"
#include "builtin/builtin.h"
#include "execute/function.h"
#include "utilities/utilities.h"

struct builtin_name
{
    char *name;
    int(* f)(int, char **);
};

int not_implement_yet(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    warnx("Not implement yet");

    return 0;
}

struct builtin_name builtin_list[] =
{
    { "shopt", exe_shopt },
    { "history", exe_history },
    { "echo", exe_echo },
    { "exit", exe_exit },
    { "source", exe_source },
    { ".", exe_source },
    { "alias", exe_alias },
    { "unalias", exe_unalias },
    { "jobs", exe_jobs },
    { "fg", exe_fg },
    { "bg", exe_fg },
    { "export", exe_export }
};


struct utilities
{
    char *name;
    int(* f)(int, char **);
};


struct utilities util_list[] =
{
    { "cd", exe_cd }
};



int execute_command(struct command *cmd)
{
    if (cmd->argv[0] == NULL)
        return 0;

    expand_cmd(cmd);

    for (size_t i = 0; i < sizeof(builtin_list) / sizeof(*builtin_list); i++)
    {
        if (!strcmp(builtin_list[i].name, cmd->argv[0]))
            return builtin_list[i].f(cmd->size, cmd->argv);
    }

    struct ast *function = function_find(cmd->argv[0]);
    if (function != NULL)
        return execute_ast(function);

    for (size_t i = 0; i < sizeof(util_list) / sizeof(*util_list); i++)
    {
        if (!strcmp(util_list[i].name, cmd->argv[0]))
            return util_list[i].f(cmd->size, cmd->argv);
    }

    return execute_bin(cmd);
}
