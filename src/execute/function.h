/**
** \file function.h
** \brief The module for the shell functions
*/
#ifndef FUNCTION_H
#define FUNCTION_H

#include "parser/ast.h"

/**
** \fn void function_add(char *name, struct ast *function)
** \brief Add a function.
**
** \param name The function name
** \param function The function represent by an AST
*/
void function_add(char *name, struct ast *function);

/**
** \fn struct ast *function_find(char *name)
** \brief Find a function and return the function represent AST
**
** \param name The function name
** \return The function represent by an AST
*/
struct ast *function_find(char *name);

/**
** \fn void fuction_clear()
** \brief Destroy all the function
*/
void fuction_clear(void);

#endif /* FUNCTION_H */
