#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "lexer/lexer.h"
#include "parser/parser.h"
#include "execute/exe_ast.h"
#include "interface/input.h"
#include "print/print_ast.h"
#include "execute/term_man.h"
#include "execute/variable.h"
#include "execute/jobs_ctrl.h"
#include "utilities/utilities.h"

static int execute_from_lexer(struct lexer *lexer, int ast_print)
{
    int return_code = EXIT_SUCCESS;


    while (lexer->input->str != NULL && lexer_peek(lexer) != TOKEN_EOF)
    {
        term_man_new_command();
        struct ast *ast = parse_input(lexer);
        jobs_ctrl_update_status();

        if (ast == NULL)
            continue;

        if (ast_print)
            init_print_ast(ast);

        return_code = execute_ast(ast);
        variable_set_from_int("?", return_code);

        ast_destroy(ast);

        char *home = getenv("HOME");
        if (home != NULL)
        {
            char *line;
            asprintf(&line, "%s/.42sh_history", home);
            write_history(line);

            mfree(line);
        }
    }
    return return_code;
}


int execute_from_file(char *str, int ast_print)
{
    struct stat statbuf = { 0 };
    lstat(str, &statbuf);

    if ((statbuf.st_mode & S_IFMT)!= S_IFREG)
        return EXIT_UNK_CMD;

    rl_instream = fopen(str, "r");
    if (!rl_instream)
    {
        rl_instream = stdin;
        if (errno == EACCES)
            return EXIT_NOT_EXEC;
        else
            return EXIT_UNK_CMD;
    }

    term_man_init();

    struct lexer *lexer = lexer_init(input_init());
    int return_code = execute_from_lexer(lexer, ast_print);
    lexer_destroy(lexer);
    fclose(rl_instream);
    rl_instream = stdin;

    return return_code;

}


int execute_from_str(char *str, int ast_print)
{
    term_man_init();

    struct lexer *lexer = lexer_init(input_init_from_str(str));
    int return_code = execute_from_lexer(lexer, ast_print);
    lexer_destroy(lexer);

    return return_code;
}


int execute_from_stdin(int ast_print)
{
    rl_instream = stdin;

    term_man_init();

    struct lexer *lexer = lexer_init(input_init());
    int return_code = execute_from_lexer(lexer, ast_print);
    lexer_destroy(lexer);

    return return_code;
}


