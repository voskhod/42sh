#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>

#include "execute/variable.h"
#include "execute/term_man.h"
#include "execute/jobs_ctrl.h"
#include "utilities/utilities.h"


static struct job *jobs_list = NULL;


static struct job *job_init(pid_t pid, pid_t pgid, char display_update, char *cmd)
{
    struct job *new = mcalloc(1, sizeof(struct job));
    new->pid = pid;
    new->pgid = pgid;
    new->display_update = display_update;
    new->cmd = cmd;

    return new;
}


int jobs_ctrl_add(pid_t pid, pid_t pgid, char display_update, char *cmd)
{
    struct job *new = job_init(pid, pgid, display_update, cmd);

    new->job_id = (jobs_list == NULL) ? 1 : jobs_list->job_id + 1;
    new->cmd = cmd;

    new->next = jobs_list;
    jobs_list = new;

    char *str = NULL;
    asprintf(&str, "%d", pid);
    variable_set(strdup("!"), str);

    if (display_update)
        fprintf(stderr, "[%d] %d\n", jobs_list->job_id, jobs_list->pid);

    return new->job_id;
}


void update_job_from_status(struct job *job, int status)
{
    if (job == NULL)
        return;

    /* job has terminated  normally */
    if (WIFEXITED(status))
    {
        job->status = DONE;
        job->exit_code = WEXITSTATUS(status);
    }
    /* job has been stop by a signal */
    else if (WIFSTOPPED(status))
    {
        job->display_update = 1;

        int sig = WSTOPSIG(status);
        if (sig == SIGSTOP)
            job->status = STOPPED_SIGSTOP;
        else if (sig == SIGTTIN)
            job->status = STOPPED_SIGTTIN;
        else if (sig == SIGTTOU)
            job->status = STOPPED_SIGTTOU;
        else
            job->status = STOPPED;
    }
    /* job has resume */
    else if (WIFCONTINUED(status))
        job->status = RUNNING;
}


static struct job *remove_done_jobs(struct job *list)
{
    if (list == NULL)
        return NULL;

    struct job *to_delete = NULL;
    if (list->status == DONE)
    {
        to_delete = list;
        list = remove_done_jobs(list->next);

        mfree(to_delete->cmd);
        mfree(to_delete);
    }
    else
        list->next = remove_done_jobs(list->next);

    return list;
}


static struct job *get_job_from_pid(pid_t pid)
{
    struct job *cur = jobs_list;
    for (; cur != NULL && cur->pid != pid; cur = cur->next)
        continue;

    return cur;
}


static struct job *get_job(int job_id)
{
    struct job *cur = jobs_list;
    for (; cur != NULL && cur->job_id != job_id; cur = cur->next)
        continue;

    return cur;
}


static void print_job(struct job *job, FILE *out, char c)
{
    char *status_str[] =
    {
        "Running          ",
        "Done             ",
        "Stopped          ",
        "Stopped (SIGTSTP)",
        "Stopped (SIGSTOP)",
        "Stopped (SIGTTIN)",
        "Stopped (SIGTTOU)",
    };

    if (job->display_update)
        fprintf(out, "[%d]%c  %s          %s\n",
            job->job_id, c, status_str[job->status], job->cmd);
}


void jobs_ctrl_update_status(void)
{
    jobs_list = remove_done_jobs(jobs_list);

    int status;
    errno = 0;
    int pid_to_update;
    for (struct job *cur = jobs_list; cur != NULL; cur = cur->next)
    {
        pid_to_update = cur->pid;

        int has_changed = waitpid(pid_to_update, &status,
                WNOHANG | WUNTRACED | WCONTINUED);

        if (!has_changed)
            continue;

        update_job_from_status(cur, status);
        print_job(cur, stderr, '+');
    }
}


static void jobs_ctrl_print_all_rec(struct job *list, char c)
{
    if (list == NULL)
        return;

    char cc = ' ';
    if (c == '+')
        cc = '-';

    jobs_ctrl_print_all_rec(list->next, cc);

    print_job(list, stdout, c);
}


void jobs_ctrl_print_all(void)
{
    jobs_ctrl_print_all_rec(jobs_list, '+');
}


void jobs_ctrl_clear(void)
{
    struct job *to_delete = NULL;
    while (jobs_list != NULL)
    {
        to_delete = jobs_list;
        jobs_list = jobs_list->next;

        kill(-to_delete->pid, SIGTERM);

        mfree(to_delete->cmd);
        mfree(to_delete);
    }
}


static inline int is_stopped(struct job *j)
{
    if (j == NULL)
        return 0;

    return (j->status == STOPPED
            || j->status == STOPPED_SIGTSTP
            || j->status == STOPPED_SIGSTOP
            || j->status == STOPPED_SIGTTIN
            || j->status == STOPPED_SIGTTOU);
}


int jobs_ctrl_fg(int job_id)
{
    struct job *job = NULL;
    if (job_id == -1)
        job = jobs_list;
    else
        job = get_job(job_id);

    if (job == NULL)
        return 1;

    if (is_stopped(job))
        term_man_put_process_fg(job, 1);
    else
        term_man_put_process_fg(job, 0);

    return 0;
}


int jobs_ctrl_bg(int job_id)
{
    struct job *job = NULL;
    if (job_id == -1)
        job = jobs_list;
    else
        job = get_job(job_id);

    if (job == NULL)
        return 1;

    if (job->status >= STOPPED)
        kill(-job->pid, SIGCONT);

    return 0;
}


int jobs_ctrl_wait_job(int job_id)
{
    int status;
    int pid;
    struct job *j;
    struct job *job_to_wait = get_job(job_id);

    do
    {
        pid = waitpid(job_to_wait->pid, &status, WUNTRACED);

        if (pid >= 0)
        {
            j = get_job_from_pid(pid);
            update_job_from_status(j, status);
        }
    }
    while (pid > 0 && job_to_wait->status == RUNNING);

    return (is_stopped(job_to_wait)) ? 148 : job_to_wait->exit_code;
}
