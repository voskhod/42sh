#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "parser/ast.h"
#include "expand/expand.h"
#include "parser/parser.h"
#include "execute/exe_ast.h"
#include "execute/exe_cmd.h"
#include "execute/term_man.h"
#include "execute/variable.h"
#include "execute/function.h"
#include "execute/jobs_ctrl.h"
#include "interface/readline.h"
#include "utilities/utilities.h"


static int execute_case(struct ast *ast)
{
    struct case_node *case_node = ast->statement.case_node;
    int result = 0;
    struct case_item *current = case_node->item;
    int is_done = 0;
    while (current != NULL && is_done == 0)
    {
        for (size_t i = 0; i < current->vector->size; i++)
        {
            char *val = expand_word(strdup(case_node->value)); 

            if (strcmp(current->vector->data[i], val) == 0)
            {
                result = execute_ast(current->body);
                is_done = 1;
                break;
            }

            mfree(val);
        }
        current = current->next;
    }
    return result;
}


static int execute_for(struct ast *ast)
{
    struct for_node *for_node = ast->statement.for_node;
    int result = 0;
    for (size_t i = 0; i < for_node->vector->size; i++)
    {
        result = execute_ast(for_node->body);
    }
    return result;
}

static int execute_cpd_list_br(struct ast *ast)
{
    struct compound_list_break *cpd = ast->statement.cpd_list_break;
    int result = 0;
    struct compound_list_break *current = cpd;
    while (current)
    {
        result = execute_ast(current->current);
        current = current->next;
    }
    return result;
}



static int execute_until(struct ast *ast)
{
    struct until_node *until_node = ast->statement.until_node;
    int result = 0;
    while (execute_ast(until_node->cond) != SHELL_TRUE)
        result = execute_ast(until_node->body);

    return result;
}


static int execute_while(struct ast *ast)
{
    struct while_node *while_node = ast->statement.while_node;
    int result = 0;
    while (execute_ast(while_node->cond) == SHELL_TRUE)
    {
        result = execute_ast(while_node->body);
    }
    return result;
}


static int execute_if(struct ast *ast)
{
    struct if_node *if_node = ast->statement.if_node;

    if (execute_ast(if_node->cond) == SHELL_TRUE)
        return execute_ast(if_node->if_body);
    else
        return execute_ast(if_node->else_body);
}


static int execute_and_or(struct ast *ast)
{
    struct and_or_node *and_or_node = ast->statement.and_or_node;

    int left_res = execute_ast(and_or_node->left);
    if ((left_res == SHELL_TRUE &&and_or_node->type == OR_TYPE
                && and_or_node->right->type != AST_NODE_AND_OR)
            || (left_res != SHELL_TRUE && and_or_node->type == AND_TYPE
                && and_or_node->right->type != AST_NODE_AND_OR))
        return left_res;

    int right_res = execute_ast(and_or_node->right);
    if (right_res == EXIT_UNK_CMD)
        return right_res;
    if (and_or_node->type == AND_TYPE)
        return !(!left_res && !right_res);
    else
        return !(!left_res || !right_res);
}


static int execute_pipe(struct ast *ast)
{
    struct pipeline *pipeline = ast->statement.pipeline;

    if (pipeline->right == NULL || pipeline->left == NULL)
    {
        warnx("The '|' operator need two arguments");
        return EXIT_WRG_OPT;
    }

    int fd[2];
    pipe(fd);

    pid_t pid_left = fork();
    if (pid_left == 0) // left child
    {
        term_man_set(1);

        close(fd[0]);
        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);
        exit(execute_ast(pipeline->left));
    }
    term_man_set_parent(pid_left);

    pid_t pid_right = fork();
    if (pid_right == 0) // right child
    {
        term_man_set(1);

        close(fd[1]);
        dup2(fd[0], STDIN_FILENO);
        close(fd[0]);
        exit(execute_ast(pipeline->right));
    }
    term_man_set(pid_right);

    close(fd[0]);
    close(fd[1]);

    int status_left;
    waitpid(pid_left, &status_left, 0);
    int status_right;
    waitpid(pid_right, &status_right, 0);

    return WEXITSTATUS(status_right);
}


static int execute_cmd_node(struct ast *ast)
{
    return execute_command(ast->statement.cmd);
}


static int execute_fun(struct ast *ast)
{
    function_add(ast->statement.function->name,
                    ast->statement.function->fun_body);

    return 0;
}


static int execute_var_def(struct ast *ast)
{
    char *var = expand_word(strdup(ast->statement.var_def->var_def));

    variable_assign(var);
    mfree(var);

    return 0;
}


static int *set_fd(int in, int out, int err)
{
    if (in == REDIRECT_WRG_FILE || out == REDIRECT_WRG_FILE
            || err == REDIRECT_WRG_FILE)
        return NULL;

    int *saved_fd = mmalloc(3 * sizeof(int));

#ifdef DEBUG
    warnx("fd: %d %d %d", in, out, err);
#endif /* DEBUG */

    if (in != -1)
    {
        saved_fd[STDIN_FILENO] = dup(STDIN_FILENO);
        dup2(in, STDIN_FILENO);
        close(in);
    }
    else
        saved_fd[STDIN_FILENO] = -1;

    if (out != -1)
    {
        saved_fd[STDOUT_FILENO] = dup(STDOUT_FILENO);
        dup2(out, STDOUT_FILENO);
        close(out);
    }
    else
        saved_fd[STDOUT_FILENO] = -1;

    if (err != -1)
    {
        saved_fd[STDERR_FILENO] = dup(STDERR_FILENO);
        dup2(err, STDERR_FILENO);
        close(err);
    }
    else
        saved_fd[STDERR_FILENO] = -1;

    return saved_fd;
}


static void reset_fd(int *saved_fd)
{

    if (saved_fd[STDIN_FILENO] != -1)
    {
        dup2(saved_fd[STDIN_FILENO], STDIN_FILENO);
        close(saved_fd[STDIN_FILENO]);
    }

    if (saved_fd[STDOUT_FILENO] != -1)
    {
        dup2(saved_fd[STDOUT_FILENO], STDOUT_FILENO);
        close(saved_fd[STDOUT_FILENO]);
    }

    if (saved_fd[STDERR_FILENO] != -1)
    {
        dup2(saved_fd[STDERR_FILENO], STDERR_FILENO);
        close(saved_fd[STDERR_FILENO]);
    }

    mfree(saved_fd);
}


int execute_async(struct ast *ast)
{
    pid_t pid = fork();

    if (pid == 0) /* Child */
    {
        term_man_new_command();
        term_man_set(0);

        ast->is_async = 0;
        int return_code = execute_ast(ast);
        exit(return_code);
    }

    term_man_set_parent(pid);
    if (ast->type == AST_NODE_COMMAND)
        jobs_ctrl_add(pid, pid, 1, strdup(ast->statement.cmd->argv[0]));
    else
        jobs_ctrl_add(pid, pid, 1, strdup("Probably a pipeline"));
    return 0;
}


int execute_ast(struct ast *ast)
{
    if (ast == NULL)
        return 0;

    if (ast->is_async && is_interactive())
        return execute_async(ast);

    int return_code = 0;

    int *saved_fd = set_fd(ast->fd_in, ast->fd_out, ast->fd_err);
    if (saved_fd == NULL)
    {
        if (ast->is_async)
            exit(EXIT_EXEC_ERROR);
        else
            return EXIT_EXEC_ERROR;
    }

    int(* node_execute[])(struct ast *) =
    {
        execute_cmd_node,
        execute_cpd_list_br,
        execute_if,
        execute_and_or,
        execute_while,
        execute_until,
        execute_pipe,
        execute_for,
        execute_case,
        execute_fun,
        execute_var_def
    };

    return_code = node_execute[ast->type](ast);

    reset_fd(saved_fd);

    if (ast->next != NULL)
        return_code = execute_ast(ast->next);

    return return_code;
}
