#include <string.h>

#include "execute/function.h"
#include "parser/ast.h"
#include "utilities/utilities.h"

static struct list *functions;

struct list
{
    char *function_name;
    struct ast *function_ast;

    struct list *next;
};


void function_add(char *name, struct ast *function)
{
#ifdef DEBUG
    warnx("Function: add %s", name);
#endif /* DEBUG */

    struct list *new = mmalloc(sizeof(struct list));
    new->function_name = name;
    new->function_ast = function;
    new->next = functions;

    functions = new;
}


struct ast *function_find(char *name)
{
    if (functions == NULL)
        return NULL;

    for(struct list *cur = functions; cur != NULL; cur = cur->next)
    {
        if (!strcmp(name, cur->function_name))
            return cur->function_ast;
    }

    return NULL;
}


void fuction_clear(void)
{
    struct list *to_delete = NULL;

    while (functions != NULL)
    {
        to_delete = functions;
        functions = functions->next;

        mfree(to_delete->function_name);
        ast_destroy(to_delete->function_ast);
        mfree(to_delete);
    }
}
