#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utilities/utilities.h"
#include "execute/variable.h"


struct var
{
    char *name;
    char *value;

    struct var *next;
};

static struct var *variable_storage = NULL;
static char *empty_var = "";

void variable_set(char *name, char *value)
{
    struct var *cur = variable_storage;
    for (; cur != NULL; cur = cur->next)
        continue;

    if (cur != NULL)
    {
        mfree(cur->value);
        cur->value = value;
    }
    else
    {
        struct var *new = mcalloc(1, sizeof(struct var));
        new->name = name;
        new->value = value;

        new->next = variable_storage;
        variable_storage = new;
    }
}


void variable_assign(char *str)
{
    int equal_pos = strcspn(str, "=");
    str[equal_pos] = '\0';

    variable_set(strdup(str), strdup(str + equal_pos + 1));
}


char *variable_get(char *name)
{
    if (!strcmp(name, "RANDOM"))
    {
        char *value = calloc(15, sizeof(char));
        snprintf(value, 15, "%d", rand());
        variable_set(strdup("RANDOM"), value);

        return value;
    }

    struct var *cur = variable_storage;
    for (; cur != NULL; cur = cur->next)
    {
        if (!strcmp(cur->name, name))
            return cur->value;
    }

    return empty_var;
}


int strcmp_match(char *ref, char *str)
{
    int nb_match = 0;
    for (; *ref; ref++, str++)
    {
        nb_match++;
        if (*ref != *str)
            return -1;
    }

    return nb_match;
}


char *variable_get_longest_match(char *name, int *len)
{
    char *value = NULL;
    int max = 0;

    struct var *cur = variable_storage;
    for (; cur != NULL; cur = cur->next)
    {
        int n = strcmp_match(cur->name, name);
        if (n > max)
        {
            max = n;
            value = cur->value;
        }
    }

    if (max <= 6 && !strncmp(name, "RANDOM", 6))
    {
        value = calloc(15, sizeof(char));
        snprintf(value, 15, "%d", rand());
        variable_set(strdup("RANDOM"), value);
    }

    if (max == 0)
        max = strcspn(name, " ") - 1;

    *len = max;
    return (value != NULL) ? value : empty_var;
}


void variable_set_from_int(char *name, int value)
{
    char *str_nb = NULL;
    asprintf(&str_nb, "%d", value);
    variable_set(strdup(name), str_nb);
}


void variable_clear(void)
{
    struct var *to_delete;
    while (variable_storage != NULL)
    {
        to_delete = variable_storage;
        variable_storage = variable_storage->next;

        mfree(to_delete->name);
        mfree(to_delete->value);
        mfree(to_delete);
    }
}
