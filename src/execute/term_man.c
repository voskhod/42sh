#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <readline/readline.h>

#include "utilities/utilities.h"
#include "interface/readline.h"
#include "execute/jobs_ctrl.h"
#include "execute/term_man.h"


static int shell_term = 0;
static pid_t shell_pgid = 0;
static struct termios shell_mode;

static pid_t current_pgid = 0;


void sigint_handler(int sig)
{
    (void)sig;

    printf("\n");
    rl_on_new_line();
    rl_replace_line("", 0);
    rl_redisplay();
}


void sigstop_handler(int sig)
{
    (void)sig;

    term_man_restore();
}


void set_signal(void)
{
    signal(SIGINT, sigint_handler);
    signal(SIGSTOP, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGCHLD, SIG_IGN);

    //signal (SIGQUIT, SIG_IGN);
}

void term_man_init(void)
{
    shell_term = rl_instream ? fileno(rl_instream) : fileno(stdin);

    if (!isatty(shell_term))
        return;

    /* Put the shell in foreground */
    while (tcgetpgrp(shell_term) != (shell_pgid = getpgrp()))
        kill (-shell_pgid, SIGTTIN);

    set_signal();

    /* Put the shell in is own process group */
    shell_pgid = getpid();
    if (setpgid(shell_pgid, shell_pgid) < 0)
        err(129, "Fail setpgid");

    /* Take the control of the terminal */
    tcsetpgrp(shell_term, shell_pgid);

    /* Save the settings */
    tcgetattr(shell_term, &shell_mode);
}


void term_man_new_command(void)
{
    current_pgid = getpid();
}


void term_man_set(int foreground)
{
    if (!isatty(shell_term))
        return;

    /* Restore the signal to default */
    signal (SIGINT, SIG_DFL);
    signal (SIGQUIT, SIG_DFL);
    signal (SIGTSTP, SIG_DFL);
    signal (SIGTTIN, SIG_DFL);
    signal (SIGTTOU, SIG_DFL);
    signal (SIGCHLD, SIG_DFL);

    /* Set the new process in a process group */
    pid_t pid = getpid();
    //if (current_pgid == 0)
    current_pgid = pid;
    setpgid (pid, 0);

    /* If the process is in foreground give it acess to the terminal */
    if (foreground)
        tcsetpgrp (shell_term, current_pgid);
}


void term_man_set_parent(pid_t pid)
{
    if (!isatty(shell_term))
        return;

    /* Update the process group */
    if (current_pgid == 0)
        current_pgid = pid;
    setpgid(pid, current_pgid);

    /* Restore the term mode */
    tcsetattr(shell_term, TCSADRAIN, &shell_mode);
}


pid_t term_man_get_cur_pgid(void)
{
    return current_pgid;
}


void term_man_put_process_fg(struct job *job, int send_cont)
{
    pid_t pid = job->pid;

    tcsetpgrp(shell_term, pid);

    if (send_cont)
    {
        tcsetattr(shell_term, TCSADRAIN, &job->tmode);
        kill(-pid, SIGCONT);
    }

    jobs_ctrl_wait_job(job->job_id);

    current_pgid = shell_pgid;
    tcsetpgrp(shell_term, shell_pgid);

    tcgetattr(shell_term, &job->tmode);
    tcsetattr(shell_term, TCSADRAIN, &shell_mode);
}


void term_man_restore(void)
{
    tcsetattr(shell_term, TCSADRAIN, &shell_mode);
}
