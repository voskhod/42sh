#include <dirent.h>
#include <err.h>
#include <fnmatch.h>
#include <locale.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utilities/vector.h"
#include "utilities/utilities.h"


struct vector *str_to_vector(char *str)
{
    char *tmp = strdup(str);
    struct vector *vector = vector_init();
    char *token = NULL;
    char *rest = str;

    while ((token = strtok_r(rest, "/", &rest)))
    {
        vector_append(vector, token);
    }

    str = strcpy(str, tmp);
    free(tmp);
    return vector;
}


char *my_strcat(char *str1, char *str2, char *delim)
{
    if (!str1)
        return strdup(str2);
    if (!str2)
        return strdup(str1);

    char *tmp = str1;
    if (!delim)
        asprintf(&str1, "%s%s", str1, str2);

    else
        asprintf(&str1, "%s%s%s", str1, delim, str2);
    
    free(tmp);
    return str1;
}


static inline int del_dot(const struct dirent *dir)
{
    if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, ".."))
        return 0;
    else
        return 1;
}


static inline void scandir_destroy(struct dirent **dirs, int count)
{
    (void)count;

    if (dirs == NULL)
        return;
    mfree(dirs);
    dirs = NULL;
}


int path_expand_vector_empty(struct vector *directory, char *current_path)
{
    setlocale(LC_ALL, "");
    struct dirent **dirs = NULL;
    int is_expand = 0;
    scandir_destroy(dirs, 0);
    dirs = NULL;
    int count = scandir(".", &dirs, del_dot, alphasort);
    for (int k = 0; k < count; k++)
    {
        if (!fnmatch(current_path, dirs[k]->d_name, FNM_PATHNAME | FNM_PERIOD))
        {
            is_expand = 1;
            vector_append(directory, dirs[k]->d_name);
        }
        free(dirs[k]);
    }
    scandir_destroy(dirs, 0);
    dirs = NULL;
    return is_expand;
}


int path_expand_vector(struct vector *directory, struct vector *path)
{
    setlocale(LC_ALL, "");
    struct dirent **dirs = NULL;
    char *current_path;
    int count = 0;
    size_t tmp_size;
    int is_expand = 0;
    size_t i = 0;
    do {
        is_expand = 0;
        tmp_size = directory->size;
        if (tmp_size == 0)
        {
            if (path_expand_vector_empty(directory, path->data[i]) == 1)
                is_expand = 1;
        }
        else
        {
            for (size_t j = 0; j < tmp_size; j++)
            {
                scandir_destroy(dirs, 0);
                dirs = NULL;
                count = scandir(directory->data[j], &dirs, del_dot, alphasort);
                for (int k = 0; k < count; k++)
                {
                    if (!fnmatch(path->data[i], dirs[k]->d_name,
                                FNM_PATHNAME | FNM_PERIOD))
                    {
                        is_expand = 1;
                        current_path = my_strcat(strdup(directory->data[j]),
                                dirs[k]->d_name, "/");
                        vector_append(directory, current_path);
                        free(current_path);
                    }
                    free(dirs[k]);
                }
                vector_del_at(directory, j);
            }
        }
        i++;
    } while (i < path->size);
    scandir_destroy(dirs, 0);
    dirs = NULL;
    return is_expand;
}


char *vector_to_str(struct vector *vector)
{
    char *str = NULL;
    for (size_t i = 0; i < vector->size; i++)
    {
        str = my_strcat(str, vector->data[i], " ");
    }
    return str;
}


char *path_expand(char *str)
{
    struct vector *path = str_to_vector(str);
    struct vector *directory = vector_init();
    int is_expand = path_expand_vector(directory, path);
    if (!is_expand)
    {
        vector_destroy(directory);
        vector_destroy(path);
        return strdup(str);
    }
    char *result = vector_to_str(directory);
    vector_destroy(directory);
    vector_destroy(path);
    return result;
}
