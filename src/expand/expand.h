/**
** \file expand.h
** \brief Perform the expansion.
*/
#ifndef EXPAND_H
#define EXPAND_H

#include "parser/command.h"
#include "interface/input.h"

/**
** \fn void expand_cmd(struct command *cmd)
** \brief Expand the args of a command.
**
** \param cmd The command with the args to expand
*/
void expand_cmd(struct command *cmd);

/**
** \fn char *expand_word(char *str)
** \brief Expand a word according to the scl.
**
** \param str The input the function try to expand
** \return The result after expansion
*/
char *expand_word(char *str);

#endif /* EXPAND_H */
