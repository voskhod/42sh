#include <math.h>
#include <string.h>

#include "lexer/delimiter.h"
#include "utilities/utilities.h"
#include "expand/maths_compute.h"

enum maths_token_type
{
    MATHS_EOF = 0,

    MATHS_AND,
    MATHS_OR,
    MATHS_NOT,

    MATHS_POW,
    MATHS_MINUS,
    MATHS_PLUS,
    MATHS_FACTOR,
    MATHS_DIV,

    MATHS_BIT_AND,
    MATHS_BIT_OR,
    MATHS_BIT_XOR,
    MATHS_BIT_NOT,

    MATHS_PAR_OP,
    MATHS_PAR_CL,

    MATHS_DIGIT
};


struct maths_token
{
    enum maths_token_type type;
    long value;
};


static int strcmp_len(char *ref, char *str)
{
    int i = 0;
    for (; ref[i]; i++)
    {
        if (ref[i] != str[i])
            return 0;
    }

    return i + 1;
}


static struct maths_token *get_token(char *str, int *size)
{
    size_t begin = skip_space(str, 0);
    str += begin;

    struct maths_token *token = mcalloc(1, sizeof(struct maths_token));

    if (*str == '\0')
    {
        size = 0;
        return token;
    }

    char *str_type[] =
    {
        "&&",
        "||",
        "!",

        "**",
        "-",
        "+",
        "*",
        "/",

        "&",
        "|",
        "^",
        "~",

        "(",
        ")"
    };


    for (size_t i = 0; i < sizeof(str_type) / sizeof(*str_type); i++)
    {
        if (strcmp_len(str_type[i], str) != 0)
        {
            (*size) = begin + strlen(str_type[i]);
            token->type = i + 1;

            return token;
        }
    }

    token->type = MATHS_DIGIT;

    size_t i = 0;
    for (; str[i] && str[i] >= '0' && str[i] <= '9'; i++)
        token->value = token->value * 10 + str[i] - '0';

    *size = begin + i;

    return token;
}


static enum maths_token_type peek(char **str)
{
    int size;
    struct maths_token *token = get_token(*str, &size);
    enum maths_token_type type = token->type;

    mfree(token);

    return type;
}


static struct maths_token *pop(char **str)
{
    int size;
    struct maths_token *token = get_token(*str, &size);

    *str = size + *str;

    return token;
}

static void eat(char **str)
{
    int size;
    struct maths_token *token = get_token(*str, &size);

    mfree(token);
    *str = size + *str;
}


struct parse_fun
{
    struct parse_fun *des;

    long (* semantic)(char **, long a, int *);
};


static long unary_semantic(char **str, long a, int *err);
static long pow_semantic(char **str, long a, int *err);
static long fact_semantic(char **str, long a, int *err);
static long sum_semantic(char **str, long a, int *err);
static long bit_op_semantic(char **str, long a, int *err);
static long logical_and_semantic(char **str, long a, int *err);
static long logical_or_semantic(char **str, long a, int *err);

enum f_type
{
    UNARY = 0,
    POW,
    FACT,
    SUM,
    BIT_OP,
    LOGI_AND,
    LOGI_OR
};

struct parse_fun pf_tab[] =
{
    { NULL, unary_semantic },
    { &pf_tab[UNARY], pow_semantic },
    { &pf_tab[POW], fact_semantic },
    { &pf_tab[FACT], sum_semantic },
    { &pf_tab[SUM], bit_op_semantic },
    { &pf_tab[BIT_OP], logical_and_semantic },
    { &pf_tab[LOGI_AND], logical_or_semantic }
};


static long parse_function(struct parse_fun *pf, char **str, int *err)
{
/* unary case: only one unary can be parse */
    if (pf->des == NULL)
        return pf->semantic(str, 0, err);

/* Other case */
    long a = parse_function(pf->des, str, err);

    if (*err)
    {
        warnx("Syntax error at: %s", *str);
        return 0;
    }

    int stop = 0;
    while (stop == 0)
        a = pf->semantic(str, a, &stop);

    if (stop == -1)
    {
        *err = stop;
        return 0;
    }

    return a;
}


long maths_compute(char *str, int *err)
{
    *err = 0;

    if (peek(&str) == MATHS_EOF)
        return 0;

    long a = parse_function(&pf_tab[LOGI_OR], &str, err);

    if (peek(&str) == MATHS_EOF && *err == -1)
        return 0;
    else
        *err = -1;

    return a;
}


static long unary_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_DIGIT)
    {
        struct maths_token *token = pop(str);
        a = token->value;
        mfree(token);
    }
    else if (peek(str) == MATHS_BIT_NOT)
    {
        eat(str);
        a = ~parse_function(&pf_tab[UNARY], str, err);
    }
    else if (peek(str) == MATHS_PAR_OP)
    {
        eat(str);
        a = parse_function(&pf_tab[LOGI_OR], str, err);

        if (peek(str) == MATHS_PAR_CL)
            eat(str);
        else
            *err = -1;
    }
    else
        *err = -1;

    return a;
}


static long pow_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_POW)
    {
        eat(str);
        a = pow(a, parse_function(&pf_tab[UNARY], str, err));
    }
    else
        *err = 1;

    return a;
}


static long fact_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_FACTOR)
    {
        eat(str);
        a *= parse_function(&pf_tab[POW], str, err);
    }
    else if (peek(str) == MATHS_DIV)
    {
        eat(str);
        a /= parse_function(&pf_tab[POW], str, err);
    }
    else
        *err = 1;

    return a;
}


static long sum_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_PLUS)
    {
        eat(str);
        a += parse_function(&pf_tab[FACT], str, err);
    }
    else if (peek(str) == MATHS_MINUS)
    {
        eat(str);
        a -= parse_function(&pf_tab[FACT], str, err);
    }
    else
        *err = 1;

    return a;
}


static long bit_op_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_BIT_AND)
    {
        eat(str);
        a &= parse_function(&pf_tab[SUM], str, err);
    }
    else if (peek(str) == MATHS_BIT_OR)
    {
        eat(str);
        a |= parse_function(&pf_tab[SUM], str, err);
    }
    else if (peek(str) == MATHS_BIT_XOR)
    {
        eat(str);
        a ^= parse_function(&pf_tab[SUM], str, err);
    }
    else
        *err = 1;

    return a;
}


static long logical_and_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_AND)
    {
        eat(str);
        a = (a && parse_function(&pf_tab[BIT_OP], str, err)) ;
    }
    else
        *err = 1;

    return a;
}


static long logical_or_semantic(char **str, long a, int *err)
{
    if (peek(str) == MATHS_OR)
    {
        eat(str);
        a = (a || parse_function(&pf_tab[LOGI_AND], str, err)) ;
    }
    else
        *err = 1;

    return a;
}

