#include <string.h>

#include "utilities/utilities.h"
#include "expand/substitution.h"
#include "expand/path_expand.h"
#include "parser/command.h"
#include "expand/expand.h"



static void remove_char(char *str, size_t index)
{
    memmove(&str[index], &str[index + 1], strlen(str) - index);
}


static void removal_quote(char *str)
{
    for (size_t i = 0; str[i]; i++)
    {
        if (str[i] == '\'')
        {
            remove_char(str, i);
            for (; str[i] && str[i] != '\''; i++)
                continue;
            remove_char(str, i);
        }
        else if (str[i] == '"')
        {
            remove_char(str, i);
            for (; str[i] && str[i] != '"'; i++)
                continue;
            remove_char(str, i);
        }
        else if (str[i] == '\\')
            remove_char(str, i);
    }
}


static int is_ifs(char c)
{
    return (c == ' ' || c == '\n' || c == '\t');
}


static char *get_next_field(char **input)
{
    char *begin = *input;
    for (; *begin && is_ifs(*begin); begin++)
        continue;

    char *end = begin;
    int in_quote = 0;

    for (; *end && (in_quote || !is_ifs(*end)); end++)
    {
        if (*end == '\'' || *end == '"')
            in_quote = !in_quote;
    }

    if (*end != '\0')
    {
        *end = '\0';
        end++;
    }

    *input = end;

    return begin;
}


static void field_spliting(struct command *cmd, char *str)
{
    if (*str == '\0')
    {
        command_append_field(cmd, strdup(""));
        return;
    }

    char *field = NULL;
    char *next_field = str;

    while ((field = get_next_field(&next_field)) && *field && !is_ifs(*field))
        command_append_field(cmd, strdup(field));
}


static void remove_blank_end(char *str)
{
    char *end = str;
    for (; *end; end++)
        continue;

    end--;
    for (; end >= str && is_ifs(*end); end--)
        *end = '\0';
}


static void map(char **str_ref, int(* sub)(char **, size_t))
{
    char *str = *str_ref;
    int in_single_quote = 0;
    int is_tilde = 1;
    for (size_t i = 0; str[i] != '\0'; i++)
    {
        if (str[i] == '"')
            is_tilde = (is_tilde ? 0 : 1);

        if (str[i] == '\'')
            in_single_quote = (in_single_quote ? 0 : 1);

        else if (str[i] == '\\' && !in_single_quote)
            i++;
        else if (!in_single_quote && ( sub != tilde_expansion || is_tilde)
                                    && sub(str_ref, i))
        {
            str = *str_ref;
            i--;
        }
    }
}


void expand_cmd(struct command *cmd)
{
    char *str = cmd->argv[0];

    map(&str, tilde_expansion);
    map(&str, parameter_expansion);
    map(&str, command_substitution);
    map(&str, arithmetic_expansion);

    cmd->argv[0] = NULL;
    cmd->size = 0;

    field_spliting(cmd, str);
    mfree(str);
    
    char *tmp;
    for (size_t i = 0; i < cmd->size; i++)
    {
        tmp = cmd->argv[i];
        cmd->argv[i] = path_expand(cmd->argv[i]);
        mfree(tmp);
        removal_quote(cmd->argv[i]);
        remove_blank_end(cmd->argv[i]);
    }
}


char *expand_word(char *str)
{
    map(&str, tilde_expansion);
    map(&str, parameter_expansion);
    map(&str, command_substitution);
    map(&str, arithmetic_expansion);

    removal_quote(str);
    return str;
}

