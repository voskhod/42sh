#include <sys/types.h>
#include <sys/wait.h>
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "parser/ast.h"
#include "parser/command.h"
#include "expand/maths_compute.h"
#include "execute/variable.h"
#include "utilities/utilities.h"
#include "execute/exec_from_input.h"

static char *string_insert(char *str1, char *str2, int size)
{
    char *new_line = NULL;
    asprintf(&new_line, "%s%s%s", str1, str2, str1 + size);
    mfree(str1);

    return new_line;
}


int tilde_expansion(char **str_ref, size_t i)
{
    char *str = *str_ref;

    if (str[i] != '~')
        return 0;

    str[i] = '\0';

    if (str[i + 1] == '+')
        *str_ref = string_insert(str, getenv("PWD"), i + 2);

    else if ((str[i + 1] == '/' || isspace(str[i + 1]) || str[i + 1] == '\0')
                && (i == 0 || str[i - 1] == ' '))
        *str_ref = string_insert(str, getenv("HOME"), i + 1);

    else if (str[i + 1] == '-' && (i == 0 || str[i - 1] == ' '))
        *str_ref = string_insert(str, variable_get("OLDPWD"), i + 2);

    else
    {
        str[i] = '~';
        return 0;
    }

    return 1;
}


int parameter_expansion(char **str_ref, size_t i)
{
    char *str = *str_ref;
    if (str[i] != '$' || str[i + 1] == '(')
        return 0;

    str[i] = '\0';

    char *var_name = str + i + 1;
    char *var_value = NULL;
    int size = 0;
    if (*var_name == '{')
    {
        var_name++;
        size = strcspn(var_name, "}");

        if (var_name[size] == '\0')
        {
            warnx("Missing ')'");
            return 0;
        }

        var_name[size] = '\0';
        size++;
        var_value = variable_get(var_name);
    }
    else
        var_value = variable_get_longest_match(var_name, &size);

    if (var_value[0] == '\0')
        size++;

    *str_ref = string_insert(str, var_value, size + var_name - str);

    return 1;
}


char *get_cmd_output(FILE *process_output)
{
    if (process_output == NULL)
        return NULL;

    size_t n = 0;
    char *str_cmd_output = NULL;
    if (getline(&str_cmd_output, &n, process_output) != -1)
    {
        char *tmp = NULL;
        char *to_append = NULL;

        while (getline(&to_append, &n, process_output) != -1)
        {
            asprintf(&tmp, "%s%s", str_cmd_output, to_append);

            mfree(str_cmd_output);
            str_cmd_output = tmp;
        }

        mfree(to_append);
    }
    else
    {
        mfree(str_cmd_output);
        str_cmd_output = strdup("");
    }

    return str_cmd_output;
}


char *execute_subshell(char *cmd_input)
{
    int fd[2];
    pipe(fd);

    pid_t pid = fork();
    if (pid == 0) /* child */
    {
        close(fd[0]);
        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);

        int return_code = execute_from_str(cmd_input, 0);

        exit(return_code);
    }

    close(fd[1]);
    FILE *cmd_output = fdopen(dup(fd[0]), "r");
    close(fd[0]);

    char *str_output = get_cmd_output(cmd_output);

    fclose(cmd_output);
    int status;
    waitpid(pid, &status, 0);

    return str_output;
}


int command_substitution(char **str_ref, size_t i)
{
    char *str  = *str_ref;
    char *begin_cmd = NULL;

    if (str[i] == '$' && str[i + 1] == '(' && str[i + 2] != '(')
        begin_cmd = str + i + 2;
    else if (str[i] == '`')
        begin_cmd = str + i + 1;
    else
        return 0;

    str[i] = '\0';
    int size = strcspn(begin_cmd, ")`");
    if (begin_cmd[size] == '\0')
    {
        warnx("Missing ')'");
        return 0;
    }

    begin_cmd[size] = '\0';

    char *new_line = NULL;
    if (size != 0)
    {
        char *cmd_result = execute_subshell(begin_cmd);

        asprintf(&new_line, "%s%s%s", str, cmd_result, begin_cmd + size + 1);
        mfree(cmd_result);
    }
    else
        asprintf(&new_line, "%s%s", str, begin_cmd + 1);

    mfree(*str_ref);
    *str_ref = new_line;

    return 1;
}


int arithmetic_expansion(char **str_ref, size_t i)
{
    char *str = *str_ref;

    if (str[i] != '$' || str[i + 1] != '(' || str[i + 2] != '(')
        return 0;

    int size = 3;
    int nb_paren_open = 2;
    for (; str[size + i] && nb_paren_open != 0; size++)
    {
        if (str[size + i] == '(')
            nb_paren_open++;
        else if (str[size + i] == ')')
            nb_paren_open--;
    }

    if (nb_paren_open != 0)
    {
        warnx("Missing '))'");
        return 0;
    }

    str[i] = '\0';
    str[i + size - 2] = '\0';

    int err;
    long r = maths_compute(str + i + 3, &err);

    char *new_line = NULL;
    asprintf(&new_line, "%s%ld%s", str, r, str + size + i);
    mfree(str);

    *str_ref = new_line;

    return 1;
}

