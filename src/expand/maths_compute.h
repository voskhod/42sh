/**
** \file maths_compute.h
** \brief Perform the arithmetic expansion.
*/
#ifndef MATHS_COMPUTE_H
#define MATHS_COMPUTE_H


/**
** \fn long maths_compute(char *str, int *err)
** \brief Perform the arithmetic expansion.
**
** \param str The arithmetic expression in str
** \param err If err contain -1 an error append
** \return The result of the arithmetic expansion
*/
long maths_compute(char *str, int *err);

#endif /* MATHS_COMPUTE_H */
