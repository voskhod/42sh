/**
** \file substitution.h
** \brief Perform tilde, parameter expansion,command substitution and
**        arithmetic expansion.
*/
#ifndef SUBSTITUTION_H
#define SUBSTITUTION_H

/**
** \fn int try_substition(char **str, size_t i, int is_tilde)
** \brief Perform the tilde expansions.
**
** \param str A pointer to the input string
** \param i The offset
** \return 0 if the substitution failed, return 1 othewise
*/
int tilde_expansion(char **str_ref, size_t i);

/**
** \fn int try_substition(char **str, size_t i, int is_tilde)
** \brief Perform the parameter expansions.
**
** \param str A pointer to the input string
** \param i The offset
** \return 0 if the substitution failed, return 1 othewise
*/
int parameter_expansion(char **str_ref, size_t i);

/**
** \fn int try_substition(char **str, size_t i, int is_tilde)
** \brief Perform the command expansions.
**
** \param str A pointer to the input string
** \param i The offset
** \return 0 if the substitution failed, return 1 othewise
*/
int command_substitution(char **str_ref, size_t i);

/**
** \fn int try_substition(char **str, size_t i, int is_tilde)
** \brief Perform the command expansions.
**
** \param str A pointer to the input string
** \param i The offset
** \return 0 if the substitution failed, return 1 othewise
*/
int arithmetic_expansion(char **str_ref, size_t i);
#endif /* SUBSTITUTION_H */
