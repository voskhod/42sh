#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "execute/exec_from_input.h"
#include "interface/configure_shell.h"
#include "utilities/utilities.h"
#include "interface/option.h"
#include "execute/variable.h"
#include "builtin/shopt.h"



int main(int argc, char *argv[])
{
    shell_init();
    int return_code = EXIT_SUCCESS;

    struct option *opt = get_option(argc, argv);
    if (opt == NULL)
        return EXIT_SUCCESS;

    if (!opt->norc)
    {
        char *path;
        char *home = getenv("HOME");;
        if (home != NULL)
        {
            asprintf(&path, "%s/.42shrc", home);
            return_code = execute_from_file(path, g_shopt->ast_print);
            variable_set_from_int("?", return_code);
            mfree(path);
        }
        execute_from_file("/etc/42shrc", g_shopt->ast_print);
    }

    if (opt->file)
    {
        return_code = execute_from_file(opt->file, g_shopt->ast_print);
        if (return_code != EXIT_SUCCESS)
            errx(return_code, "Could not open script: %s", opt->file);

    }
    else if (opt->command_input != NULL)
        return_code = execute_from_str(opt->command_input, g_shopt->ast_print);
    else
        return_code = execute_from_stdin(g_shopt->ast_print);

    variable_set_from_int("?", return_code);

    option_destroy(opt);

    shell_quit();

    return return_code;
}
