#include <stdio.h>
#include <stddef.h>
#include <err.h>

#include "print/print_ast.h"
#include "lexer/lexer.h"
#include "parser/parser.h"
#include "parser/ast.h"
#include "utilities/utilities.h"

static void print_ast_cmd(struct command *cond, FILE *fd, char prev[])
{
    fprintf(fd, "\"%s\" -> \"", prev);
    size_t nb = 0;
    while (nb < cond->size)
    {
        if (nb + 1 == cond->size)
            fprintf(fd, "%s", cond->argv[nb]);
        else
            fprintf(fd, "%s ", cond->argv[nb]);

        nb++;
    }
    fprintf(fd, "\";\n");
}

static void print_ast_if(struct if_node *if_node, FILE *fd, char prev[])
{
    fprintf(fd, "\"%s\"", prev);
    char branch[] = "if";
    fprintf(fd, " -> \"%s\";\n", branch);
    print_ast(if_node->cond, fd, branch);
    print_ast(if_node->if_body, fd, branch);
    if (if_node->else_body != NULL)
    {
        print_ast(if_node->else_body, fd, branch);
    }
}


static void print_ast_and_or(struct and_or_node *a_or_n, FILE *fd, char prev[])
{
    fprintf(fd, "\"%s\"", prev);
    char *branch = NULL;
    if (a_or_n->type == AND_TYPE)
        branch = "&&";
    else
        branch = "||";
    fprintf(fd, " -> \"%s\";\n", branch);
    print_ast(a_or_n->left, fd, branch);
    print_ast(a_or_n->right, fd, branch);
}


static void print_ast_while(struct while_node *while_n, FILE *fd, char prev[])
{
    if (while_n != NULL)
    {
        fprintf(fd, "\"%s\"", prev);
        char branch[] = "while";
        fprintf(fd, " -> \"%s\";\n", branch);
        print_ast(while_n->cond, fd, branch);
        print_ast(while_n->body, fd, branch);
    }
}


static void print_ast_until(struct until_node *until_n, FILE *fd, char prev[])
{
    if (until_n != NULL)
    {
        fprintf(fd, "\"%s\"", prev);
        char branch[] = "until";
        fprintf(fd, " -> \"%s\";\n", branch);
        print_ast(until_n->cond, fd, branch);
        print_ast(until_n->body, fd, branch);
    }
}


static void print_ast_list_break(struct compound_list_break *l, FILE *fd,
                                    char prev[])
{
    if (l != NULL)
    {
        fprintf(fd, "\"%s\"", prev);
        char branch[] = "list_break";
        fprintf(fd, " -> \"%s\";\n", branch);
        print_ast(l->current, fd, branch);
        print_ast_list_break(l->next, fd, branch);
    }
}

static void print_ast_pipe(struct pipeline *pipe, FILE *fd, char prev[])
{
    if (pipe != NULL)
    {
        fprintf(fd, "\"%s\"", prev);
        char branch[] = "pipeline";
        fprintf(fd, " -> \"%s\";\n", branch);
        print_ast(pipe->left, fd, branch);
        print_ast(pipe->right, fd, branch);
    }
}


static void print_ast_for(struct for_node *for_n, FILE *fd, char prev[])
{
    if (for_n != NULL)
    {
        fprintf(fd, "\"%s\"", prev);
        char branch[] = "for";
        fprintf(fd, " -> \"%s\";\n", branch);
        fprintf(fd, "\"%s\" -> \"inter %lu\"", branch, for_n->vector->size);
        print_ast(for_n->body, fd, branch);
    }
}



void print_ast(struct ast *ast, FILE *fd, char prev[])
{
    if (ast == NULL)
        return;

    if (ast->type == AST_NODE_COMMAND)
        print_ast_cmd(ast->statement.cmd, fd, prev);
    else if (ast->type == AST_NODE_IF)
        print_ast_if(ast->statement.if_node, fd, prev);
    else if (ast->type == AST_NODE_AND_OR)
        print_ast_and_or(ast->statement.and_or_node, fd, prev);
    else if (ast->type == AST_NODE_WHILE)
        print_ast_while(ast->statement.while_node, fd, prev);
    else if (ast->type == AST_NODE_LIST_BREAK)
        print_ast_list_break(ast->statement.cpd_list_break, fd, prev);
    else if (ast->type == AST_NODE_UNTIL)
        print_ast_until(ast->statement.until_node, fd, prev);
    else if (ast->type == AST_NODE_FOR)
        print_ast_for(ast->statement.for_node, fd, prev);
    else if (ast->type == AST_NODE_PIPE)
        print_ast_pipe(ast->statement.pipeline, fd, prev);

    print_ast(ast->next, fd, prev);
}


void init_print_ast(struct ast *ast)
{
    FILE *fd;
    fd = fopen("graph.dot", "w+");
    if (fd != NULL)
    {
        fprintf(fd, "digraph ast_tree {\n");
        char prev[] = "head";
        print_ast(ast, fd, prev);
        fprintf(fd, "}");
        fclose(fd);
    }
    else
        errx(1, "Can't create graph.dot");
}
