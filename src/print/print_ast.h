/**
** \file print_ast.h
** \brief The ast printer
*/
#ifndef PRINT_AST_H
#define PRINT_AST_H

#include <stdio.h>

#include "lexer/lexer.h"
#include "parser/ast.h"

/**
** \fn void print_ast(struct ast *ast, FILE *fd, char prev[])
** \brief Print an ast.
**
** \param ast The ast to print
** \param fd The file in which we print our ast
** \param prev The previous node
*/
void print_ast(struct ast *ast, FILE *fd, char prev[]);

/**
** \fn void init_print_ast(struct ast *ast)
** \brief Initialise differente variable for print an ast.
**
** \param ast The ast to print
*/
void init_print_ast(struct ast *ast);

#endif /* PRINT_AST_H */
