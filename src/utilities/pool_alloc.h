/**
** \file pool_alloc.h
** \brief A pool allocator for easy memory management.
**
** This module is a wrapper around the c' memory management functions. It add
** an 'free all' for easier memory management and a better error management.
*/
#ifndef XALLOC_H
#define XALLOC_H

#include <stddef.h>
#include "utilities/hash_map.h"


struct pool
{
    struct hmap *mem;
};


/**
** \fn void pool_init(void)
** \brief Initialize a pool.
**
** Initialize a pool.
*/
struct pool *pool_init(void);

/**
** \fn void pool_destroy(void)
** \brief Destroy the pool and free all the pointers stil not free.
**
** \param pool The pool to destroy
**
** Destroy the pool and free all the pointers stil not free.
*/
void pool_destroy(struct pool *pool);


/**
** \fn void *pool_alloc(size_t size)
** \brief Same behaviour has malloc(3) execpt it update the pool and exit
**        on error.
**
** \param size The size of the desire allocation
** \param pool The pool in witch the new pointer will be store
** \return A valid pointer wich must be free by xfree
**
** Same behaviour has malloc(3) execpt it update the pool sytem and exit
** on error.
*/
void *pool_alloc(size_t size, struct pool *pool);

/**
** \fn void pool_free(void *ptr)
** \brief Same behaviour has free(3) execpt it update the pool.
**
** \param pool The pool in witch the pointer to free is in
**
** \param ptr The pointer to free
**
** Same behaviour has free(3) execpt it update the pool. If the pointer was not
** return by one of the pool alloc's functions, it is still free
** and the anti-leak system is left unchanged.
*/
void pool_free(void *ptr, struct pool *pool);

/**
** \fn void *pool_calloc(size_t nmemb, size_t size)
** \brief Same behaviour cas calloc(3) execpt it update the pool sytem and exit
**          on error.
**
** \param nmemb The number of element desire
** \param size The size of one element
** \param pool The pool in witch the new pointer will be store
** \return A valid pointer wich must be free by xfree
**
** Same behaviour has calloc(3) execpt it update the pool and exit on error.
*/
void *pool_calloc(size_t nmemb, size_t size, struct pool *pool);

/**
** \fn void *pool_realloc(void *ptr, size_t size)
** \brief Same behaviour cas realloc(3) execpt it update the pool and exit on
**          error.
**
** \param ptr The memory block to be changed
** \param size The size of the new desire size
** \param pool The pool in witch the pointer is store
** \return A valid pointer wich must be free by xfree
**
** Same behaviour has realloc(3) execpt it update the pool and exit on error.
*/
void *pool_realloc(void *ptr, size_t size, struct pool *pool);

/**
** \fn char *strndup(const char *s, size_t n)
** \brief Add a new pointer to the pool
**
** \param ptr The pointer to track
** \param pool The pool in witch the pointer is store
*/
void pool_add(void *ptr, struct pool *pool);

#endif /* XALLOC_H */
