#include <stddef.h>
#include <string.h>

#include "utilities/utilities.h"
#include "utilities/vector.h"


#define DEFAULT_SIZE 16


struct vector *vector_init(void)
{
    struct vector *vector = mcalloc(1, sizeof(struct vector));
    vector->capacity = DEFAULT_SIZE;
    vector->data = mcalloc(DEFAULT_SIZE, sizeof(char *));
    return vector;
}


void vector_destroy(struct vector *vector)
{
    for (size_t i = 0; i < vector->size; i++)
        mfree(vector->data[i]);
    mfree(vector->data);
    mfree(vector);
}


void vector_append(struct vector *vector, char *str)
{
    if (vector->capacity <= vector->size)
    {
        vector->data = mrealloc(vector->data,
                vector->capacity * 2 * sizeof(char *));
        vector->capacity *= 2;
    }
    vector->data[vector->size] = strdup(str);
    vector->size++;
}


void vector_del_at(struct vector *vector, size_t index)
{
    if (index >= vector->size)
        return;
    char *tmp = vector->data[index];
    for (size_t i = index; i < vector->size - 1; i++)
        vector->data[i] = vector->data[i + 1];
    mfree(tmp);
    vector->size--;
}
