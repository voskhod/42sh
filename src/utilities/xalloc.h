/**
** \file xalloc.h
** \brief A wrapper around the c' functions for memory management.
**
** This module is a wrapper around the c' memory management functions. It add
** an anti-leak system and a better error management.
*/
#ifndef XALLOC_H
#define XALLOC_H

#include <stddef.h>


/**
** \fn void xalloc_init(void)
** \brief Initialize the anti-leak system.
**
** Initialize the anti-leak system.
*/
void xalloc_init(void);

/**
** \fn void xalloc_destroy(void)
** \brief Destroy the anti-leak system and free all the pointers stil not free.
**
** Destroy the anti-leak system and free all the pointers stil not free.
** \warning This function must be call only at the end of the program. If not
**          pointers still in use may be free.
*/
void xalloc_destroy(void);


/**
** \fn void *mmalloc(size_t size)
** \brief Same behaviour has malloc(3) execpt it update the anti-leak sytem and
**          exit on error.
**
** \param size The size of the desire allocation
** \return A valid pointer wich must be free by xfree
**
** Same behaviour has malloc(3) execpt it update the anti-leak sytem and exit
** on error.
*/
void *mmalloc(size_t size);

/**
** \fn void mfree(void *ptr)
** \brief Same behaviour has free(3) execpt it update the anti-leak sytem.
**
** \param ptr The pointer to free
**
** Same behaviour has free(3) execpt it update the anti-leak sytem. If the
** pointer was not return by one of the xalloc's functions, it is still free
** and the anti-leak system is left unchanged.
*/
void mfree(void *ptr);

/**
** \fn void *mcalloc(size_t nmemb, size_t size)
** \brief Same behaviour cas malloc(3) execpt it update the anti-leak sytem and
**          exit on error.
**
** \param nmemb The number of element desire
** \param size The size of one element
** \return A valid pointer wich must be free by xfree
**
** Same behaviour has calloc(3) execpt it update the anti-leak sytem and exit
** on error.
*/
void *mcalloc(size_t nmemb, size_t size);

/**
** \fn void *mrealloc(void *ptr, size_t size)
** \brief Same behaviour cas realloc(3) execpt it update the anti-leak sytem
**      and exit on error.
**
** \param ptr The memory block to be changed
** \param size The size of the new desire size
** \return A valid pointer wich must be free by xfree
**
** Same behaviour has realloc(3) execpt it update the anti-leak sytem and exit
** on error.
*/
void *mrealloc(void *ptr, size_t size);

#endif /* XALLOC_H */
