/**
** \file vector.h
** \brief A simple vector of string struct
**
** This module create a basic vector struct
**
*/
#ifndef VECTOR_H
#define VECTOR_H

#include <stddef.h>

/**
** \struct vector
** \brief Struct vector
**
** This struct contaon an array of strings
*/
struct vector
{
    size_t size;
    size_t capacity;
    char **data;
};

/**
** \fn struct vector *vector_init(void)
** \brief Create a struct vector
**
** \return A struct vector initialized
**
** Create a vector
*/
struct vector *vector_init(void);

/**
** \fn void vector_destroy(struct vector *vector)
** \brief Free a struct vector
**
** \param the struct vector to free
**
** Free a struct vector
*/
void vector_destroy(struct vector *vector);

/**
** \fn void vector_append(struct vector *vector, char *str)
** \brief add str at the end of vector
**
** \param the current vector already init and the str to add
**
** add str at the end of vector
*/
void vector_append(struct vector *vector, char *str);

/**
** \fn void vector_del_at(struct vector *vector, size_t index)
** \brief delete a char at index
**
** \param the current vector, and the index to delete
**
** delete the data at index i
** if index >= vector->size do nothing
*/
void vector_del_at(struct vector *vector, size_t index);


#endif
