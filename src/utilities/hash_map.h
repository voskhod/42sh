/**
** \file hash_map.h
** \brief Data storage with quick insertion and deletion.
**
** This module is a data storage with quick insertion and deletion but
** without order.
*/
#ifndef HASH_MAP_H
#define HASH_MAP_H


/**
** \struct hmap
** \brief An hash map
**
** An hash map containing numbers.
*/
struct hmap;


/**
** \fn struct hmap *hmap_init(void)
** \brief Create and initialize an hash map.
**
** \return The initialized hash map
**
** Create and initialize an hash map.
*/
struct hmap *hmap_init(void);

/**
** \fn void hmap_destroy(struct hmap *hmap)
** \brief Destroy an hash map.
**
** \param hmap The hmap to destroy.
**
** Destroy the given hash map.
*/
void hmap_destroy(struct hmap *hmap);


/**
** \fn void hmap_insert(struct hmap *hmap, size_t elm)
** \brief Insert a number in the hash map.
**
** \param hmap An initialize hmap by hmap_init
** \param elm The number to insert
**
** Insert the given number in the hash map.
*/

void hmap_insert(struct hmap *hmap, size_t elm);
/**
** \fn int hmap_delete(struct hmap *hmap, size_t elm)
** \brief Delete a number in the hash map.
**
** \param hmap An initialize hmap by hmap_init
** \param elm The number to delete
** \return 0 if the element has been delete, 1 else
**
** Delete the given number in the hash map, if it doesn't exit yet return 1 and
** the hmap is left unchanged.
*/

int hmap_delete(struct hmap *hmap, size_t elm);

/**
** \fn size_t hmap_pop(struct hmap *hmap, int *err)
** \brief Delete and return an element.
**
** \param hmap An initialize hmap by hmap_init
** \param err A pointer where the error value will be store
** \return The number deleted
**
** Has the same behaviour as hmap_delete execpt, the error code is store in err
** and the deleted element is return.
*/
size_t hmap_pop(struct hmap *hmap, int *err);

#endif /* HASH_MAP_H */
