#include <stdlib.h>
#include <err.h>

#include "utilities/pool_alloc.h"


static inline size_t ptr_to_nb(void *ptr)
{
    return (size_t)ptr;
}


static inline void *nb_to_ptr(size_t nb)
{
    return (void *)nb;
}


struct pool *pool_init(void)
{
    struct pool *p = malloc(sizeof(struct pool));
    if (p == NULL)
        errx(EXIT_FAILURE, "Fail to allocate memory");

    p->mem = hmap_init();
    return p;
}


void pool_destroy(struct pool *pool)
{
    void *to_free;
    int err;

#ifdef DEBUG
    size_t ptr_count = 0;
#endif /* DEBUG */

    to_free = nb_to_ptr(hmap_pop(pool->mem, &err));
    while (err == 0)
   {
#ifdef DEBUG
        ptr_count++;
#endif /* DEBUG */

        free(to_free);
        to_free = nb_to_ptr(hmap_pop(pool->mem, &err));
    }

#ifdef DEBUG
    if (ptr_count != 0)
        warnx("Still %zu block allocated!", ptr_count);
#endif /* DEBUG */

    hmap_destroy(pool->mem);

    free(pool);
}


void *pool_alloc(size_t size, struct pool *pool)
{
    void *ptr = malloc(size);
    if (ptr == NULL)
        errx(1, "Memory allocation failed");

    hmap_insert(pool->mem, ptr_to_nb(ptr));

    return ptr;
}


void pool_free(void *ptr, struct pool *pool)
{

    if (ptr == NULL)
        return;

    hmap_delete(pool->mem, ptr_to_nb(ptr));

    free(ptr);
}


void *pool_calloc(size_t nmemb, size_t size, struct pool *pool)
{
    void *ptr = calloc(nmemb, size);
    if (ptr == NULL)
        errx(1, "Memory allocation failed");

    hmap_insert(pool->mem, ptr_to_nb(ptr));

    return ptr;
}


void *pool_realloc(void *ptr, size_t size, struct pool *pool)
{
    void *new_ptr = realloc(ptr, size);
    if (new_ptr == NULL)
        errx(1, "Memory allocation failed");

    if (new_ptr != ptr)
    {
        if (ptr != NULL)
            hmap_delete(pool->mem, ptr_to_nb(ptr));

        if (size != 0)
            hmap_insert(pool->mem, ptr_to_nb(new_ptr));
    }

    return new_ptr;
}


void pool_add(void *ptr, struct pool *pool)
{
    hmap_insert(pool->mem, ptr_to_nb(ptr));
}
