/**
** \file utilities.h
** \brief Some macro definition and include err.h and xalloc.h.
*/
#ifndef UTILITIES_H
#define UTILITIES_H

#include <err.h>

#include "utilities/xalloc.h"


#define EXIT_SUCCESS 0
#define EXIT_EXEC_ERROR 1
#define EXIT_SET_UNSET_ERROR 1
#define EXIT_SHOPT_ERROR 1
#define EXIT_WRG_H_ARG 1
#define EXIT_WRG_OPT 2
#define EXIT_SYNTAX_ERR 2
#define EXIT_WRG_PERM 100
#define EXIT_NOT_EXEC 126
#define EXIT_UNK_CMD 127
#define EXIT_FATAL_ERROR 129

#endif /* UTILITIES_H */
