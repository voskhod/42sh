#include <stdlib.h>
#include <err.h>

#include "utilities/pool_alloc.h"
#include "utilities/xalloc.h"


static struct pool *pool = NULL;

void xalloc_init(void)
{
    pool = pool_init();
}


void xalloc_destroy(void)
{

    pool_destroy(pool);

}

void *mmalloc(size_t size)
{
    return pool_alloc(size, pool);
}


void mfree(void *ptr)
{
    pool_free(ptr, pool);
}


void *mcalloc(size_t nmemb, size_t size)
{
    return pool_calloc(nmemb, size, pool);
}


void *mrealloc(void *ptr, size_t size)
{
    return pool_realloc(ptr, size, pool);
}
