#include <stdlib.h>

#include "utilities/utilities.h"

#define HASH_MAP_SIZE 200



struct list
{
    size_t val;
    struct list *next;
};

struct hmap{
    size_t size;
    struct list **list;
};


struct hmap *hmap_init(void)
{
    struct hmap *hmap = malloc(sizeof(struct hmap));
    if (hmap == NULL)
        err(EXIT_FAILURE, "Fail to allocate memory");

    hmap->size = HASH_MAP_SIZE;
    hmap->list = calloc(HASH_MAP_SIZE, sizeof(struct list));
    if (hmap->list == NULL)
        err(EXIT_FAILURE, "Fail to allocate memory");

    return hmap;
}


void hmap_destroy(struct hmap *hmap)
{
    if (hmap != NULL)
        free(hmap->list);
    free(hmap);
}


static inline int get_hash(size_t n)
{
    return ( n >> 5) % HASH_MAP_SIZE;
}


static struct list *list_insert(struct list *list, size_t elm)
{
    struct list *new = calloc(1, sizeof(struct list));
    if (new == NULL)
        err(EXIT_FAILURE, "Fail to allocate memory");

    new->val = elm;

    if (list != NULL)
        new->next = list;

    return new;
}


void hmap_insert(struct hmap *hmap, size_t elm)
{
    int pos = get_hash(elm);

    hmap->list[pos] = list_insert(hmap->list[pos], elm);
}


static struct list *list_pop_head(struct list *list, size_t *elm, int *err)
{
    if (list == NULL)
    {
        *err = 1;
        return list;
    }
    else
        *err = 0;

    *elm = list->val;
    struct list *next = list->next;
    free(list);

    return next;
}


static struct list *list_delete(struct list *list, size_t elm, int *err)
{
    if (list == NULL)
    {
        *err = 1;
        return list;
    }

    if (list->val == elm)
        list = list_pop_head(list, &elm, err);
    else
        list->next = list_delete(list->next, elm, err);

    return list;
}


int hmap_delete(struct hmap *hmap, size_t elm)
{
    int pos = get_hash(elm);

    int err = 0;
    hmap->list[pos] = list_delete(hmap->list[pos], elm, &err);

    return err;
}

size_t hmap_pop(struct hmap *hmap, int *err)
{
    *err = 0;
    size_t elm = 0;

    for (size_t i = 0; i < HASH_MAP_SIZE; i++)
    {
        hmap->list[i] = list_pop_head(hmap->list[i], &elm, err);

        if (*err == 0)
            break;
    }

    return elm;
}
