#include <string.h>

#include "utilities/utilities.h"
#include "interface/input.h"
#include "lexer/delimiter.h"
#include "lexer/lexer.h"
#include "lexer/alias_sub.h"


struct token_type_str
{
    char *ref;
    enum token_type type;
};

static struct token_type_str token_str[] =
{
    { "\n", TOKEN_ENDLINE },
    { ";", TOKEN_SEMI_COLON },
    { "&", TOKEN_BG },
    { "for", TOKEN_FOR },
    { "||", TOKEN_OR },
    { "&&", TOKEN_AND},
    { "if", TOKEN_IF },
    { "then", TOKEN_THEN },
    { "esac", TOKEN_ESAC},
    { "else", TOKEN_ELSE },
    { "elif", TOKEN_ELIF },
    { "fi", TOKEN_FI },
    { "while", TOKEN_WHILE },
    { "until", TOKEN_UNTIL },
    { "do", TOKEN_DO },
    { "in", TOKEN_IN },
    { "done", TOKEN_DONE },
    { ">", TOKEN_REDIRECT_OUT },
    { "<", TOKEN_REDIRECT_IN },
    { ">>", TOKEN_REDIRECT_OUT_APP },
    { "<<-", TOKEN_REDIRECT_IN_APP_NT },
    { "<<", TOKEN_REDIRECT_IN_APP },
    { ">&", TOKEN_REDIRECT_OUT_DUP },
    { "<&", TOKEN_REDIRECT_IN_DUP },
    { ">|", TOKEN_REDIRECT_OUT_FORCE },
    { "<>", TOKEN_REDIRECT_BOTH },
    { "!", TOKEN_NOT },
    { "|", TOKEN_PIPE },
    { "case", TOKEN_CASE},
    { ";;", TOKEN_DBL_SEMI_COLON},
    { "(", TOKEN_LEFT_PARENTHESIS },
    { ")", TOKEN_RIGHT_PARENTHESIS },
    { "{", TOKEN_LEFT_BRACKET },
    { "}", TOKEN_RIGHT_BRACKET },
    { "function", TOKEN_FUNCTION }
};


static enum token_type get_token_type(struct input *input, size_t token_size)
{
    if (token_size == 2 && !strncmp("<<-", input->str + input->cur, 3))
        return TOKEN_REDIRECT_IN_APP_NT;

    if (token_size == 1 && is_ionumber(&input->str[input->cur]))
        return TOKEN_IO_NUMBER;

    for (size_t i = 0; i < sizeof(token_str) / sizeof(*token_str); i++)
    {

        if (!strncmp(token_str[i].ref, input->str + input->cur, token_size))
        {
            if (token_str[i].type == TOKEN_ENDLINE ||
                        strlen(token_str[i].ref) == token_size)
                return token_str[i].type;
        }
    }

    if (token_size == 1 && is_ionumber(input->str + input->cur))
        return TOKEN_IO_NUMBER;

    return TOKEN_WORD;
}


void get_assign_word(struct input *input, struct token *token,
                                                size_t *token_size)
{
    (*token_size)++;
    token->type = TOKEN_ASSIGN_WORD;

    (*token_size) += get_token_size(input, *token_size);

    mfree(token->value);
    token->value = strndup(input->str + input->cur, *token_size);
}


static int is_assign_word(struct input *input, size_t token_size)
{
    return (input->str[input->cur + token_size] == '=');
}


static int is_fun_def(struct input *input, size_t token_size)
{
    return (input->str[input->cur + token_size - 1] != '$'
            && input->str[input->cur + token_size] == '('
            && input->str[input->cur + token_size + 1] == ')');
}


struct token *get_token(struct lexer *lexer, size_t *token_size)
{
    lexer->input = input_update(lexer->input);

    if (lexer->input->str == NULL)
    {
        *token_size = 0;
        return token_eof();
    }

    lexer->input->cur = skip_space(lexer->input->str, lexer->input->cur);

    *token_size = get_token_size(lexer->input, 0);

    enum token_type type = get_token_type(lexer->input, *token_size);
    if (type == TOKEN_REDIRECT_IN_APP_NT)
        (*token_size)++;

    struct token *token = token_init();
    token->type = type;

    token->value = strndup(lexer->input->str + lexer->input->cur, *token_size);

    if (token->type == TOKEN_WORD)
    {
        char *alias_value;
        if ((alias_value = is_alias(token->value)) != NULL)
        {
            mfree(token->value);
            token->value = strdup(alias_value);
        }
        if (is_assign_word(lexer->input, *token_size))
            get_assign_word(lexer->input, token, token_size);
        else if (is_fun_def(lexer->input, *token_size))
        {
            token->value[*token_size] = '\0';
            token->type = TOKEN_FUN_NAME;
            *token_size += 2;
        }
    }

    if (token->type == TOKEN_ENDLINE)
        *token_size = 1;

    return token;
}


struct lexer *lexer_init(struct input *input)
{
    struct lexer *lexer = mcalloc(1, sizeof(struct lexer));
    lexer->input = input;

    return lexer;
}


void lexer_destroy(struct lexer *lexer)
{
    if (lexer != NULL)
        input_destroy(lexer->input);

    mfree(lexer);
}


enum token_type lexer_peek(struct lexer *lexer)
{
    size_t token_size;
    struct token *token = get_token(lexer, &token_size);
    enum token_type type = token->type;
    token_destroy(token);

    return type;
}


struct token *lexer_pop(struct lexer *lexer)
{
    size_t token_size;

    struct token *token = get_token(lexer, &token_size);

    lexer->input->cur += token_size;

#ifdef DEBUG
    warnx("lexer: %s \"%s\"", token_to_str(token), token->value);
#endif /* DEBUG */

    return token;
}
