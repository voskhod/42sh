/**
** \file quoting.h
** \brief Cut a string acording to quoting.
*/
#ifndef QUOTING_H
#define QUOTING_H

#include <stddef.h>

#include "interface/input.h"

/*
" "
' '
` `
${  }
$( )
$( ( ) )

*/

/**
** \fn int get_block(struct input *input, int i)
** \brief Return the size of the block delimited by space (non quoted) of text.
**
** \param input The input
** \param i The current position.
** \return The size of the block
*/
int get_block(struct input *input, int i);

#endif /* QUOTING_H */
