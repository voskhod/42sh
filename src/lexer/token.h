/**
** \file token.h
** \brief Initialise and destroy a token.
*/
#ifndef TOKEN_H
#define TOKEN_H

/**
** \enum token_type
** \brief The type of a token.
*/
enum token_type
{
    TOKEN_EOF = 0,
    TOKEN_ENDLINE,
    TOKEN_SEMI_COLON,
    TOKEN_BG,
    TOKEN_IO_NUMBER,
    TOKEN_WORD,
    TOKEN_FOR,
    TOKEN_OR,
    TOKEN_AND,
    TOKEN_IF,
    TOKEN_THEN,
    TOKEN_ELSE,
    TOKEN_ELIF,
    TOKEN_FI,
    TOKEN_WHILE,
    TOKEN_UNTIL,
    TOKEN_DO,
    TOKEN_IN,
    TOKEN_DONE,
    TOKEN_REDIRECT_OUT,
    TOKEN_REDIRECT_IN,
    TOKEN_REDIRECT_IN_APP,
    TOKEN_REDIRECT_OUT_APP,
    TOKEN_REDIRECT_IN_APP_NT,
    TOKEN_REDIRECT_OUT_DUP,
    TOKEN_REDIRECT_IN_DUP,
    TOKEN_REDIRECT_OUT_FORCE,
    TOKEN_REDIRECT_BOTH,
    TOKEN_NOT,
    TOKEN_PIPE,
    TOKEN_CASE,
    TOKEN_ESAC,
    TOKEN_DBL_SEMI_COLON,
    TOKEN_LEFT_PARENTHESIS,
    TOKEN_RIGHT_PARENTHESIS,
    TOKEN_LEFT_BRACKET,
    TOKEN_RIGHT_BRACKET,
    TOKEN_ASSIGN_WORD,
    TOKEN_FUN_NAME,
    TOKEN_FUNCTION,
    TOKEN_SGL_QUOTE
};


/**
** \struct token
** \brief A token with the type of token and it's value.
*/
struct token
{
    enum token_type type;
    char *value;
};


/**
** \fn const char *token_to_str(struct token *token)
** \brief Convert a token to a string.
**
** \param token The token to convert
** \return A string representing the token
**
** Convert a token to a string, with it's type and value.
*/
const char *token_to_str(struct token *token);


/**
** \fn struct token *token_eof(void)
** \brief Create a token eof.
**
** \return An eof token
*/
struct token *token_eof(void);


/**
** \fn struct token *token_init(void)
** \brief Initialise a token.
**
** \return A initilized token.
**
** Initialise a token. All the fileds are initilized at 0.
*/
struct token *token_init(void);

/**
** \fn void token_destroy(struct token *token);
** \brief Destroy a token.
**
** \param token The token to destroy
*/
void token_destroy(struct token *token);

#endif /* TOKEN_H */
