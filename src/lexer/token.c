#include <stddef.h>

#include "utilities/utilities.h"
#include "lexer/token.h"


struct token_type_str
{
    char *str;
    enum token_type type;
};


const char *token_to_str(struct token *token)
{
    static struct token_type_str token_str[] =
    {
        { "eof", TOKEN_EOF },
        { "\\n", TOKEN_ENDLINE },
        { "';'", TOKEN_SEMI_COLON },
        { "'&'", TOKEN_BG },
        { "Io_nb", TOKEN_IO_NUMBER },
        { "for", TOKEN_FOR },
        { "if", TOKEN_IF },
        { "then", TOKEN_THEN },
        { "fi", TOKEN_FI },
        { "&&", TOKEN_AND },
        { "||", TOKEN_OR },
        { "while", TOKEN_WHILE },
        { "until", TOKEN_UNTIL },
        { "do", TOKEN_DO },
        { "in", TOKEN_IN },
        { "done", TOKEN_DONE },
        { "or", TOKEN_OR },
        { "and", TOKEN_AND },
        { "if", TOKEN_IF },
        { "then", TOKEN_THEN },
        { "fi", TOKEN_FI },
        { "else", TOKEN_ELSE },
        { "elif", TOKEN_ELIF },
        { ">", TOKEN_REDIRECT_OUT },
        { "<", TOKEN_REDIRECT_IN },
        { "<<", TOKEN_REDIRECT_IN_APP },
        { "<<-", TOKEN_REDIRECT_IN_APP_NT },
        { "case", TOKEN_CASE },
        { "esac", TOKEN_ESAC },
        { ";;", TOKEN_DBL_SEMI_COLON },
        { "(", TOKEN_LEFT_PARENTHESIS },
        { ")", TOKEN_RIGHT_PARENTHESIS },
        { "'{'", TOKEN_LEFT_BRACKET },
        { "'}'", TOKEN_RIGHT_BRACKET },
        { "var=", TOKEN_ASSIGN_WORD },
        { "fun_dec", TOKEN_FUN_NAME },
        { "function", TOKEN_FUNCTION },
        { "single quote", TOKEN_SGL_QUOTE }
    };

    for (size_t i = 0; i < sizeof(token_str) / sizeof(*token_str); i++)
    {

        if (token_str[i].type == token->type)
            return token_str[i].str;
    }

    return "Word";
}


struct token *token_eof()
{
    struct token *token = token_init();
    token->type = TOKEN_EOF;

    return token;
}


struct token *token_init(void)
{
    return mcalloc(1, sizeof(struct token));
}


void token_destroy(struct token *token)
{
    if (token != NULL)
        mfree(token->value);
    mfree(token);
}
