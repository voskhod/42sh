/**
** \file lexer.h
** \brief Module that performs lexical analysis.
**
** This module cut the input stream in token and identify them.
*/
#ifndef LEXER_H
#define LEXER_H

#include <stddef.h>

#include "token.h"

/**
**\struct lexer
**\brief Struct lexer
**
** This struct contain the input stream and the next token to read.
*/
struct lexer
{
    struct input *input;

    struct token *next_token_to_read;
};


/**
** \fn struct lexer *lexer_init(struct input *input)
** \brief Create a struct lexer.
**
** \param input The input string
** \return A struct lexer initialized
**
** Create a lexer from an input string.
*/
struct lexer *lexer_init(struct input *input);

/**
** \fn void lexer_destroy(struct lexer *lexer)
** \brief Destroy a lexer.
**
** \param lexer The lexer to destroy
**
** Destroy a lexer.
*/
void lexer_destroy(struct lexer *lexer);


/**
** \fn struct token *lexer_peek(struct lexer *lexer)
** \brief Show the next token without cosume it.
**
** \param lexer A lexer initialized by lexer_init().
** \return The type of next token in the input stream.
**
** Show the next token without cosume it from the input stream.
** \warning Do not destroy or free it!
*/
enum token_type lexer_peek(struct lexer *lexer);


/**
** \fn enum token_type lexer_peek_command(struct lexer *lexer)
** \brief Show the next token in context without consume it.
**
** \param A lexer initialized by lexer_init().
** \return The type of the next token in the input stream.
**
** Show the type of the next token in the input stream. But keyword are
** treat as word.
** \warning Do not destory or free the token!
*/
//enum token_type lexer_peek_command(struct lexer *lexer);

/**
** \fn struct token *lexer_pop(struct lexer *lexer)
** \brief Consume the next token in the input stream.
**
** \param lexer A lexer initialized by lexer_init().
** \return The next token in the input stream.
**
** Consume the next token from the input stream. Unlike lexer_peek the token
** return must be destroy.
*/
struct token *lexer_pop(struct lexer *lexer);

/**
** \fn struct token *lexeer_pop_command(struct lexer *lexer)
** \brief Show the next token in input stream and consume it.
**
** \param A lexer initialized by lexer_init().
** \return The next token in the input stream but if his type is a keyword it
** it becomes a token_word.
**
** Consume the next token in the stream 
*/
//struct token *lexer_pop_command(struct lexer *lexer);

#endif /* LEXER_H */
