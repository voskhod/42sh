#include "utilities/utilities.h"
#include "interface/input.h"
#include "lexer/quoting.h"


static int get_block_str(char *str);


int is_delim(char c)
{
    return (c == '"' || c == '\'' || c == '`' || c == '$');
}


static int get_block_single_quote(char *str)
{
    int len = 1;
    for (; str[len] != '\0' && str[len] != '\''; len++)
    {
        if (is_delim(str[len]))
            len += get_block_str(str + len);
    }

    if (str[len] == '\'')
        return len;
    return -1;
}


static int get_block_double_quote(char *str)
{
    int len = 1;
    for (; str[len] != '\0' && str[len] != '"'; len++)
    {
        if (is_delim(str[len]))
            len += get_block_str(str + len);
    }

    if (str[len] == '"')
        return len;
    return -1;
}


static int get_block_back_quote(char *str)
{
    int len = 1;
    for (; str[len] != '\0' && str[len] != '`'; len++)
    {
        if (is_delim(str[len]))
            len += get_block_str(str + len);
    }

    if (str[len] == '`')
        return len;
    return -1;
}


static int get_block_bracket(char *str)
{
    int len = 2;
    for (; str[len] != '\0' && str[len] != '}'; len++)
    {
        if (is_delim(str[len]))
            len += get_block_str(str + len);
    }

    if (str[len] == '}')
        return len;
    return -1;
}


static int get_block_cmd(char *str)
{
    int len = 2;
    for (; str[len] != '\0' && str[len] != ')' ; len++)
    {
        if (is_delim(str[len]))
            len += get_block_str(str + len);
    }

    if (str[len] == ')')
        return len;
    return -1;
}


static int get_block_aryth(char *str)
{
    int len = 3;
    int nb_paren_open = 2;

    for (; str[len] && nb_paren_open != 0; len++)
    {
        if (str[len] == '(')
            nb_paren_open++;
        else if (str[len] == ')')
            nb_paren_open--;
    }

    if (nb_paren_open == 0)
        return len - 1;
    return -1;
}


static int get_block_str(char *str)
{
    if (str[0] == '\'')
        return get_block_single_quote(str);
    else if (str[0] == '"')
        return get_block_double_quote(str);
    else if (str[0] == '`')
        return get_block_back_quote(str);
    else if (str[0] == '$')
    {
        if (str[1] == '{')
            return get_block_bracket(str);

        else if (str[1] == '(')
        {
            if (str[2] == '(')
                return get_block_aryth(str);
            else
                return get_block_cmd(str);
        }
    }

    return 0;
}


int get_block(struct input *input, int i)
{
    char *str = input->str + input->cur + i;

    int len = get_block_str(str);

    return (len == 0) ? 0 : len + 1;
}
