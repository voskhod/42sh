#include <err.h>
#include <ctype.h>

#include "lexer/quoting.h"
#include "lexer/delimiter.h"

static inline int is_special_char(char c)
{
    return (c == '|' || c == '&' || c == ';' || c == '<' || c == '>' 
            || c == '=' || c == ' ' || c == '#' || c == '[' || c == ']'
            || c == '(' || c == ')');
}


static size_t get_special_word_size(char *str, size_t begin)
{
    size_t i = begin;
    for (; str[i] && str[i] != ' ' && str[i] != '\n'
                            && is_special_char(str[i]); i++)
        continue;

    return i - begin;
}


static size_t get_word_size(char *str, size_t begin)
{
    size_t i = begin;
    for (; str[i] && str[i] != ' ' && str[i] != '\n' 
                            && !is_special_char(str[i]); i++)
        continue;

    return i - begin;
}


static inline int is_white_space(char c)
{
    return (c == ' ' || c == '\v' || c == '\f'
            || c == '\r' || c == '\t' || c == '\b');
}


size_t skip_space(char *str, size_t begin)
{
    size_t i = begin;
    for (; str[i] && is_white_space(str[i]); i++)
        continue;

    return i;
}


int is_ionumber(char *str)
{
    if (isdigit(str[0]) && (str[1] == '<'  || str[1] == '>'))
        return 1;

    return 0;
}


size_t get_token_size(struct input *input, size_t i)
{
    if (is_ionumber(&input->str[input->cur + i]))
        return 1;

    if (is_special_char(input->str[input->cur + i]))
        return get_special_word_size(input->str, input->cur + i);

    size_t len = 0;
    int n = 0;

    while (input->size > input->cur + len + i &&
            ((n = get_block(input, len + i))
            || (n = get_word_size(input->str, input->cur + len + i))))
    {
        if (n == -1)
            errx(2, "Lexer error");

        len += n;
        n = 0;
    }

    return len;
}

