/**
** \file delimiter.h
** \brief The function wich perform some magic on string using the delimiters.
*/
#ifndef DELIMITER_H
#define DELIMITER_H

#include <stddef.h>

#include "interface/input.h"


/**
** \fn size_t skip_space(char *str, size_t begin)
** \brief Skip the space of a string.
**
** \param str The input string
** \param begin The begining of the string
** \return The number of space to skip
*/
size_t skip_space(char *str, size_t begin);

/**
** \fn int is_ionumber(char *str)
** \brief Return true if the string is an io number.
**
** \param str The string to check
** \return True if the string is an io number
*/
int is_ionumber(char *str);

/**
** \fn size_t get_token_size(struct input *input, size_t i)
** \brief Get the size of the next token.
**
** \param input The input
** \param i The offset
** \return The size of the next token
*/
size_t get_token_size(struct input *input, size_t i);

#endif /* DELIMITER_H */
