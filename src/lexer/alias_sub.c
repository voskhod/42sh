#include "lexer/alias_sub.h"
#include "builtin/alias.h"
#include <string.h>

char *is_alias(char *value)
{
    struct alias *alias_next = alias_list;
    while (alias_next != NULL)
    {
        if (!strcmp(alias_next->alias, value))
        {
            return alias_next->value;
        }
        alias_next = alias_next->next;
    }
    return NULL;
}
