/**
** \file readline.h
** \brief readline interface.
*/
#ifndef READLINE_H
#define READLINE_H




/**
**  \brief check if 42sh is in interactive mode
**
**  \return 1 if 42sh is in interactive mode, 0 else
*/
int is_interactive(void);



/**
** \brief read input from standard input and display prompt if necessary
**
** The prompt is displayed after every new line if the standard input is a tty
**
** \return the next input line
** \param prompt the string to print as prompt
*/
char *get_next_line(const char *prompt);

#endif /* ! READLINE_H */
