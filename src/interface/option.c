#include <stdio.h>
#include <string.h>

#include "builtin/shopt.h"
#include "interface/option.h"
#include "execute/variable.h"
#include "utilities/utilities.h"


static inline const char *bool_to_str(int b)
{
    return b ? "on" : "off";
}


static void shopt_print(struct shopt *shopt)
{
    printf("ast_print      %s\n", bool_to_str(shopt->ast_print));
    printf("dotglob        %s\n", bool_to_str(shopt->dotglob));
    printf("expand_aliases %s\n", bool_to_str(shopt->expand_aliases));
    printf("extglob        %s\n", bool_to_str(shopt->extglob));
    printf("nocaseglob     %s\n", bool_to_str(shopt->nocaseglob));
    printf("nullglob       %s\n", bool_to_str(shopt->nullglob));
    printf("sourcepath     %s\n", bool_to_str(shopt->sourcepath));
    printf("xpg_echo       %s\n", bool_to_str(shopt->xpg_echo));

}


static void shopt_print_as_input()
{
    printf("shopt -O ast_print\n");
    printf("shopt -O dotglog\n");
    printf("shopt -O expand_aliases\n");
    printf("shopt -O extglob\n");
    printf("shopt -O nocaseglob\n");
    printf("shopt -O nullglob\n");
    printf("shopt -O sourcepath\n");
    printf("shopt -O xpg_echo\n");

}

static struct option *update_option(char *new_option, struct option *option,
                                        int activate)
{
    if (!strcmp(new_option, "norc"))
        option->norc = activate;
    else if (!strcmp(new_option, "ast-print"))
        g_shopt->ast_print = activate;

    else
        errx(EXIT_WRG_OPT, "Unknown option");

    return option;
}


static struct option *update_shopt(int is_next_valid, char *next,
                                    struct option *option, int plus)
{
    if (is_next_valid)
        option = update_option(next, option, plus);
    else
    {
        if (plus)
            shopt_print_as_input();
        else
            shopt_print(g_shopt);
    }

    return option;
}


struct option *get_option(int argc, char *argv[])
{
    struct option *option = mcalloc(1, sizeof(struct option));

    int i = 1;
    for (; i < argc; i++)
    {
        if (!strcmp(argv[i] + 1, "O"))
        {
            i++;
            update_shopt((i < argc), argv[i], option, argv[i - 1][0] != '+');
        }

        else if (!strcmp(argv[i], "-c"))
        {
            if (!option->file)
            {
                i++;
                if (i < argc)
                    option->command_input = argv[i];
                else
                    errx(EXIT_WRG_OPT, "-c option requires an argument");
            }
        }

        else if (!strncmp("--", argv[i], 2))
            option = update_option(argv[i] + 2, option, 1);
        else if (argv[i][0] == '-')
            option = update_option(argv[i] + 1, option, 1);

        else if (!option->file && !option->command_input)
        {
            option->file = argv[i];
            break;
        }
    }

    i++;
    char *buffer = NULL;
    for (int j = 1; i < argc && j < 10; i++, j++)
    {
        asprintf(&buffer, "%d", j);
        variable_set(buffer, strdup(argv[i]));
        buffer = NULL;
    }

#ifdef DEBUG
    printf("command_input: %s\n", option->command_input);
    printf("norc:          %s\n", bool_to_str(option->norc));
    printf("file:          %s\n", option->file);
    shopt_print(g_shopt);
#endif /* DEBUG */

    return option;
}

void option_destroy(struct option *option)
{
    mfree(option);
}
