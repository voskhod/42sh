/**
** \file option.h
** \brief The option parser.
*/
#ifndef OPTION_H
#define OPTION_H


/**
** \struct shopt
** \brief The shopt options
*/
struct shopt
{
    int ast_print;

    int dotglob;
    int expand_aliases;
    int extglob;
    int nocaseglob;
    int nullglob;
    int sourcepath;
    int xpg_echo;
};


/**
** \struct option
** \brief The options
*/
struct option
{
    char *command_input;
    int norc;
    char *file;
};


/**
** \fn struct option *get_option(int argc, char *argv[])
** \brief Get all the option from the shell argument.
**
** \param argc The number of strings in argv
** \param argv The arguments array
** \return The option find in argv
*/
struct option *get_option(int argc, char *argv[]);

/**
** \fn void option_destroy(struct option *option)
** \brief Destroy an struct option.
**
** \param option The struct option to delete
*/
void option_destroy(struct option *option);

#endif /* OPTION_H */
