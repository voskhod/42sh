#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <readline/readline.h>

#include "utilities/utilities.h"
#include "interface/completion.h"


char **cmd_tab = NULL;

char *builtin[] = 
{
    "shopt",
    "history",
    "echo",
    "exit",
    "source",
    "alias",
    "unalias",
    "jobs",
    "echo",
    "fg",
    "bg",
    "export",
    "alias",
    "cd",
    NULL
};

char *command_generator(const char *text, int state)
{
    static int i;
    static int len;
    char *cmd = NULL;

    if (!state)
    {
        i = 0;
        len = strlen(text);
    }

    /* Completion for the buitins and /usr/bin */
    while ((cmd = cmd_tab[i++]))
    {
        if (strncmp(cmd, text, len) == 0)
            return strdup(cmd);
    }

    /* Get the completion for the filename */
    return rl_filename_completion_function(text, state);
}


char **command_completion(const char *text, int begin, int end)
{
    (void)begin;
    (void)end;

    rl_attempted_completion_over = 1;

    return rl_completion_matches(text, command_generator);
}


void set_completion(void)
{
    unsigned count = 0;
    unsigned capacity = 100 + sizeof(builtin) / sizeof(char *);
    cmd_tab = mmalloc(capacity * sizeof(char *));

    char *cmd;
    unsigned i = 0;
    while ((cmd = builtin[i++]) != NULL)
    {
        cmd_tab[count] = cmd;
        count++;
    }


    DIR *d;
    struct dirent *cur;
    d = opendir("/usr/bin");
    if (d != NULL)
    {
        while ((cur = readdir(d)) != NULL)
        {
            /* We want store a null at the end */
            if (count + 1 >= capacity)
            {
                capacity = capacity * 2;
                cmd_tab = mrealloc(cmd_tab, capacity * sizeof(char *));
            }

            cmd_tab[count] = strdup(cur->d_name);
            count++;
        }
        closedir(d);
    }

    cmd_tab[count] = NULL;

    rl_attempted_completion_function = command_completion;
}
