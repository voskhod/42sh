#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "builtin/builtin.h"
#include "interface/input.h"
#include "execute/function.h"
#include "execute/term_man.h"
#include "execute/variable.h"
#include "interface/option.h"
#include "execute/jobs_ctrl.h"
#include "utilities/utilities.h"
#include "interface/completion.h"

void shell_init()
{
    xalloc_init();

    g_shopt = mcalloc(1, sizeof(struct shopt));

    alias_list = NULL;

    srand(time (NULL));

    using_history();
    stifle_history(2000);

    set_completion();

    char *home = getenv("HOME");
    char *line;
    if (home != NULL)
    {
        asprintf(&line, "%s/.42sh_history", home);
        read_history(line);
        mfree(line);
        
        variable_set(strdup("HOME"), strdup(home));
    }

    variable_set_from_int("?", 0);
    variable_set_from_int("$", getpid());
    variable_set_from_int("UID", getuid());
    
    variable_set(strdup("PS1"), strdup("42sh$ "));
    variable_set(strdup("PS2"), strdup("> "));

    variable_set_from_int("RANDOM", rand());

    variable_set(strdup("0"), strdup("42sh"));

    if (home != NULL)
        variable_set(strdup("OLDPWD"), strdup(home));
    variable_set(strdup("IFS"), strdup(" "));
}



void shell_quit()
{
    char *home = getenv("HOME");
    char *line;
    if (home != NULL)
    {
        asprintf(&line, "%s/.42sh_history", home);
        write_history(line);
        mfree(line);
    }
    mfree(g_shopt);
    
    delete_alias_list();

    jobs_ctrl_clear();
    variable_clear();
    fuction_clear();

    term_man_restore();

    xalloc_destroy();
}
