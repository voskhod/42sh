/**
** \file input.h
** \brief Input (from stdin).
*/
#ifndef INPUT_H
#define INPUT_H

#include <stdio.h>


/**
** \struct input
** \brief This struct contain one line from stdin. When the whole line has been
**      read, the old line is replace by a new one.
*/
struct input
{
    char *input;
    char *str;
    int from_str;
    size_t cur;
    size_t size;
};

/**
** \fn struct input *input_init_from_str(void)
** \brief Initialize a struct input from a string.
**
** \param str The input string
** \return An initialized struct input
*/

struct input *input_init_from_str(char *str);

/**
** \fn struct input *input_init(void)
** \brief Initialize a struct input.
**
** \return An initialized struct input
**
** The field str is a new string from stdin, the size and cur are set,
** respectively to the size of the string and 0. If an EOF is read, a struct
** input is return with str set at NULL.
*/
struct input *input_init(void);

/**
** \fn void input_destroy(struct input *input)
** \brief Destroy a struct input.
**
** \param input The input to destroy
*/
void input_destroy(struct input *input);


/**
** \fn struct input *input_update(struct input *input, size_t size_read)
** \brief Upadate the input.
**
** \param input The input to update
**
** If all the char have been read (cur => size), a new input is create and the
** old one is destroy.
*/
struct input *input_update(struct input *input);

/**
** \fn void input_reset(struct input *input)
** \brief Reset the input.
**
** \param The input to reset
**
** Reset set the current position behind the end of the string. The next call
** input_ipdate will get a new line.
*/
void input_reset(struct input *input);

/**
** \void switch_shell_prompt()
** \brief Switch the shell prompt between PS1 and PS2
*/
void switch_shell_prompt(void);

#endif /* INPUT_H */
