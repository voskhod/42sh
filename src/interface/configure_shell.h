/**
** \file configure_shell.h
** \brief Configure and close the shell.
*/
#ifndef CONFIGURE_SHELL_H
#define CONFIGURE_SHELL_H

/**
** \fn void shell_init()
** \brief Initialize the shell.
*/
void shell_init();

/**
** \fn void shell_quit()
** \brief Quit the shell.
*/
void shell_quit();

#endif /* CONFIGURE_SHELL_H */
