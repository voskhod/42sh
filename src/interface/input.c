#include <stdio.h>
#include <string.h>
#include <readline/history.h>

#include "utilities/utilities.h"
#include "interface/readline.h"
#include "execute/variable.h"
#include "interface/input.h"


static char *prompt = "42sh$ ";
static int is_ps1 = 1;

static void remove_comment(char *str)
{
    if (str == NULL)
        return;

    for (; *str && *str != '#'; str++)
        continue;

    if (*str == '#')
    {
        str[0] = '\n';
        str[1] = '\0';
    }
}


struct input *input_init_from_str(char *str)
{
    struct input *input = mcalloc(1, sizeof(struct input));

    input->str = strtok(str, "\n");
    remove_comment(input->str);

    if (input->str != NULL)
        input->size = strlen(input->str);
    else
        input->size = 0;

    input->from_str = 1;

    return input;
}

struct input *input_init(void)
{
    struct input *input = mcalloc(1, sizeof(struct input));

    fflush(stdout);
    char *line = get_next_line(prompt);
    remove_comment(line);

    if (line == NULL)
    {
        input->str = NULL;
        input->size = 0;
    }
    else
    {
        if (is_interactive() && *line != '\0')
        {
            HIST_ENTRY *prev = previous_history();
            if (prev == NULL || strcmp(prev->line, line))
                add_history(line);
        }

        input->size = asprintf(&input->str, "%s\n", line);
        mfree(line);
    }

    return input;
}


void input_destroy(struct input *input)
{
    if (input != NULL)
        mfree(input->str);
    mfree(input);
}


struct input *input_update(struct input *input)
{
    if (input->cur >= input->size)
    {
        if (input->from_str)
        {
            input->str = strtok(NULL, "\n");
            remove_comment(input->str);

            if (input->str == NULL)
                input->size = 0;
            else
                input->size = strlen(input->str);
        }
        else
        {
            input_destroy(input);
            input = input_init();
        }
    }

    return input;
}


void input_reset(struct input *input)
{
    input->cur += input->size + 1;
}


void switch_shell_prompt(void)
{
    if (is_ps1)
        prompt = variable_get("PS2");
    else
        prompt = variable_get("PS1");

    is_ps1 = !is_ps1;
}
