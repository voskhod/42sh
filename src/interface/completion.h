/**
** \file completion.h
** \brief Set up the comlpetion.
*/
#ifndef COMPLETION_H
#define COMPLETION_H

/**
** \fn char **set_completion(const char *text, int begin, int end)
** \brief Set up the comlpetion.
*/
void set_completion(void);

#endif /* COMPLETION_H */
