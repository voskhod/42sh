#include <criterion/criterion.h>
#include <string.h>

#include "utilities/utilities.h"
#include "lexer/lexer.h"
#include "interface/input.h"


struct lexer *init_func(char *str)
{
    xalloc_init();
    struct input *input = input_init_from_str(strdup(str));
    return lexer_init(input);
}


int try_expand(struct input *input, size_t i)
{
    input = input;
    i = i;

    return 0;
}


void end_func(struct lexer *lexer)
{
    lexer_destroy(lexer);
    xalloc_destroy();
}


int check_token_value(char *ref, char *str)
{
    if (ref == NULL && str == NULL)
        return 1;

    if (ref == NULL || str == NULL)
        return 0;

    return !strcmp(ref, str);
}


Test(Lexer, Single_peek_end)
{
    static char *str = " ls";
    struct lexer *lexer = init_func(str);

    cr_assert_eq(TOKEN_WORD, lexer_peek(lexer));

    end_func(lexer);
}


Test(Lexer, Single_peek_for)
{
    static char *str = "  for   \n";
    struct lexer *lexer = init_func(str);

    cr_assert_eq(TOKEN_FOR, lexer_peek(lexer));

    end_func(lexer);
}


Test(Lexer, Single_peek_word)
{
    static char *str = "    echo \n";
    struct lexer *lexer = init_func(str);

    cr_assert_eq(TOKEN_WORD, lexer_peek(lexer));

    end_func(lexer);
}


Test(Lexer, Single_pop)
{
    static char *str = "echo\n";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);

    cr_assert_eq(token->type, TOKEN_WORD);
    cr_assert(strcmp("echo", token->value) == 0);

    token_destroy(token);
    end_func(lexer);
}


Test(Lexer, Simple_pop_multiple)
{
    static char *str = " ls ; cat   ";
    struct lexer *lexer = init_func(str);

    enum token_type expected_types[] = { TOKEN_WORD, TOKEN_SEMI_COLON };

    struct token *token = NULL;
    size_t i = 0;
    for (; i < sizeof(expected_types) / sizeof(*expected_types); i++)
    {
        token = lexer_pop(lexer);

        cr_assert_eq(token->type, expected_types[i]);

        token_destroy(token);
    }

    end_func(lexer);
}


Test(Lexer, Single_pop_or)
{
    static char *str = "|| \n";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);

    cr_assert_eq(token->type, TOKEN_OR);

    token_destroy(token);
    end_func(lexer);
}


Test(Lexer, Pop_eof)
{
    static char *str = "ls";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);
    token_destroy(token);

    token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_EOF);
    token_destroy(token);

    end_func(lexer);
}


Test(Lexer, simple_if)
{
    static char *str = "if ls";
    struct lexer *lexer = init_func(str);

    enum token_type expected_types[] = { TOKEN_IF, TOKEN_WORD, TOKEN_EOF };

    struct token *token = NULL;
    size_t i = 0;
    for (; i < sizeof(expected_types) / sizeof(*expected_types); i++)
    {
        token = lexer_pop(lexer);

        cr_assert_eq(token->type, expected_types[i]);

        token_destroy(token);
    }

    end_func(lexer);
}


Test(Lexer, simple_else)
{
    static char *str = "else ls";
    struct lexer *lexer = init_func(str);

    enum token_type expected_types[] = { TOKEN_ELSE, TOKEN_WORD, TOKEN_EOF };

    struct token *token = NULL;
    size_t i = 0;
    for (; i < sizeof(expected_types) / sizeof(*expected_types); i++)
    {
        token = lexer_pop(lexer);

        cr_assert_eq(token->type, expected_types[i]);

        token_destroy(token);
    }

    end_func(lexer);
}


Test (Lexer, long_input)
{
    static char *str = "else if elif || && && word word2 word3";
    struct lexer *lexer = init_func(str);

    enum token_type expected_types[] = { TOKEN_ELSE, TOKEN_IF, TOKEN_ELIF,
        TOKEN_OR, TOKEN_AND, TOKEN_AND, TOKEN_WORD, TOKEN_WORD, TOKEN_WORD,
        TOKEN_EOF };

    struct token *token = NULL;
    size_t i = 0;
    for (; i < sizeof(expected_types) / sizeof(*expected_types); i++)
    {
        token = lexer_pop(lexer);

        cr_assert_eq(token->type, expected_types[i]);

        token_destroy(token);
    }

    end_func(lexer);
}


Test(Lexer, Simple_and)
{
    static char *str = "ls && ls";
    struct lexer *lexer = init_func(str);

    enum token_type expected_types[] = { TOKEN_WORD, TOKEN_AND, 
        TOKEN_WORD, TOKEN_EOF };

    struct token *token = NULL;
    size_t i = 0;
    for (; i < sizeof(expected_types) / sizeof(*expected_types); i++)
    {
        token = lexer_pop(lexer);

        cr_assert_eq(token->type, expected_types[i]);

        token_destroy(token);
    }

    end_func(lexer);
}


Test (Lexer, IO_number)
{
    char *str = "ls 1> file";
    struct lexer *lexer = init_func(str);

    token_destroy(lexer_pop(lexer));

    cr_assert_eq(lexer_peek(lexer), TOKEN_IO_NUMBER);
    token_destroy(lexer_pop(lexer));

    cr_assert_eq(lexer_peek(lexer), TOKEN_REDIRECT_OUT);
    token_destroy(lexer_pop(lexer));

    cr_assert_eq(lexer_peek(lexer), TOKEN_WORD);
    token_destroy(lexer_pop(lexer));

    end_func(lexer);
}


Test (Lexer, Number)
{
    char *str = "1";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_WORD);
    token_destroy(token);

    end_func(lexer);
}


Test (Lexer, e_vs_else)
{
    char *str = "e else else_word";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_WORD);
    token_destroy(token);

    token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_ELSE);
    token_destroy(token);

    token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_WORD);
    token_destroy(token);

    end_func(lexer);
}

Test (Lexer, double_semin_colon)
{
    static char *str = ";;";
    struct lexer *lexer = init_func(str);

    enum token_type expected_types[] = { TOKEN_DBL_SEMI_COLON, TOKEN_EOF };

    struct token *token = NULL;
    size_t i = 0;
    for (; i < sizeof(expected_types) / sizeof(*expected_types); i++)
    {
        token = lexer_pop(lexer);

        cr_assert_eq(token->type, expected_types[i]);

        token_destroy(token);
    }

    end_func(lexer);
}


Test (Lexer, Assign)
{
    static char *str = "var=ls pwd";
    struct lexer *lexer = init_func(str);


    struct token *token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_ASSIGN_WORD);
    cr_assert(!strcmp(token->value, "var=ls"));
    token_destroy(token);

    token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_WORD);
    cr_assert(!strcmp(token->value, "pwd"));
    token_destroy(token);

    end_func(lexer);
}


Test (Lexer, Single_quote_simple)
{
    static char *str = "'ls'";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);
    cr_assert_eq(token->type, TOKEN_WORD);
    cr_assert(!strcmp(token->value, "'ls'"));
    token_destroy(token);

    end_func(lexer);
}


Test (Lexer, Cmd)
{
    static char *str = "$()";
    struct lexer *lexer = init_func(str);

    struct token *token = lexer_pop(lexer);

    warnx("----------------------------------- %d", token->type);
    cr_assert_eq(token->type, TOKEN_WORD);
    cr_assert(!strcmp(token->value, "$()"));
    token_destroy(token);

    end_func(lexer);
}

