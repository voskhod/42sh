#include <criterion/criterion.h>

#include "utilities/utilities.h"
#include "lexer/lexer.h"
#include "parser/ast.h"
#include "interface/input.h"
#include "parser/parser.h"


int try_expand(struct input *input, size_t i)
{
    input = input;
    i = i;

    return 0;
}


Test(Parser, Empty_input)
{
    xalloc_init();
    struct lexer *lexer = lexer_init(input_init_from_str("\n"));

    struct ast *ast = parse_input(lexer);
    cr_assert_eq(NULL, ast);

    ast_destroy(ast);

    lexer_destroy(lexer);
    xalloc_destroy();
}


Test(Parser, Simple_command)
{
    xalloc_init();
    struct lexer *lexer = lexer_init(input_init_from_str("ls"));

    struct ast *ast = parse_input(lexer);

    cr_assert_neq(NULL, ast);
    cr_assert_eq(AST_NODE_COMMAND, ast->type);
    cr_assert(!strcmp("ls", ast->statement.cmd->argv[0]));

    cr_assert_eq(NULL, ast->next);

    ast_destroy(ast);

    lexer_destroy(lexer);
    xalloc_destroy();
}


Test(Parser, Simple_command_with_args)
{
    xalloc_init();
    struct lexer *lexer = lexer_init(input_init_from_str("ls -la"));

    struct ast *ast = parse_input(lexer);

    cr_assert_neq(NULL, ast);
    cr_assert_eq(AST_NODE_COMMAND, ast->type);
    cr_assert(!strcmp("ls -la", ast->statement.cmd->argv[0]));

    cr_assert_eq(NULL, ast->next);

    ast_destroy(ast);

    lexer_destroy(lexer);
    xalloc_destroy();
}

