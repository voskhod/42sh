from argparse import ArgumentParser
from pathlib import Path
from difflib import unified_diff
from termcolor import colored

import subprocess as sp
import yaml
import os
import sys


def run_shell(args, stdin, time):
    return sp.run(args, capture_output=True, text=True, input=stdin, timeout=time)

def run_file(args, time):
    return sp.run(args, capture_output=True, text=True, timeout=time)


def diff(ref, student):
    ref = ref.splitlines(keepends=True)
    student = student.splitlines(keepends=True)

    return ''.join(unified_diff(ref, student, fromfile="ref", tofile="student"))


def test(binary, testcase, valgrind, timeout, category):
    

    if not testcase.get("file"):
        current_ref = ["bash", "--posix"]
        binary = str(binary).split()
        
        if valgrind:
            binary = ["valgrind"] + ["--quiet"] + binary
            current_ref = ["valgrind"] + ["--quiet"] + current_ref

        ref = run_shell(current_ref, testcase["stdin"], timeout)
        student = run_shell(binary, testcase["stdin"], timeout)

    else:
        args_ref = ["bash", "--posix", "tests/yaml_test/" + category + "/" + testcase.get("file")]
        args_student = [str(binary), "tests/yaml_test/" + str(category) + "/" + testcase.get("file")]
        ref = run_file(args_ref, timeout)
        student = run_file(args_student, timeout)


    for check in testcase.get("checks", ["stdout", "stderr", "returncode"]):
        if check == "stdout":
            assert ref.stdout == student.stdout, \
                f"stdout differs:\n{(diff(ref.stdout, student.stdout))}"
        if check == "returncode":
            assert ref.returncode == student.returncode, \
                f"returncode differs:\nexpected: {ref.returncode}\n get: {student.returncode}"
        if check == "stderr":
            assert ref.stderr == student.stderr, \
                f"stderr differs:\n{(diff(ref.stderr, student.stderr))}"
        if check == "has_stderr":
            assert student.stderr != "", \
                "Something was expect on stderr"


def file_treatment(content, timeout, sanity, binary, quiet, category):
    if timeout != None:
        timeout = float(timeout)
    for testcase in content:
        descr = testcase.get("description")
        try:
            test(binary, testcase, args.sanity, timeout, category)
        except AssertionError as err:
            print(f"[{colored('KO', 'red')}]", testcase["name"], f"(from {category})")
            if quiet:
                print(f"{colored('Test description:', 'yellow')} ", descr)
            print(err)
            print('--------------------------------------------------------------------------------')
        else:
            if quiet:
                print(f"[{colored('OK', 'green')}]", testcase["name"], f"(from {category})")
                print(f"{colored('Test description:', 'yellow')} ", descr)
                print('--------------------------------------------------------------------------------')


def print_category():
    for dirs in os.listdir('tests/yaml_test/'):
        print(' - ', dirs)



if __name__ == "__main__":
    parser = ArgumentParser(description='42sh Testsuite')
    parser.add_argument('bin', metavar='BIN', nargs='?', help="binary to compare to bash --posix")
    parser.add_argument('-l', '--list', action='store_true', help="Print every category available")
    parser.add_argument('-s', '--sanity', action='store_true', help="Check memory leak if available")
    parser.add_argument('-t', '--timeout', nargs='?', default=None, type=float, \
            help="Add a timeout restriction on execution")
    parser.add_argument('-c', '--category', nargs='*', default=None, \
            help='Select category to tests, by default test every category')
    parser.add_argument('-q', '--quiet', action='store_false', help='Only print failed tests without description')
    args = parser.parse_args()

    if args.list:
        print_category()
        sys.exit()

    binary = args.bin
    if not binary:
        binary = "42sh"

    if (binary and not os.path.isfile(binary) and not os.access(binary, os.X_OK)):
        raise Exception (f"{binary} is not an executable file")

    else:
        binary = Path(binary).absolute()
        category = args.category
        if category == None:
            category = os.listdir('tests/yaml_test/')
        for cate in category:
            if (not os.path.exists('tests/yaml_test/' + cate)):
                raise Exception ("Category doesn't exist")
            dirs = os.listdir('tests/yaml_test/' + cate)
            for yml_file in dirs:
                _, ext = os.path.splitext(yml_file)
                if ext != '.yml':
                    continue
                yml_file = Path('tests/yaml_test/' + cate + '/' + yml_file)
                with open(yml_file, "r") as test_file:
                    content = yaml.safe_load(test_file)
                file_treatment(content, args.timeout, args.sanity, binary, args.quiet, cate)
