#include <criterion/criterion.h>

#include "expand/maths_compute.h"
#include "utilities/utilities.h"

Test (Math_compute, Empty)
{
    xalloc_init();
    int err;
    cr_assert_eq(0, maths_compute("", &err));
    cr_assert_neq(-1, err);
    xalloc_destroy();
}


Test (Math_compute, Easy)
{
    xalloc_init();
    int err;
    cr_assert_eq(5, maths_compute("5", &err));
    xalloc_destroy();
}


Test (Math_compute, Fact)
{
    xalloc_init();
    int err;
    cr_assert_eq(6, maths_compute("2 * 3", &err));
    xalloc_destroy();
}


Test (Math_compute, Plus)
{
    xalloc_init();
    int err;
    cr_assert_eq(5, maths_compute("2 + 3", &err));
    xalloc_destroy();
}


Test (Math_compute, Plus_factor)
{
    xalloc_init();
    int err;
    cr_assert_eq(7, maths_compute("1 + 2 * 3", &err));
    xalloc_destroy();
}


Test (Math_compute, Div)
{
    xalloc_init();
    int err;
    cr_assert_eq(2, maths_compute("4 / 2", &err));
    xalloc_destroy();
}


Test (Math_compute, Sub)
{
    xalloc_init();
    int err;
    cr_assert_eq(2, maths_compute("4 - 2", &err));
    xalloc_destroy();
}


Test (Math_compute, Pow_easy)
{
    xalloc_init();
    int err;
    cr_assert_eq(9, maths_compute("3 ** 2", &err));
    xalloc_destroy();
}


Test (Math_compute, Pow_less_easy)
{
    xalloc_init();
    int err;
    cr_assert_eq(19, maths_compute("2 * 3 ** 2 + 1", &err));
    xalloc_destroy();
}


Test (Math_compute, Logical_not)
{
    xalloc_init();
    int err;
    cr_assert_eq(0, maths_compute("!5", &err));
    xalloc_destroy();
}


Test (Math_compute, Bit_not)
{
    xalloc_init();
    int err;
    cr_assert_eq(-6, maths_compute("~5", &err));
    xalloc_destroy();
}


Test (Math_compute, Bit_and)
{
    xalloc_init();
    int err;
    cr_assert_eq(0, maths_compute("1 + 2 & 0", &err));
    xalloc_destroy();
}


Test (Math_compute, Bit_or)
{
    xalloc_init();
    int err;
    cr_assert_eq(3, maths_compute("1 + 2 | 0", &err));
    xalloc_destroy();
}


Test (Math_compute, Bit_xor)
{
    xalloc_init();
    int err;
    cr_assert_eq(3, maths_compute("1 + 2 | 0", &err));
    xalloc_destroy();
}


Test (Math_compute, Logical_and)
{
    xalloc_init();
    int err;
    cr_assert_eq(0, maths_compute("2 && 0", &err));
    xalloc_destroy();
}


Test (Math_compute, Logical_or)
{
    xalloc_init();
    int err;
    cr_assert_eq(1, maths_compute("3 || 0", &err));
    xalloc_destroy();
}


Test (Math_compute, Logical_and_or)
{
    xalloc_init();
    int err;
    cr_assert_eq(0, maths_compute("2 && 0 || 0", &err));
    xalloc_destroy();
}


Test (Math_compute, Logical_and_or_2)
{
    xalloc_init();
    int err;
    cr_assert_eq(1, maths_compute("3 || 0 && 0", &err));
    xalloc_destroy();
}



Test (Math_compute, Par)
{
    xalloc_init();
    int err;
    cr_assert_eq(8, maths_compute("2 * ( 3  + 1)", &err));
    xalloc_destroy();
}
