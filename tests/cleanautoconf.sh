#!/bin/sh

cd $PWD

make clean

rm -rf aclocal.m4 autom4te.cache config.h config.log configure Makefile
rm -rf Makefile.in build-aux config.h.in* config.status stamp-h1 test
rm -rf testsuite test-example test_xalloc tests_lexer D42sh 42sh
rm -rf */.deps */.dirstamp */*/.deps */*/.dirstamp
rm -f *.o */*.o */*/*.o
rm -rf doc/Doxyfile
rm -f graph.dot graph.png
