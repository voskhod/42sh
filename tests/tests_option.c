#include <criterion/criterion.h>

#include "interface/option.h"
#include "utilities/utilities.h"
#include "builtin/shopt.h"


Test(Option, Simple_long)
{
    xalloc_init();

    g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "--norc" };
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_eq(NULL, opt->command_input);
    cr_assert_eq(1, opt->norc);
    cr_assert_eq(0, g_shopt->ast_print);

    mfree(g_shopt);
    option_destroy(opt);
    xalloc_destroy();
}


Test(Option, Simple_short)
{
    xalloc_init();
    g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "-c", "asd" };
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_str_eq("asd", opt->command_input);
    cr_assert_eq(0, opt->norc);
    cr_assert_eq(0, g_shopt->ast_print);

    mfree(g_shopt);
    option_destroy(opt);
    xalloc_destroy();
}


Test(Option, Short_and_long)
{
    xalloc_init();
    
    g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "--norc", "-c", "asd", "lol"};
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_str_eq("asd", opt->command_input);
    cr_assert_eq(1, opt->norc);
    cr_assert_eq(0, g_shopt->ast_print);
    
    mfree(g_shopt);
    option_destroy(opt);
    xalloc_destroy();
}


Test(Option, Multiple_input)
{
    xalloc_init();

    g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "test", "-c", "asd" };
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_eq(NULL, opt->command_input);
    cr_assert_neq(NULL, opt->file);

    option_destroy(opt);
    mfree(g_shopt);
    xalloc_destroy();
}


Test(Option, Multiple_input_2)
{
    xalloc_init();

    g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "-c", "asd", "test" };
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_neq(NULL, opt->command_input);
    cr_assert_eq(NULL, opt->file);

    mfree(g_shopt);
    option_destroy(opt);
    xalloc_destroy();
}


Test(Option, Input_O_minus)
{
    xalloc_init();

    g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "-O", "ast-print" };
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_eq(1, g_shopt->ast_print);
    option_destroy(opt);
    mfree(g_shopt);
    xalloc_destroy();
}


Test(Option, Input_O_plus)
{
    xalloc_init();
    
     g_shopt = mcalloc(1, sizeof(struct shopt));

    char *argv[] = { "42sh", "--ast-print", "+O", "ast-print" };
    struct option *opt = get_option(sizeof(argv) / sizeof(*argv), argv);

    cr_assert_eq(0, g_shopt->ast_print);
    option_destroy(opt);

    mfree(g_shopt);
    xalloc_destroy();
}
