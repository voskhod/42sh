#include <criterion/criterion.h>
#include "utilities/utilities.h"


Test(Xalloc, init_destroy)
{
    xalloc_init();
    xalloc_destroy();
}


Test(Xalloc, basic_free_alloc)
{
    xalloc_init();

    void *ptr = malloc(5);
    free(ptr);

    xalloc_destroy();
}
