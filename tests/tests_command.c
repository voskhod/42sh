#include <criterion/criterion.h>
#include <string.h>

#include "parser/command.h"
#include "utilities/utilities.h"

#undef CMD_CAPACITY
#define CMD_CAPACITY 2


Test(Command, Multiple_append)
{
    xalloc_init();
    struct command *cmd = command_init();

    command_append(cmd, strdup("aze"));

    cr_assert(!strcmp("aze", cmd->argv[0]));
    cr_assert_eq(NULL, cmd->argv[1]);

    command_destroy(cmd);
    xalloc_destroy();
}
