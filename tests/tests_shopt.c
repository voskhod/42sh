#include <criterion/criterion.h>
#include <stdio.h>

#include "builtin/shopt.h"
#include "utilities/utilities.h"
#include "interface/option.h"

Test(Shopt, Arg_without_option)
{
    xalloc_init();
    
    char *argv[] = { "shopt", "ast_print" };
    g_shopt = mcalloc(1, sizeof(struct shopt));
    int argc = (sizeof(argv) / sizeof(*argv));
    int value = exe_shopt(argc, argv);

    cr_assert_eq(value, 0);

    mfree(g_shopt);
    xalloc_destroy();
}

Test(Shopt, Arg_with_option_s)
{
    xalloc_init();
    
    char *argv[] = { "shopt", "-s", "ast_print" };
    g_shopt = mcalloc(1, sizeof(struct shopt));
    int argc = (sizeof(argv) / sizeof(*argv));
    int value = exe_shopt(argc, argv);

    cr_assert_eq(value, 0);
    cr_assert_eq(g_shopt->ast_print, 1);

    mfree(g_shopt);
    xalloc_destroy();
}

Test(Shopt, Arg_with_option_q)
{
    xalloc_init();
    
    char *argv[] = { "shopt", "-q", "ast_print" };
    g_shopt = mcalloc(1, sizeof(struct shopt));
    int argc = (sizeof(argv) / sizeof(*argv));
    int value = exe_shopt(argc, argv);

    cr_assert_eq(value, 1);

    mfree(g_shopt);
    xalloc_destroy();
}

Test(Shopt, Arg_with_double_option)
{
    xalloc_init();
    
    char *argv[] = { "shopt", "-s", "-s", "dotglob", "ast_print" };
    g_shopt = mcalloc(1, sizeof(struct shopt));
    int argc = (sizeof(argv) / sizeof(*argv));
    int value = exe_shopt(argc, argv);

    cr_assert_eq(value, 0);
    cr_assert_eq(g_shopt->ast_print, 1);
    cr_assert_eq(g_shopt->dotglob, 1);
 
    char *argvv[] = { "shopt", "-u", "dotglob", "ast_print" };
    int argcc = (sizeof(argvv) / sizeof(*argvv));
    value = exe_shopt(argcc, argvv);

    cr_assert_eq(value, 0);
    cr_assert_eq(g_shopt->ast_print, 0);
    cr_assert_eq(g_shopt->dotglob, 0);

    mfree(g_shopt);
    xalloc_destroy();
}
