* 42sh is an POSIX compliant ([SCL][1] shell written in C, made by Alexis Huard, Romain Chevoleau
  and me, during our third year at EPITA. We had only four week to implements everything (basic
  command execution, builtins, control flow, redirections, jobs controls...).

* If you are a students, don't be a fucking monkey and go read readline's doc and [this][2] indead
  of cheating.

[1]: https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html
[2]: https://www.gnu.org/software/libc/manual/html_node/Job-Control.html#Job-Control

```
            _______
            |     |
            |     |
    ________|     |________
    |                     |
    |   gabriel.bauduin   |
    |_______       _______|
            |      |
            |      |
            |      |
            |      |
            |      |
            |      |
            |      |
            |      |
            |______|

        Adieu camarade,
 Tu restera a jamais dans nos coeur.
```

# Build
```
./autogen.sh && ./configure
make
```

# Documentation
```
make doc
```

# Run the tests
* Create the env directory : `python -m venv env`
* Activate virtual env : `source env/bin/activate`
* Download needed library : `pip install -r tests/requirements.txt`
* Deativate virtual env : `deactivate`
* Run testsuite : On the root on the project python tests/testsuite.py
* Any other questions : Read help page of the testsuite
